
.. index::
   pair: HTTP ; CORS


.. _http_julia_cors:

=======================================================
CORS
=======================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/tag/CORS
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS#Simple_requests
   - https://x.com/b0rk/status/1162398648996651010?s=20
   - https://x.com/b0rk/status/1162393308024492033?s=20


.. figure:: cors_1.jpeg
   :align: center

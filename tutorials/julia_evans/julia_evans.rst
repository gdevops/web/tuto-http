
.. index::
   pair: Tutorial ; Julia Evans

.. _http_julia:

=======================================================
**Learn your browser's language** by Julia Evans
=======================================================

- https://x.com/b0rk/status/1162392625057583104?s=20
- https://wizardzines.com/


.. toctree::
   :maxdepth: 3

   apis/apis
   curl/curl
   cors/cors
   how_urls/how_urls
   security_headers/security_headers
   x_headers/x_headers

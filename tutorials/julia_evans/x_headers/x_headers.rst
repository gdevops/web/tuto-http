

.. index::
   pair: HTTP; x-headers


.. _http_julia_x_headers:

=======================================================
x-headers
=======================================================

.. seealso::

   - https://x.com/b0rk/status/1161283690925834241?s=20



.. figure:: xheaders.jpeg
   :align: center

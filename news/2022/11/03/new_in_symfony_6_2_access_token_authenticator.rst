
.. _access_token_authenticator:

===============================================================
2022-11-03 **New in Symfony 6.2: Access Token Authenticator**
===============================================================


- https://symfony.com/blog/new-in-symfony-6-2-access-token-authenticator


Access tokens, also called **bearer tokens**, are defined in RFC6750 and
are popular when working with APIs.

Any party in possession of an access token can use it to get access to
the associated resources.

That's why these tokens need to be protected from disclosure in storage
and in transport.

In Symfony 6.2 we're adding a new authenticator which is able to fetch
access tokens and retrieve the associated user identifier.

The new authenticator can extract tokens from the request header (RFC6750 Section 2.1),
the request body (RFC6750 Section 2.2) and the query string (RFC6750 Section 2.3).

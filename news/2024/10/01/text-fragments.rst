.. index::
   pair: Text ; Fragments
   pair: Firefox; Text framenets (2024-10-01)

.. _text_fragments_2024_10_01:

====================================================================================
2024-10-01 **Text fragments** are now supported with Firefox 131 (2024-10-01)
====================================================================================

- https://web.dev/articles/text-fragments?hl=fr#acknowledgements
- https://github.com/WICG/scroll-to-text-fragment
- https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/131
- https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/131#apis
- https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments
- https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/131#:~:text=text%20fragments%20are%20now%20supported
- https://web.dev/articles/text-fragments?hl=fr#:~:text=Conclusion-,L'URL%20des%20fragments%20de,le%20fragment%20de%20texte).,-Liens%20associ%C3%A9s
- https://web.dev/articles/text-fragments?hl=fr
- https://web.dev/articles/text-fragments?hl=fr#text_fragments 
- https://web.dev/articles/text-fragments?hl=fr#start_and_end
- https://web.dev/articles/text-fragments?hl=fr#prefix-_and_-suffix
- https://web.dev/articles/text-fragments?hl=fr#creating_text_fragment_urls_with_a_browser_extension
- https://web.dev/articles/text-fragments?hl=fr#multiple_text_fragments_in_one_url
- https://web.dev/articles/text-fragments?hl=fr#mixing_element_and_text_fragments
- https://web.dev/articles/text-fragments?hl=fr#the_fragment_directive
- https://web.dev/articles/text-fragments?hl=fr#conclusion


Firefox 131 announce
============================

`Text fragments are now supported <https://developer.mozilla.org/en-US/docs/Mozilla/Firefox/Releases/131#:~:text=text%20fragments%20are%20now%20supported>`_, 
allowing users to link to and highlight specific portions of text in a web page. 

This feature uses a particular syntax in the URL fragment that identifies 
the target based on patterns in the rendered text. 

Developers can now also use the existence of the 
**Document.fragmentDirective** property** (an instance of the 
FragmentDirective interface) to  feature-check for text fragment support. 

Additionally, the ::target-text pseudo-element can be used to select 
and style text that has been selected using a text fragment link. 
(Firefox bug 1914877) 


Text fragments by MDN
==============================

- https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments

Text fragments allow linking directly to a specific portion of text in a 
web document, without requiring the author to annotate it with an ID, 
using particular syntax in the URL fragment. 

Supporting browsers are free to choose how to draw attention to the 
linked text, e.g. with a color highlight and/or scrolling to the content 
on the page. 

This is useful because it allows web content authors to deep-link to 
other content they don't control, without relying on the presence of IDs 
to make that possible. 

Building on top of that, it could be used to generate more effective 
content-sharing links for users to pass to one another.

Browser compatibility
===========================

- https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments#browser_compatibility


Directive "fragment"
============================

Il y a un élément de la syntaxe que je n'ai pas encore expliqué: la 
directive de fragment :~:. Pour éviter les problèmes de compatibilité 
avec les fragments d'éléments d'URL existants, comme indiqué ci-dessus,
la spécification des fragments de texte introduit la directive de fragment. 

La directive de fragment est une partie du fragment d'URL délimitée par 
la séquence de code :~:. 

Il est réservé aux instructions de l'user-agent, telles que text=, et 
est supprimé de l'URL lors du chargement afin que les scripts d'auteur 
ne puissent pas interagir directement avec lui. 

Les instructions de l'agent utilisateur sont également appelées directives. 
Dans le cas concret, text= est donc appelé une directive de texte.

Remerciements
===================

- https://web.dev/articles/text-fragments?hl=fr#acknowledgements
- https://github.com/WICG/scroll-to-text-fragment

Les fragments de texte ont été implémentés et spécifiés par Nick Burris 
et David Bokan, avec la contribution de Grant Wang. 

Merci à Joe Medley pour avoir examiné attentivement cet article. 

Image héros de Greg Rakozy sur Unsplash.


Liens MDN
=============

- https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments#:~:text=historically%2C%20one%20of%20the%20web's%20key%20features%20has%20always%20been%20its%20ability%20,the%20web%2C%20a%20web

Articles
===============

- https://web.dev/articles/text-fragments?hl=fr
- https://web.dev/articles/text-fragments?hl=fr#text_fragments 
- https://web.dev/articles/text-fragments?hl=fr#start_and_end
- https://web.dev/articles/text-fragments?hl=fr#prefix-_and_-suffix
- https://web.dev/articles/text-fragments?hl=fr#creating_text_fragment_urls_with_a_browser_extension
- https://web.dev/articles/text-fragments?hl=fr#multiple_text_fragments_in_one_url
- https://web.dev/articles/text-fragments?hl=fr#mixing_element_and_text_fragments
- https://web.dev/articles/text-fragments?hl=fr#the_fragment_directive
- https://web.dev/articles/text-fragments?hl=fr#conclusion

.. index::
   pair: Hurl ; 6.0.0

.. _hurl_6_0_0:

==================================================================================
2024-12-03 **Hurl 6.0.0**
==================================================================================

- https://github.com/Orange-OpenSource/hurl/releases/tag/6.0.0
- https://hurl.dev/
- https://github.com/Orange-OpenSource/hurl
- https://linuxfr.org/news/testeur-de-requetes-http-hurl-6-0-0

Enhancements
=====================

- Implement function newUuid #973
- Implement --limit-rate from curl #1222
- **Add --curl option to export executed requests to curl commands** #2679
- Configure --connect-timeout per request #3163
- Support short name for sections [QueryStringParams] => [Query], [FormParams] => [Form], [MultipartFormData] => [Multipart] #3238
- Remove url-specific parser (align with grammar) #3244
- Remove the crate float-cmp #3247
- Jsonpath / Add filter on boolean value #3252
- Jsonpath / Add non-equal filter on string and number value #3261
- Add support for backtick strings in predicates values #3317
- Categorise options in --help #3339
- Support more JSON / XML "like" mimetypes with debug output #3343
- Add curl debug command to --json and JSON report #3374
- Add curl debug command to HTML report #3386
- Render Date value #3431
- Add newDate generator #3443


Articles
=================

#Hurl, maybe my favourite #Postman replacement
-----------------------------------------------------

- https://floss.social/@fabian/113594430091406141

#Hurl, maybe my favourite #Postman replacement, just released a new major 
version. 

Nothing too big from my POV, but the --curl "native export" looks useful. 

So you can communicate your requests to folks who use curl, or use it as 
input to other tools’ importers, which usually "speak" curl.

(Before you had to fish the #curl equivalent out of the verbose output. 

PS: I with --very-verbose was just -vv or -vv; to whom it may concern :)

Testeur de requêtes HTTP Hurl 6.0.0
-----------------------------------------

- https://linuxfr.org/news/testeur-de-requetes-http-hurl-6-0-0
- https://linuxfr.org/users/jcamiel
- https://linuxfr.org/news/epub-le-convertisseur-epub3-a-la-volee-de-linuxfr-org#comment-1974494

Hurl a déjà été mentionné sur LinuxFr.org, notamment parce qu’il est 
utilisé pour les tests de certains services du site, à savoir actuellement:

- img,  le cache d’images et epub, 
- le convertisseur EPUB3 à la volée (chacun avec sa suite de tests pour 
  valider le bon fonctionnement des services en IPv4 et en IPv6, en HTTP 1.1 
  et en HTTP 2.0). 
  
`jcamiel, un des mainteneurs de Hurl <https://linuxfr.org/users/jcamiel>`_, 
est `d’ailleurs intervenu <https://linuxfr.org/news/epub-le-convertisseur-epub3-a-la-volee-de-linuxfr-org#comment-1974494>`_ 
sur une des  dépêches précédentes, venu discuter des évolutions et idées 
d’améliorations qui permettraient de simplifier l’écriture de telles suites de tests.

Nouveautés de la version 6.0.0 
+++++++++++++++++++++++++++++++++++++++

- fonctions pour générer des valeurs dynamiquement : il était possible 
  de donner en argument de la ligne de commande une valeur à une variable 
  utilisée dans un fichier .hurl. 
  Maintenant une fonction pour générer dynamiquement une valeur : 
  les deux fonctions fournies avec cette version sont:
  
  - newDate pour avoir la date courante UTC au format RFC 3339 
  - et newUUID pour produire un identifiant unique universel UUIDv4 
    aléatoire. 
    
    D’autres fonctions seront bien évidemment ajoutées par la suite.
    
- export de l’exécution en commandes curl : l’option --curl permet de 
  stocker dans un fichier une liste de commandes curl correspondant à 
  l’exécution (de manière plus facile qu’en utilisant --verbose et en 
  cherchant ce qui ressemble à du curl dedans). 
  Les commandes curl peuvent aussi être obtenues dans les rapports JSON 
  et HTML désormais.
- noms de sections plus courts : 

  - [Query] peut être utilisé à la place de [QueryStringParams], 
  - [Form] à la place de [FormParams] 
  - et [Multipart] à la place de [MultipartFormData]
  
- prise en charge de deux options de curl supplémentaires:

  - --limit-rate 
  - et --connect-timeout, 
  
  et leur équivalent limit-rate et connect-timeout dans la section [Options])
  
- refonte de hurl --help
- corrections de bugs
- encore d’autres choses et pour avoir plus de détails consultez la note 
  de publication détaillée.


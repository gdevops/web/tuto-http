.. index::
   pair: Web ; Text_fragments
   pair: URI ; Text_fragments
   pair: Fragment ; Text_fragments

.. _joachim_2024_12_03:

==================================================================================
2024-12-03 **Lier un fragment de texte dans une page web** par Joachim Robert
==================================================================================

- https://blog.professeurjoachim.com/billet/2024-12-03-lier-un-fragment-de-texte-dans-une-page-web
- https://addons.mozilla.org/fr/firefox/addon/link-to-text-fragment/
- https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments
- :ref:`text_fragments_2024_10_01`

Sur le blog
===============


LE truc qui a changé mon usage du web ces derniers mois, c’est les 
caractères suivants::

    #:~:text=

**Cette formule magique permet de faire un lien direct vers un fragment 
de texte présent dans une page**.

Ce lien est composé d’une adresse::

- https://…/blablabla.html
- à laquelle on ajoute #:~:text=, puis le texte qu’on vise.

En cumulant les trois, le lien ciblera la première occurence de la phrase. 
Magie 🧙✨

Exemple::: 

    gutenberg.org/cache/epub/55456/pg55456-images.html#:~:text=Mais je ne veux pas fréquenter des fous

Il y a plus d’infos sur `l’excellent site MDN https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments <https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments>`_ 

- comment définir un mot de début et un mot de fin, 
- comment spécifier un préfixe ou un suffixe à la sélection (pour affiner 
  la recherche lorsqu’on vise un fragment de texte précis alors qu’il est 
  répété dans le document). 
  Lorsque vous faites un copier-coller d’un lien comme ça, vous verrez 
  peut-être des %20 à la place des espaces et d’autres %xx à la place 
  d’autres caractères (ponctuation, lettres accentuées…), c’est normal 
  et ça ne change rien au résultat.

La fonctionnalité existait sur Chrome depuis quelques années, et elle est 
arrivée dans Firefox et Safari plus récemment. 

Il existe une extension Firefox pour copier le lien qui vise la sélection 
courante dans le document.

Ce qui a tout changé pour moi c’est que ça enrichit vraiment le partage 
de liens. 
J’aime partager des liens. 

Un collègue a besoin d’une info ? Je lui trouve le bon endroit dans la 
doc qui fait 40 écrans de scroll. 
Une amie m’envoie un article amusant ? Je lui réponds un lien vers mon 
paragraphe préféré et on rigole bien. 
Mon pire ennemi me calomnie bassement ? Je lui réponds de manière passive 
agressive avec un lien ciblé sur la pire insulte dans une liste de mots 
vulgaires sur Topito (il ne mérite pas plus de considération que ça de 
ma part mais j’espère qu’il se sent insulté que ça n’ait pas été un site 
respectable comme le wiktionnaire ou urban dictionary).

Bref, ça a ses usages, ce truc.


Sur mastodon
================

- https://boitam.eu/@joachim/113584646664277288

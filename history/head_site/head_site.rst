
============================================
Head site history (https://httpwg.org)
============================================

- https://httpwg.org


2023
=======

.. figure:: ../../images/http_home_specs_2023_08.png
   :align: center
   :width: 600


2022
=======

.. figure:: ../../images/http_home_specs_2022.png
   :align: center
   :width: 600


.. index::
   pair: Robert Cailliau ; people

.. _robert_cailliau:

=====================================
Robert Cailliau
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Robert_Cailliau
   - http://www.robertcailliau.eu/en/Welcome/


.. figure:: Robert_Cailliau_On_Desk.jpg
   :align: center

Présentation
=============

Robert Cailliau, né le 26 janvier 1947 à Tongres, est un ingénieur et
informaticien belge qui a contribué au développement et, de façon cruciale,
à la diffusion du World Wide Web dès son invention par Tim Berners-Lee.

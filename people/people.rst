
.. index::
   pair: HTTP  ; people

.. _http_people:

=====================================
People
=====================================

.. toctree::
   :maxdepth: 3

   robert_cailliau/robert_cailliau
   stephane_bortzmeyer/stephane_bortzmeyer

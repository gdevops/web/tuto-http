
.. index::
   pair: Bortzmeyer  ; people

.. _stephane_bortzmeyer:

=====================================
Stéphane Bortzmeyer
=====================================

.. seealso::

   - https://www.bortzmeyer.org/
   - https://x.com/bortzmeyer
   - https://mastodon.gougere.fr/@bortzmeyer
   - https://the-federation.info/

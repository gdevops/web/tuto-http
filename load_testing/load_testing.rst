.. index::
   pair: HTTP ; Load testing
   ! Load testing

.. _load_testing:

=====================================
Load testing
=====================================

- https://www.youtube.com/watch?v=KECr2BujqtM (15 Top Load Testing Tools Open Source MUST KNOW in 2021)

.. toctree::
   :maxdepth: 6

   locust/locust

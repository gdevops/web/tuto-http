.. index::
   pair: Load testing ; Locust
   ! locust

.. _locust:

==========================================================================================================================================================
|Locust| **Locust** (The Python-based load testing framework)
==========================================================================================================================================================

- https://github.com/locustio/locust
- https://locust.io/
- https://docs.locust.io/en/stable/
- https://www.youtube.com/c/keptn/videos
- https://github.com/cyberw.atom

Description
============

- https://www.youtube.com/watch?v=Ok4x2LIbEEY

**Locust is an easy to use, scriptable and scalable performance testing tool**.

You define the behaviour of your users in regular Python code, instead
of being stuck in a UI or restrictive domain specific language.

This makes Locust infinitely expandable and very developer friendly.


Chapitres
==========

.. toctree::
   :maxdepth: 3

   installation/installation
   plugins/plugins
   locust-swarm/locust-swarm
   tests/tests
   examples/examples

.. toctree::
   :maxdepth: 6

   articles/articles

.. toctree::
   :maxdepth: 3

   versions/versions

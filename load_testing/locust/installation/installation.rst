.. index::
   pair: Locust; Installation

.. _locust_install:

==================================================
**Locust Installation**
==================================================


Local installation
===================

With poetry
-------------

::

    poetry init
    poetry add locust
    poetry add ipython

    poetry install


locust version
-----------------

::

    ✦ ❯ locust -V

    locust 2.12.1

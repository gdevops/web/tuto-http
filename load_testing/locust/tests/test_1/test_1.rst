

.. _tests_locust_1:

================================
Test locust 1
================================

- http://0.0.0.0:8089

.. code-block:: python

    from locust import HttpUser, task

    class HelloWorldUser(HttpUser):
        @task
        def hello_world(self):
            self.client.get("/hello")
            self.client.get("/world")



::

    ✦ ❯ locust

::


    [2022-10-13 17:15:52,673] uc045/WARNING/locust.main: System open file limit '1024' is below minimum setting '10000'.
    It's not high enough for load testing, and the OS didn't allow locust to increase it by itself.
    See https://github.com/locustio/locust/wiki/Installation#increasing-maximum-number-of-open-files-limit for more info.
    [2022-10-13 17:15:52,673] uc045/INFO/locust.main: Starting web interface at http://0.0.0.0:8089 (accepting connections from all network interfaces)
    [2022-10-13 17:15:52,683] uc045/INFO/locust.main: Starting Locust 2.12.1


.. warning:: System open file limit '1024' is below minimum setting '10000'.

   It's not high enough for load testing, and the OS didn't allow locust
   to increase it by itself.


.. figure:: page_localhost_locust.png
   :align: center

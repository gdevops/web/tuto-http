.. index::
   pair: Load testing ; Locust swarm
   ! locust-swarm

.. _locust_swarm:

==========================================================================================================================================================
|LocustSwarm| **Locust-swarm** (A tool for launching distributed locust runs on a set of load generators)
==========================================================================================================================================================

- https://github.com/SvenskaSpel/locust-swarm
- https://github.com/SvenskaSpel/locust-swarm/commits.atom

Description
============

A tool for launching distributed locust runs on a set of load generators.


Swarm
========

Swarm is a tool for running [locust](https://github.com/locustio/locust)
in a distributed fashion on a set of load generator servers, using SSH.

Swarm supports [locust-plugins](https://github.com/SvenskaSpel/locust-plugins),
automatically installing it on load gens if available.


Installation on the master
==============================

::


    pip install locust-swarm

Installation on the loadgens
================================


::

    pip install locust
    apt install parallel # or yum install or whatever matches your system
    # if you want to use locust-plugins:
    # pip install locust-plugins
    # as swarm automatically copies locust-plugins to loadgens every time, you can then uninstall it, leaving only its dependencies:
    # pip uninstall locust-plugins


Swarm uses SSH to launch remote processes and SSH tunnels for communication,
so you should ensure you can access the workers over ssh.

Other requirements
==========================

Servers must be running bash v4.3 or later.

.. index::
   pair: Versions ; Locust

.. _locust_versions:

===================================
**locust versions**
===================================

- https://github.com/locustio/locust/releases
- https://github.com/locustio/locust/graphs/contributors

.. toctree::
   :maxdepth: 3

   2.15.1/2.15.1
   2.15.0/2.15.0
   2.13.0/2.13.0
   2.8.0/2.8.0
   2.0.0/2.0.0
   1.0/1.0
   0.8.0/0.8.0

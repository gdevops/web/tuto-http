.. index::
   pair: 0.8.0 (2017-09-19) ; Locust

.. _locust_0_8_0:

===================================
**locust 0.8.0 (2017-09-19)**
===================================

- https://github.com/locustio/locust/releases/tag/v0.8

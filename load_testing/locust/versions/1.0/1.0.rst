.. index::
   pair: 1.0.0 (2020-05-25);  Locust

.. _locust_1_0_0:

===================================
**locust 1.0 (2020-05-25)**
===================================

- https://github.com/locustio/locust/releases/tag/1.0

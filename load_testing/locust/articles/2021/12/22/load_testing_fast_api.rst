
.. _fastapi_locust_2021_05_18:

================================================================================================================
2021-12-22 **Load Testing FastAPI with Locust Python**
================================================================================================================

- https://www.youtube.com/watch?v=esIEW0aEKqk
- https://blog.jcharistech.com/2021/10/10/load-testing-streamlit-apps-with-locust-python/

In this tutorial we will explore how to use Locust to perform load testing
of an NLP API service built with FastAPI.


- 📝 Written Tutorial https://blog.jcharistech.com/2021/10/10/load-testing-streamlit-apps-with-locust-python/
- 📺 Become a Patron:https://www.patreon.com/jcharistech


.. _django_locust_2021_07_31:

=======================================================================================================================
**2021-07-31 DjangoCon 2021 | Load Testing a Django Application using LocustIO by Pranjal Jain & Vibhash Chandra**
=======================================================================================================================


- https://www.youtube.com/watch?v=2rvsOQrbLuc


.. figure:: images/testing_django_locust.png
   :align: center

Introduction
==============

Fed up of using existing tools for determining benchmark and doing load
testing for your server application ? **LocustIO is present to the rescue.**

LocustIO is an easy-to-use, distributed, user load testing tool.

It is intended for load-testing web sites (or other systems) and figuring
out how many concurrent users a system can handle.

Using Locust you will be able to determine the system performance at
different endpoints in very simple and efficient way.

This will provide you a rough idea on how many requests per second is
supported by your application.

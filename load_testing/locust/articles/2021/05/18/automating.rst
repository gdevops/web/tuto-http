
.. _automating_locust_2021_05_18:

================================================================================================================
2021-05-18 **Automating & evaluating load testing with Locust and Keptn - Keptn User Group**
================================================================================================================

- https://www.youtube.com/watch?v=Ok4x2LIbEEY&t=194s

.. figure:: images/keptn_locust.png
   :align: center
   :width: 400


Introduction
==============

Learn how to use Locust load testing and how to write powerful load tests
in a simple way.

**We will then use Keptn to have the tests automatically executed as part
of our CD** and **Keptn Quality Gates to evaluate the impact of the tests
on our applications**.

We even build a self-service platform for developers to be able to
trigger deployment, tests, and evaluations for new version of their applications.


Presenters:

- |LarsHolmberg| Lars Holmberg, Redshirt (https://x.com/cyberw)
- Jürgen Etzlstorfer, Dynatrace/Keptn (https://github.com/jetzlstorfer)

Timecodes:

- 00:00 Introduction
- 01:35 What is Locust?
- 25:26 Locust + Keptn
- 31:56 Demo of Locust + Keptn
- 47:55 Q&A and closing remarks


Document
==========


Meeting notes:

- https://docs.google.com/document/d/1Om9pj16hGKP_w2vUaH-7Cp0ffEIj-Oe3IezeVCpFYAM/edit

- Learn more: https://keptn.sh
- Get started with tutorials: https://tutorials.keptn.sh
- Join us in Slack: https://slack.keptn.sh
- Star us on Github: https://github.com/keptn/keptn
- Follow us on Twitter: https://x.com/keptnProject
- Sign up to our community newsletter: https://keptn.sh/community/newsletter/

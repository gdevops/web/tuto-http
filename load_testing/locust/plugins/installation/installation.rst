
.. _locust_plugins_installation:

===================================
**locust plugins installation**
===================================


with poetry
============

::

    poetry add locust-plugins

Then just import locust_plugins in your locustfile and use whatever
plugins you need (see below)


Python libraries
===================

::

    poetry show --tree

::


    ipython 8.5.0 IPython: Productive Interactive Computing
    ├── appnope *
    ├── backcall *
    ├── colorama *
    ├── decorator *
    ├── jedi >=0.16
    │   └── parso >=0.8.0,<0.9.0
    ├── matplotlib-inline *
    │   └── traitlets *
    ├── pexpect >4.3
    │   └── ptyprocess >=0.5
    ├── pickleshare *
    ├── prompt-toolkit >3.0.1,<3.1.0
    │   └── wcwidth *
    ├── pygments >=2.4.0
    ├── stack-data *
    │   ├── asttokens *
    │   │   └── six *
    │   ├── executing *
    │   └── pure-eval *
    └── traitlets >=5
    locust 2.12.1 Developer friendly load testing framework
    ├── configargparse >=1.0
    ├── flask >=2.0.0
    │   ├── click >=8.0
    │   │   └── colorama *
    │   ├── itsdangerous >=2.0
    │   ├── jinja2 >=3.0
    │   │   └── markupsafe >=2.0
    │   └── werkzeug >=2.2.2
    │       └── markupsafe >=2.1.1 (circular dependency aborted here)
    ├── flask-basicauth >=0.2.0
    │   └── flask *
    │       ├── click >=8.0
    │       │   └── colorama *
    │       ├── itsdangerous >=2.0
    │       ├── jinja2 >=3.0
    │       │   └── markupsafe >=2.0
    │       └── werkzeug >=2.2.2
    │           └── markupsafe >=2.1.1 (circular dependency aborted here)
    ├── flask-cors >=3.0.10
    │   ├── flask >=0.9
    │   │   ├── click >=8.0
    │   │   │   └── colorama *
    │   │   ├── itsdangerous >=2.0
    │   │   ├── jinja2 >=3.0
    │   │   │   └── markupsafe >=2.0
    │   │   └── werkzeug >=2.2.2
    │   │       └── markupsafe >=2.1.1 (circular dependency aborted here)
    │   └── six *
    ├── gevent >=20.12.1
    │   ├── cffi >=1.12.2
    │   │   └── pycparser *
    │   ├── greenlet >=1.1.3,<2.0
    │   ├── setuptools *
    │   ├── zope-event *
    │   │   └── setuptools * (circular dependency aborted here)
    │   └── zope-interface *
    │       └── setuptools * (circular dependency aborted here)
    ├── geventhttpclient >=2.0.2
    │   ├── brotli *
    │   ├── certifi *
    │   ├── gevent >=0.13
    │   │   ├── cffi >=1.12.2
    │   │   │   └── pycparser *
    │   │   ├── greenlet >=1.1.3,<2.0
    │   │   ├── setuptools *
    │   │   ├── zope-event *
    │   │   │   └── setuptools * (circular dependency aborted here)
    │   │   └── zope-interface *
    │   │       └── setuptools * (circular dependency aborted here)
    │   └── six *
    ├── msgpack >=0.6.2
    ├── psutil >=5.6.7
    ├── pywin32 *
    ├── pyzmq >=22.2.1,<23.0.0 || >23.0.0
    │   ├── cffi *
    │   │   └── pycparser *
    │   └── py *
    ├── requests >=2.23.0
    │   ├── certifi >=2017.4.17
    │   ├── charset-normalizer >=2,<3
    │   ├── idna >=2.5,<4
    │   └── urllib3 >=1.21.1,<1.27
    ├── roundrobin >=0.0.2
    ├── typing-extensions >=3.7.4.3
    └── werkzeug >=2.0.0
        └── markupsafe >=2.1.1

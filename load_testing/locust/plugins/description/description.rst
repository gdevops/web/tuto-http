.. index::
   pair: Description; Locust plugins

.. _locust_plugins_desc:

===================================
**locust plugins description**
===================================

- https://github.com/SvenskaSpel/locust-plugins
- https://github.com/SvenskaSpel/locust-plugins/tree/master/locust_plugins

Description
============

Locust Plugins

Build Status license PyPI PyPI GitHub contributors

The purpose of this project is to gather a curated set of plugins/extensions
for Locust.

Locust itself is a **bare bones load generation tool** (compared to for
example JMeter or Gatling) and it is left to the user to build even basic
functionality (like reading test data from a database, using non-HTTP
protocols, etc).

This keeps Locust lean and mean, but forcing everyone to reinvent the
wheel is not good either.

So I (Lars Holmber) |LarsHolmberg|  (https://github.com/cyberw) decided to publish my
own plugins and hope that others (maybe you ?) will contribute their
solutions to common Locust use cases.

Having this separate from "Locust core" allows the plugins to evolve faster
(at the expense of being less mature), and avoids bloating Locust with
functionality you might not be interested in.

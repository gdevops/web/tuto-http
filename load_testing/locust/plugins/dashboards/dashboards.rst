.. index::
   pair: dashboards; plugin
   pair: Installation; PostgreSQL TimescaleDB
   pair: Extension; PostgreSQL TimescaleDB
   pair: docker-compose; PostgreSQL TimescaleDB


.. _locust_dashboards:

===============================================================================
**Installation de dashboards avec PostgreSQL/timescaledb + grafana**
===============================================================================

- https://github.com/SvenskaSpel/locust-plugins/tree/master/locust_plugins/dashboards
- https://docs.timescale.com/self-hosted/latest/install/


Introduction
=============

**locust-plugins** enables you to:

- log Locust's results to a :ref:`Postgres/Timescale database <postgresql:timescaledb>`
- and to analyze them using :ref:`Grafana  <tuto_sysops:grafana>` in real time, or after the test has completed.

The dashboards provide a good overview as well as the ability to drill down,
as each request is logged individually. It also logs important run events
(number of active users, ramp up finished etc) as well as aggregated results
after the test has finished. Because the data is persisted, you can also
track any changes in the performance of your system over time.

This aims to be a complete replacement for the reporting/graphing parts
of the Locust web UI, so it is often used with Locust in --headless mode.

Because Timescale is queried using regular SQL it is relatively straightforward
make your own custom dashboards or edit the existing ones.

You can even calculate your own completely custom metrics (e.g. number of
requests above a certain response time threshold).


**Installation with docker-compose**
==========================================

- https://docs.locust.io/en/stable/configuration.html#configuration-file

- :ref:`set up Timescale and Grafana (documented below <setup_timescale_and_grafana>`
- import locust_plugins in your locustfile (or any of locust_plugins underlying modules)
- Add --timescale to the command line (or set the LOCUST_TIMESCALE env var to 1,
  or add it to your `.conf files <https://docs.locust.io/en/stable/configuration.html#configuration-file>`)

LOCUST_TIMESCALE environment variable
----------------------------------------

- LOCUST_TIMESCALE

.. _setup_timescale_and_grafana:

Set up PostgreSQL/Timescale and Grafana
-----------------------------------------------------

Assuming you already have docker set up, all you need to do is run::

    locust-compose up

That will give you:

- a PostgreSQL/Timescale container (for receiving and storing data)
- and a Grafana container (for showing nice graphs based on said data)

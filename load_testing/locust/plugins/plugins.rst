.. index::
   pair: Plugins ; Locust

.. _locust_plugins:

=====================================================================================
|LocustPlugins| **locust plugins** (A set of useful plugins/extensions for Locust)
=====================================================================================

- https://github.com/SvenskaSpel/locust-plugins
- https://github.com/SvenskaSpel/locust-plugins/tree/master/locust_plugins


.. toctree::
   :maxdepth: 3

   description/description
   installation/installation
   dashboards/dashboards
   jmeter/jmeter

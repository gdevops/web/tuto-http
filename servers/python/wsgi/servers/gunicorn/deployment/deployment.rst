
.. _gunicorn_deploy:

=====================================
gunicorn deployment
=====================================


Gunicorn is a WSGI HTTP server.

It is best to use Gunicorn behind an HTTP proxy server.
**We strongly advise you to use nginx**

Here's an example to help you get started with using :ref:`nginx <nginx>`::

    server {
    listen 80;
    server_name example.org;
    access_log  /var/log/nginx/example.log;

        location / {
            proxy_pass http://127.0.0.1:8000;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        }
    }

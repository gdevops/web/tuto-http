
.. index::
   pair: Python WSGI server ; gunicorn
   ! gunicorn

.. _gunicorn:

===================================================================
gunicorn (‘Green Unicorn’ is a Python WSGI HTTP Server for UNIX)
===================================================================

.. seealso::

   - https://github.com/benoitc/gunicorn
   - https://gunicorn.org/


.. figure:: logo_gunicorn.jpg
   :align: center
   :width: 300

.. toctree::
   :maxdepth: 3

   definition/definition
   deployment/deployment
   versions/versions

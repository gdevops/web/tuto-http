
.. index::
   pair: Python ; WSGI
   ! WSGI
   ! Web Server Gateway Interface

.. _python_wsgi:

=====================================
WSGI (Web_Server_Gateway_Interface)
=====================================

.. seealso::

   - https://en.wikipedia.org/wiki/Web_Server_Gateway_Interface


.. toctree::
   :maxdepth: 3

   definition/definition
   servers/servers

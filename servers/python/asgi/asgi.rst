.. index::
   pair: Python ; ASGI
   ! ASGI

.. _python_asgi:

=============================================
ASGI Asynchronous Server Gateway Interface
=============================================

.. seealso::

   - https://asgi.readthedocs.io/en/latest/
   - https://asgi.readthedocs.io/en/latest/specs/index.html
   - https://github.com/django/asgiref/blob/master/specs/asgi.rst
   - https://github.com/florimondmanca/awesome-asgi
   - https://asgi.readthedocs.io/en/latest/specs/main.html#overview
   - https://asgi.readthedocs.io/en/latest/specs/www.html
   - https://discuss.encode.io/
   - https://discuss.encode.io/top/yearly


.. figure:: asgi_diagram.png
   :align: center

   How ASGI *really* works

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions
   servers/servers
   tutorials/tutorials

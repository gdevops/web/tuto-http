
.. index::
   pair: ASGI ; Versions

.. _asgi_versions:

=============================================
ASGI versions
=============================================

.. seealso::

   - https://github.com/django/asgiref/releases


.. toctree::
   :maxdepth: 3

   3.1.2/3.1.2
   3.0.0/3.0.0

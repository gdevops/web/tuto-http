.. raw:: html

   <p align="center">

.. raw:: html

   </p>

.. raw:: html

   <p align="center">

The lightning-fast ASGI server.

.. raw:: html

   </p>

--------------

|Build Status| |Coverage| |Package version|

**Documentation**: https://www.uvicorn.org

**Requirements**: Python 3.5, 3.6, 3.7

Uvicorn is a lightning-fast ASGI server implementation, using
`uvloop <https://github.com/MagicStack/uvloop>`__ and
`httptools <https://github.com/MagicStack/httptools>`__.

Until recently Python has lacked a minimal low-level server/application
interface for asyncio frameworks. The `ASGI
specification <https://github.com/django/asgiref/blob/master/specs/asgi.rst>`__
fills this gap, and means we're now able to start building a common set
of tooling usable across all asyncio frameworks.

Uvicorn currently supports HTTP/1.1 and WebSockets. Support for HTTP/2
is planned.

Quickstart
----------

Install using ``pip``:

.. code:: shell

    $ pip install uvicorn

Create an application, in ``example.py``:

.. code:: python

    async def app(scope, receive, send):
        assert scope['type'] == 'http'

        await send({
            'type': 'http.response.start',
            'status': 200,
            'headers': [
                [b'content-type', b'text/plain'],
            ],
        })
        await send({
            'type': 'http.response.body',
            'body': b'Hello, world!',
        })

Run the server:

.. code:: shell

    $ uvicorn example:app

--------------

.. raw:: html

   <p align="center">

Uvicorn is BSD licensed code.Designed & built in Brighton, England.— 🦄 —

.. raw:: html

   </p>

.. |Build Status| image:: https://travis-ci.org/encode/uvicorn.svg?branch=master
   :target: https://travis-ci.org/encode/uvicorn
.. |Coverage| image:: https://codecov.io/gh/encode/uvicorn/branch/master/graph/badge.svg
   :target: https://codecov.io/gh/encode/uvicorn
.. |Package version| image:: https://badge.fury.io/py/uvicorn.svg
   :target: https://pypi.python.org/pypi/uvicorn

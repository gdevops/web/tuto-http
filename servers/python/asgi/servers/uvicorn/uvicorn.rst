.. index::
   pair: uvicorn ; ASGI HTTP Server
   ! uvicorn

.. _uvicorn:

===============================================
**uvicorn** The lightning-fast ASGI server
===============================================

.. seealso::

   - https://github.com/encode/uvicorn
   - https://github.com/encode/uvicorn/graphs/contributors
   - https://discuss.encode.io/c/uvicorn/7
   - https://www.uvicorn.org/

.. figure:: uvicorn.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions

.. index::
   pair: uvicorn ; Versions

.. _uvicorn_versions:

=====================================
uvicorn versions
=====================================

.. seealso::

   - https://github.com/encode/uvicorn/releases

.. toctree::
   :maxdepth: 3

   0.11.8/0.11.8
   0.7.0/0.7.0

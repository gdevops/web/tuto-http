
.. index::
   pair: starlette ; ASGI HTTP Server
   ! starlette

.. _starlette:

===============================================================================
**starlette** (The little ASGI framework that shines)
===============================================================================

.. seealso::

   - https://github.com/encode/starlette
   - https://github.com/encode/starlette/graphs/contributors
   - https://www.starlette.io/

.. toctree::
   :maxdepth: 3

   versions/versions

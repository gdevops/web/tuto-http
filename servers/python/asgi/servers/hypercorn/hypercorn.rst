
.. index::
   pair: hypercorn ; ASGI HTTP Server
   ! hypercorn

.. _hypercorn:

===============================================================================
**hypercorn** (ASGI Server based on Hyper libraries and inspired by Gunicorn)
===============================================================================

.. seealso::

   - https://gitlab.com/pgjones/hypercorn

.. figure:: logo_hypercorn.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions

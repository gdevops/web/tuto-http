

.. _caddy2_announce:

=======================================================================
caddy 2 announce
=======================================================================

.. seealso::

   - https://caddyserver.com/blog/announcing-caddy-1_0-caddy-2-caddy-enterprise


Caddy 2 and Caddy Enterprise

Several months ago, we quietly began designing and building the next generation
of Caddy.

Caddy 2 development is well underway.

Learning from years of experience, and by carefully observing shifts in the
web server and microservice industries, we fully expect Caddy 2 to be one of
the world's most powerful platforms for web services.


In addition, Caddy 2 will serve as the core of Caddy Enterprise, which will be
the premier web server and reverse proxy for businesses.

As an extension of the open source project, Caddy Enterprise will sport
features needed by modern, fast-moving tech companies thanks to its
unparalleled modular design.

Caddy Enterprise will ship with a web UI for dynamic fleet configuration,
metrics, and more.

We plan on making Caddy 2 public later this year. We will unveil and demo an
early prototype at Velocity Conf 2019, so look for our booth there!

If your company wants to request specific features and help test early versions
before then, let us know — we value your feedback, which can dramatically
impact the final result!

We are being careful not to develop Caddy 2 in isolation.

Every design decision is being tailored to the requirements of our users
(plus a little of what we think is awesome sprinkled on top).

We're Listening
==================

We are thrilled about today's announcements, and what they mean for the future
of your sites! Stay tuned for more updates on Caddy 2 and Caddy Enterprise,
which are now our top priorities.

We're listening to you! Request features or discuss what you'd like to see in
Caddy 2 and Caddy Enterprise by posting on our forum at caddy.community.

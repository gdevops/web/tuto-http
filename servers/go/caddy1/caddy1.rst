
.. index::
   pair: HTTP Server; caddy1
   ! caddy1

.. _caddy1:

=======================================================================
caddy 1 (Fast, cross-platform HTTP/2 web server with automatic HTTPS)
=======================================================================

.. seealso::

   - https://caddyserver.com/
   - https://github.com/mholt/caddy
   - https://caddy.community/
   - https://x.com/mholt6
   - https://matt.life/papers/after_https_thesis.pdf
   - https://x.com/caddyserver


.. figure:: caddy1_logo.png
   :align: center
   :width: 300


.. toctree::
   :maxdepth: 3

   versions/versions

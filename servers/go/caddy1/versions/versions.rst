
.. index::
   pair: Versions; caddy

.. _caddy_versions:

=======================================================================
caddy versions
=======================================================================

.. seealso::

   - https://github.com/mholt/caddy/releases

.. toctree::
   :maxdepth: 3

   1.0.0/1.0.0

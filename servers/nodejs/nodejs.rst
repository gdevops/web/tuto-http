
.. index::
   pair: HTTP Server; nodejs
   ! nodejs HTTP server

.. _nodejs_http_server:

===========================================
nodejs HTTP server
===========================================

.. seealso::

   - :ref:`nodejs_def`

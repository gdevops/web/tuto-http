
.. index::
   pair: HTTP ; Servers
   ! HTTP servers

.. _http_servers:

=====================================
Servers
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/Serveur_HTTP


.. toctree::
   :maxdepth: 6

   c/c
   go/go
   nodejs/nodejs
   python/python

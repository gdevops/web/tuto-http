
.. index::
   pair: HTTP Server; nginx
   ! nginx

.. _nginx_http_server:
.. _nginx:

===========================================
nginx /ɛndʒɪnˈɛks/ EN-jin-EKS HTTP server
===========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Nginx
   - https://hg.nginx.org/nginx/
   - https://github.com/nginx/nginx (mirror)
   - https://x.com/nginxorg
   - https://hub.docker.com/_/nginx/
   - :ref:`nginx_http_server`


.. figure:: Nginx_logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 6

   definition/definition
   versions/versions

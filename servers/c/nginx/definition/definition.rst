

.. _nginx_http_def:

===========================================
nginx definition
===========================================

.. seealso::

   - :ref:`nginx_balancer_def`
   - https://gdevops.frama.io/opsindev/sysops/load_balancing/nginx/nginx.html

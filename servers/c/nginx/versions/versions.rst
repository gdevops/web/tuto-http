
.. index::
   pair: Versions ; nginx

.. _nginx_versions:

=====================================
nginx versions
=====================================

.. seealso::

   - https://nginx.org/en/CHANGES


.. toctree::
   :maxdepth: 3

   1.18.0/1.18.0
   1.15.11/1.15.11

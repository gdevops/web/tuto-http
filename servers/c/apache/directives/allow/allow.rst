
.. index::
   pair: Apache ; Allow

.. _Allow:

===============
Allow
===============

.. seealso::

   - https://httpd.apache.org/docs/2.2/mod/mod_authz_host.html#allow



Description
=============

La directive Allow permet de contrôler quels hôtes peuvent accéder à une zone
du serveur.

On peut contrôler l'accès en fonction du nom d'hôte, de l'adresse IP, d'un
intervalle d'adresses IP, ou d'autres caractéristiques de la requête client
enregistrées dans des variables d'environnement.

Le premier argument de cette directive est toujours **from**.

Les arguments suivants peuvent se présenter sous trois formes.

Si Allow from all est spécifié, tous les hôtes ont l'autorisation d'accès,
sauf si la configuration des directives Deny et Order apporte une restriction
quelconque, comme décrit plus loin.

Afin de n'accorder l'accès au serveur qu'à certains hôtes ou groupes d'hôtes,
on peut utiliser l'argument hôte sous les formes suivantes :

Un nom de domaine, éventuellement partiel
-------------------------------------------

Exemple::

    Allow from apache.org
    Allow from .net exemple.edu

Les hôtes dont les noms correspondent à la chaîne spécifiée, ou se terminant
par elle, sont autorisés à accéder.

Seuls les éléments complets du nom de domaine sont comparés, si bien que
l'exemple ci-dessus correspondra à foo.apache.org, mais pas à fooapache.org.

Avec cette configuration, Apache va effectuer une double recherche DNS sur
l'adresse IP du client, sans tenir compte de la définition de la directive
HostnameLookups.

Il effectue tout d'abord une recherche DNS inverse sur l'adresse IP afin de
déterminer le nom d'hôte associé, puis une recherche directe sur le nom
d'hôte pour vérifier s'il correspond bien à l'adresse IP originale.

L'accès ne sera accordé que si le nom d'hôte correspond à la chaîne spécifiée
et si les recherches DNS inverse et directe produisent un résultat cohérent.

Une adresse IP complète
--------------------------

Exemple::

    Allow from 10.1.2.3
    Allow from 192.168.1.104 192.168.1.205


L'adresse IP d'un hôte qui a l'autorisation d'accès


Une adresse IP partielle
-------------------------

    Exemple :

    Allow from 10.1
    Allow from 10 172.20 192.168.2

Les 1 à 3 premiers octets d'une adresse IP, pour référencer un sous-réseau.


Une paire réseau/masque de sous-réseau
----------------------------------------

Exemple::

    Allow from 10.1.0.0/255.255.0.0

Un réseau a.b.c.d, et un masque de sous-réseau w.x.y.z. pour la restriction plus
fine d'un sous-réseau.

Une spécification CIDR réseau/nnn
----------------------------------

Exemple::

    Allow from 10.1.0.0/16

Identique au cas précédent, mais le masque de sous-réseau se compose des nnn
bits de poids forts.

Notez que les trois derniers exemples référencent exactement le même ensemble
d'hôtes.

On peut spécifier des adresses et sous-réseaux IPv6 de la manière suivante::

	Allow from 2001:db8::a00:20ff:fea7:ccea
	Allow from 2001:db8::a00:20ff:fea7:ccea/10

Le troisième format d'arguments de la directive Allow permet de contrôler l'accès
au serveur en fonction de l'existence d'une variable d'environnement.

Lorsque Allow from env=var-env est spécifié, la requête est autorisée si la
variable d'environnement var-env existe.

Lorsque Allow from env=!var-env est spécifié, la requête est autorisée à
accéder si la variable d'environnement var-env n'existe pas.

Le serveur fournit la possibilité de définir des variables d'environnement
avec une grande souplesse en fonction des caractéristiques de la requête
client à l'aide des directives fournies par le module mod_setenvif.

On peut ainsi utiliser cette directive pour contrôler l'accès en fonction de
certains en-têtes comme User-Agent (type de navigateur), Referer, etc...

Exemple::

	SetEnvIf User-Agent ^KnockKnock/2\.0 laisse_moi_entrer
	<Directory /docroot>
	Order Deny,Allow
	Deny from all
	Allow from env=laisse_moi_entrer
	</Directory>

Dans cet exemple, les navigateurs dont la chaîne de description de l'en-tête
user-agent commence par KnockKnock/2.0 se verront accorder l'accès, alors que
tous les autres se le verront refuser.

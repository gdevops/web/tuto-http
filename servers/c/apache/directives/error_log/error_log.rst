.. index::
   pair: Apache2 ; ErrorLog
   pair: ErrorLog ; Apache


.. _ErrorLog_directive:

=========================================================================
Directive ``ErrorLog``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#errorlog
   - https://httpd.apache.org/docs/current/logs.html




Description
==============

La directive ErrorLog permet de définir le nom du fichier dans lequel le
serveur va journaliser toutes les erreurs qu'il rencontre.

Si le chemin fichier n'est pas absolu, il est considéré comme relatif au
chemin défini par la directive ServerRoot::

    ErrorLog "/var/log/httpd/error_log"

Si le chemin fichier commence par une barre verticale "(|)", il est considéré
comme une commande à lancer pour traiter la journalisation de l'erreur::

    ErrorLog "|/usr/local/bin/httpd_errors"

Voir les notes à propos des journaux redirigés pour plus d'informations.

L'utilisation de syslog à la place d'un nom de fichier active la journalisation
via syslogd(8) si le système le supporte.

Le dispositif syslog par défaut est local, mais vous pouvez le modifier à
l'aide de la syntaxe syslog:facility, où facility peut être remplacé par un
des noms habituellement documentés dans la page de man syslog(1).

Le dispositif syslog local7 est global, et si il est modifié dans un serveur
virtuel, le dispositif final spécifié affecte l'ensemble du serveur::

    ErrorLog syslog:user

Des modules supplémentaires peuvent fournir leurs propres fournisseurs ErrorLog.
La syntaxe est similaire à celle de l'exemple syslog ci-dessus.

SECURITE
=========

Voir le document conseils à propos de sécurité pour des détails sur
les raisons pour lesquelles votre sécurité peut être compromise si le répertoire
contenant les fichiers journaux présente des droits en écriture pour tout autre
utilisateur que celui sous lequel le serveur est démarré.

Note
=====

Lors de la spécification d'un chemin de fichier sur les plates-formes non-Unix,
on doit veiller à n'utiliser que des slashes (/), même si la plate-forme
autorise l'utilisation des anti-slashes (\).

Et d'une manière générale, il est recommandé de n'utiliser que des slashes (/)
dans les fichiers de configuration.

.. index::
   pair: Apache2 ; LoadModule


.. _LoadModule:

=========================================================================
Directive LoadModule  (module mod_so)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/mod_so.html#loadmodule




Introduction
==============

La directive LoadModule permet de lier le fichier objet ou la bibliothèque
nom-fichier avec le serveur, et d'ajouter la structure de module nommée module
à la liste des modules actifs.

module est le nom de la variable externe de type module dans le fichier, et est
référencé comme Identificateur de module dans la documentation des modules.

Exemple::

    LoadModule status_module modules/mod_status.so

charge le module spécifié depuis le sous-répertoire des modules situé à la
racine du serveur.


Exemple pour mod_wsgi pour CentOS python27

::

    LoadModule wsgi_module /etc/httpd/modules/mod_wsgi-py27.so

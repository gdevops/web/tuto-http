.. index::
   pair: Apache2 ; Directives


.. _directives_apache2:

=========================================================================
Directives Apache2
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/directives.html
   - https://httpd.apache.org/docs/current/mod/quickreference.html
   - https://httpd.apache.org/docs/current/mod/directive-dict.html


.. toctree::
   :maxdepth: 3

   authtype/authtype
   authname/authname
   authuserfile/authuserfile
   allow/allow
   deny/deny
   location/location
   require/require
   satisfy/satisfy
   alias
   custom_log/custom_log
   directory
   document_root
   error_log/error_log
   files
   load_module
   log_level
   server_admin
   server_alias
   server_name
   server_root
   setenv
   virtualhost

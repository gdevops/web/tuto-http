.. index::
   pair: Apache2 ; LogLevel
   pair: Logs ; Apache


.. _LogLevel:

=========================================================================
Directive ``LogLevel``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#loglevel
   - https://httpd.apache.org/docs/current/logs.html
   - https://httpd.apache.org/docs/current/mod/mod_log_config.html




Description
==============

La configuration du niveau de journalisation par module et par répertoire est
disponible depuis la version 2.3.6 du serveur HTTP Apache

La directive LogLevel permet d'ajuster la verbosité des messages enregistrés
dans les journaux d'erreur (voir la directive ErrorLog directive).

Les niveaux disponibles sont présentés ci-après, par ordre de criticité
décroissante :

.. figure:: log_level/levels.png
   :align: center


Logs apache
============

.. seealso::

   - https://httpd.apache.org/docs/current/logs.html

Pour véritablement gérer un serveur web, il est nécessaire de disposer d'un
retour d'informations à propos de l'activité et des performances du serveur,
ainsi que de tout problème qui pourrait survenir.

Le serveur HTTP Apache propose des fonctionnalités de journalisation souples
et très complètes.

Ce document décrit comment configurer ces fonctionnalités de journalisation et
interpréter le contenu des journaux.

.. index::
   pair: Apache2 ; Files


.. _Files:

=========================================================================
Directive ``Files``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#files




Description
==============

La directive <Files> limite la portée des directives qu'elle contient aux
fichiers précisés.

Elle est comparable aux directives <Directory> et <Location>.

Elle doit se terminer par une balise </Files>.

Les directives contenues dans cette section s'appliqueront à tout objet dont
le nom de base (la dernière partie du nom de fichier) correspond au fichier
spécifié.

Les sections <Files> sont traitées selon l'ordre dans lequel elles apparaissent
dans le fichier de configuration, après les sections <Directory> et la lecture
des fichiers .htaccess, mais avant les sections <Location>.

Notez que les sections <Files> peuvent être imbriquées dans les sections
<Directory> afin de restreindre la portion du système de fichiers à laquelle
ces dernières vont s'appliquer.

::

    L'argument filename peut contenir un nom de fichier ou une chaîne de caractères
    avec caractères génériques, où ? remplace un caractère, et * toute chaîne de caractères.

::

    <Files "cat.html">
        # Insérer ici des directives qui s'appliquent au fichier cat.html
    </Files>

    <Files "?at.*">
        # Les directives insérées ici s'appliqueront aux fichiers
        # cat.html, bat.html, hat.php, et ainsi de suite.
    </Files>

On peut aussi utiliser les Expressions rationnelles en ajoutant la caractère ~.

Par exemple::

    <Files ~ "\.(gif|jpe?g|png)$">
        #...
    </Files>

correspondrait à la plupart des formats graphiques de l'Internet.


Il est cependant préférable d'utiliser la directive <FilesMatch>.

Notez qu'à la différence des sections <Directory> et <Location>, les sections
<Files> peuvent être utilisées dans les fichiers .htaccess.

Ceci permet aux utilisateurs de contrôler l'accès à leurs propres ressources,
fichier par fichier.

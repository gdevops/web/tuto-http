.. index::
   pair: Apache2 ; ServerAdmin


.. _ServerAdmin:

=========================================================================
Directive ``ServerAdmin``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#serveradmin
   - https://httpd.apache.org/docs/current/vhosts/name-based.html




Description
==============

La directive ServerAdmin permet de définir l'adresse de contact que le serveur
va inclure dans tout message d'erreur qu'il envoie au client.

Si le programme httpd ne reconnait pas l'argument fourni comme une URL, il
suppose que c'est une adresse électronique, et lui ajoute le préfixe mailto:
dans les cibles des hyperliens. Il est cependant recommandé d'utiliser
exclusivement une adresse électronique, car de nombreux scripts CGI considèrent
ceci comme implicite. Si vous utilisez une URL, elle doit pointer vers un
autre serveur que vous contrôlez. Dans le cas contraire, les utilisateurs
seraient dans l'impossibilité de vous contacter en cas de problème.

Il peut s'avérer utile de définir une adresse dédiée à l'administration du
serveur, par exemple::

    ServerAdmin www-admin@foo.example.com

car les utilisateurs ne mentionnent pas systématiquement le serveur dont
ils parlent !


.. index::
   pair: Apache ; AuthName

.. _AuthName:

=========
AuthName
=========

.. seealso::

   - https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html#authname


Message will be appeared on credentials window.

Cette directive permet de définir l'identifiant d'autorisation pour un
répertoire.

Cet identifiant est fourni au client de façon à ce qu'il sache quels nom
d'utilisateur et mot de passe envoyer.

AuthName accepte un seul argument ; s'il contient des espaces, il doit être
entouré de guillemets.

Pour pouvoir fonctionner, la directive AuthName doit être utilisée en combinaison
avec les directives AuthType et Require, ainsi que des directives comme
AuthUserFile et AuthGroupFile.

Par exemple::

    AuthName "Top Secret"

La chaîne fournie comme argument à AuthName apparaîtra dans la boîte de dialogue
d'authentification pour la plupart des navigateurs.

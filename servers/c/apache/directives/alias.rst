.. index::
   pair: Apache2 ; Alias


.. _Alias:

=========================================================================
Directive Alias  (module mod_alias)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/mod_alias.html#alias




Introduction
==============

La directive Alias permet de stocker des documents (destinés à être servis)
dans des zones du système de fichiers situées en dehors de l'arborescence du
site web DocumentRoot.

Les URLs dont le chemin (décodé avec caractères %) commence par chemin URL
seront mises en correspondance avec des fichiers locaux dont le chemin commence
par chemin répertoire.

Le chemin URL est sensible à la casse, même sur les systèmes de fichiers
insensibles à la casse.

::

    Alias "/image" "/ftp/pub/image"

Une requête pour http://example.com/image/foo.gif fera renvoyer par le serveur
le fichier /ftp/pub/image/foo.gif.

Seuls les éléments de chemin complets sont testés ; ainsi l'alias précédent ne
conviendra pas pour une requête du style http://example.com/imagefoo.gif.

Pour des mises en correspondance plus complexes faisant intervenir les
expressions rationnelles, veuillez vous reporter à la directive AliasMatch.

Notez que si vous ajoutez un slash de fin au chemin URL, vous devrez aussi
ajouter un slash de fin au chemin de la requête.

Autrement dit, si vous définissez::

    Alias "/icons/" "/usr/local/apache/icons/"

l'alias précédent ne s'appliquera pas à l'URL /icons à cause de l'absence du
slash final. Ainsi, si le slash final est absent du chemin de l'URL, il doit
aussi l'être du chemin du fichier.

Notez qu'il pourra s'avérer nécessaire de définir des sections <Directory>
supplémentaires qui couvriront la destination des alias.

Le traitement des alias intervenant avant le traitement des sections <Directory>,
seules les cibles des alias sont affectées (Notez cependant que les sections
<Location> sont traitées avant les alias, et s'appliqueront donc).

En particulier, si vous créez un alias ayant pour cible un répertoire situé
en dehors de l'arborescence de votre site web DocumentRoot, vous devrez
probablement permettre explicitement l'accès à ce répertoire.

::

    Alias "/image" "/ftp/pub/image"
    <Directory "/ftp/pub/image">
        Require all granted
    </Directory>

Le nombre de slashes dans le paramètre chemin URL doit correspondre au nombre
de slashes dans le chemin URL de la requête.

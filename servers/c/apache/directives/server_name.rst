.. index::
   pair: Apache2 ; ServerName


.. _ServerName:

=========================================================================
Directive ``ServerName``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#servername
   - https://httpd.apache.org/docs/current/vhosts/name-based.html




Description
==============

Nom d'hôte et port que le serveur utilise pour s'authentifier lui-même


La directive ServerName permet de définir les protocole, nom d'hôte et port
d'une requête que le serveur utilise pour s'authentifier lui-même.

Ceci est utile lors de la création de redirections d'URLs.

La directive ServerName permet aussi (éventuellement en conjonction avec la
directive ServerAlias) d'identifier de manière unique un serveur virtuel,
lorsqu'elle est utilisée dans un contexte de serveurs virtuels à base de noms.

Par exemple, si le nom de la machine hébergeant le serveur web est
simple.example.com, la machine possède l'alias DNS www.example.com, et si
vous voulez que le serveur web s'identifie avec cet alias, vous devez
utilisez la définition suivante::

    ServerName www.example.com

La directive ServerName peut apparaître à toutes les étapes de la définition
du serveur. Toute occurrence annule cependant la précédente (pour ce serveur).

Si la directive ServerName n'est pas définie, le serveur tente de déterminer
le nom d'hôte visible du point de vue du client en effectuant une recherche
DNS inverse sur une adresse IP du serveur.

Si la directive ServerName ne précise pas de port, le serveur utilisera celui
de la requête entrante. Il est recommandé de spécifier un nom d'hôte et un
port spécifiques à l'aide de la directive ServerName pour une fiabilité optimale
et à titre préventif.

Si vous définissez des serveurs virtuels à base de nom, une directive
ServerName située à l'intérieur d'une section <VirtualHost> spécifiera quel
nom d'hôte doit apparaître dans l'en-tête de requête Host: pour pouvoir
atteindre ce serveur virtuel.

Parfois, le serveur s'exécute en amont d'un dispositif qui implémente SSL,
comme un mandataire inverse, un répartiteur de charge ou un boîtier dédié SSL.

Dans ce cas, spécifiez le protocole https:// et le port auquel les clients se
connectent dans la directive ServerName, afin de s'assurer que le serveur
génère correctement ses URLs d'auto-identification.

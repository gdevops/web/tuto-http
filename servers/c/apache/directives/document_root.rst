.. index::
   pair: Apache2 ; DocumentRoot


.. _DocumentRoot:

=========================================================================
Directive ``DocumentRoot``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#documentroot




Description
==============

Cette directive permet de définir le répertoire à partir duquel httpd va servir
les fichiers. S'il ne correspond pas à un Alias, le chemin de l'URL sera ajouté
par le serveur à la racine des documents afin de construire le chemin du
document recherché.

Exemple::

    DocumentRoot "/usr/web"

un accès à http://my.example.com/index.html se réfère alors à /usr/web/index.html.

Si chemin répertoire n'est pas un chemin absolu, il est considéré comme relatif
au chemin défini par la directive ServerRoot.

Le répertoire défini par la directive DocumentRoot ne doit pas comporter de
slash final.

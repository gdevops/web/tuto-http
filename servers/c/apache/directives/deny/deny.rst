
.. index::
   pair: Apache ; Deny

.. _Deny:

===============
Deny
===============

.. seealso::

   - https://httpd.apache.org/docs/2.2/mod/mod_authz_host.html#deny



Description
=============


Cette directive permet de restreindre l'accès au serveur en fonction du nom
d'hôte, de l'adresse IP, ou de variables d'environnement.

Les arguments de la directive Deny sont identiques à ceux de la directive Allow.


Example
========

Restrict everyone
---------------------

::

    Deny from all

.. index::
   pair: Apache2 ; ServerRoot


.. _ServerRoot:

=========================================================================
Directive ServerRoot  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#serverroot
   - https://httpd.apache.org/docs/current/misc/security_tips.html#serverroot




Introduction
==============

La directive ServerRoot permet de définir le répertoire dans lequel le serveur
est installé.

En particulier, il contiendra les sous-répertoires conf/ et logs/.

Les chemins relatifs indiqués dans les autres directives (comme Include ou
LoadModule) seront définis par rapport à ce répertoire.

::

    ServerRoot "/home/httpd"

La valeur par défaut de ServerRoot peut être modifiée via l'argument --prefix
de la commande configure, et de nombreuses distributions tierces du serveur
proposent une valeur différente de celles listées ci-dessus.

.. index::
   pair: Apache2 ; SetEnv


.. _SetEnv:

=========================================================================
Directive ``SetEnv``  (module mod_env)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/mod_env.html#setenv
   - https://httpd.apache.org/docs/current/env.html




Description
==============

Définit une variable d'environnement interne, cette dernière étant ensuite
disponible pour les modules du serveur HTTP Apache et transmise aux scripts
CGI et aux pages SSI.

Exemple::

    SetEnv SPECIAL_PATH /foo/bin

Si l'argument valeur est absent, la variable est définie à la valeur d'une
chaîne vide.

Les variables d'environnement internes définies par cette directive le sont
après l'exécution de la plupart des directives du traitement initial des
requêtes, comme les contrôles d'accès et la mise en correspondance des URIs
avec les noms de fichiers.

Si la variable d'environnement est sensée intervenir au cours de cette phase
initiale du traitement, par exemple pour la directive RewriteRule, vous devez
plutôt utiliser la directive SetEnvIf pour définir cette variable.

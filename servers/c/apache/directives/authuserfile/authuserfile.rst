
.. index::
   pair: Apache ; AuthUserFile

.. _AuthUserFile:

===============
AuthUserFile
===============

.. seealso::

   - https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html#authtype

File where users login details are saved.

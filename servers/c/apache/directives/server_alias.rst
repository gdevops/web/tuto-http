.. index::
   pair: Apache2 ; ServerAlias


.. _ServerAlias:

=========================================================================
Directive ``ServerAlias``  (module core)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/core.html#serveralias
   - https://httpd.apache.org/docs/current/vhosts/name-based.html




Description
==============

La directive ServerAlias permet de définir les noms alternatifs d'un serveur
utilisables pour atteindre des serveurs virtuels à base de nom.

La directive ServerAlias peut contenir des caractères génériques, si nécessaire.

.. code-block:: apache

    <VirtualHost *:80>
      ServerName server.example.com
      ServerAlias server server2.example.com server2
      ServerAlias *.example.com
      UseCanonicalName Off
      # ...
    </VirtualHost>

La recherche du serveur virtuel à base de nom correspondant au plus près à la
requête s'effectue selon l'ordre d'apparition des directives <virtualhost> dans
le fichier de configuration.

Le premier serveur virtuel dont le ServerName ou le ServerAlias correspond
est choisi, sans priorité particulière si le nom contient des caractères
génériques (que ce soit pour ServerName ou ServerAlias).

Tous les noms spécifiés au sein d'une section VirtualHost sont traités comme
un ServerAlias (sans caractères génériques).

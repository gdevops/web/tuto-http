
.. index::
   pair: Apache ; require

.. _require:

=========
require
=========

.. seealso::

   - https://httpd.apache.org/docs/2.4/mod/mod_authz_core.html#require


Cette directive permet de vérifier si un utilisateur authentifié a l'autorisation
d'accès accordée pour un certain fournisseur d'autorisation et en tenant
compte de certaines restrictions.

mod_authz_core met à disposition les fournisseurs d'autorisation génériques
suivants :

Require all granted
    L'accès est autorisé sans restriction.

Require all denied
    L'accès est systématiquement refusé.

Require env env-var [env-var] ...
    L'accès n'est autorisé que si l'une au moins des variables d'environnement
    spécifiées est définie.

Require method http-method [http-method] ...
    L'accès n'est autorisé que pour les méthodes HTTP spécifiées.

Require expr expression
    L'accès est autorisé si expression est évalué à vrai.

Voici quelques exemples de syntaxes autorisées par mod_authz_user,
mod_authz_host et mod_authz_groupfile :

Require user identifiant utilisateur [identifiant utilisateur] ...
    Seuls les utilisateurs spécifiés auront accès à la ressource.

Require group nom groupe [nom groupe] ...
    Seuls les utilisateurs appartenant aux groupes spécifiés auront accès à la
    ressource.

Require valid-user
    Tous les utilisateurs valides auront accès à la ressource.

Require ip 10 172.20 192.168.2
    Les clients dont les adresses IP font partie des tranches spécifiées
    auront accès à la ressource.

D'autres modules d'autorisation comme mod_authnz_ldap, mod_authz_dbm, mod_authz_dbd,
mod_authz_owner et mod_ssl implémentent des options de la directive Require.

Pour qu'une configuration d'authentification et d'autorisation fonctionne
correctement, la directive Require doit être accompagnée dans la plupart des
cas de directives AuthName, AuthType et AuthBasicProvider ou AuthDigestProvider,
ainsi que de directives telles que AuthUserFile et AuthGroupFile (pour la
définition des utilisateurs et des groupes).

Exemple::

	AuthType Basic
	AuthName "Restricted Resource"
	AuthBasicProvider file
	AuthUserFile "/web/users"
	AuthGroupFile "/web/groups"
	Require group admin

Les contrôles d'accès appliqués de cette manière sont effectifs pour toutes les
méthodes. C'est d'ailleurs ce que l'on souhaite en général.

Si vous voulez n'appliquer les contrôles d'accès qu'à certaines méthodes, tout
en laissant les autres méthodes sans protection, placez la directive Require
dans une section <Limit>.

Le résultat de la directive Require peut être inversé en utilisant l'option not.

Comme dans le cas de l'autre directive d'autorisation inversée <RequireNone>,
si la directive Require est inversée, elle ne peut qu'échouer ou produire un
résultat neutre ; elle ne peut donc alors pas autoriser une requête de manière
indépendante.

Dans l'exemple suivant, tous les utilisateurs appartenant aux groupes alpha
et beta ont l'autorisation d'accès, à l'exception de ceux appartenant
au groupe reject.

::

	<Directory "/www/docs">
		<RequireAll>
		    Require group alpha beta
		    Require not group reject
		</RequireAll>
	</Directory>

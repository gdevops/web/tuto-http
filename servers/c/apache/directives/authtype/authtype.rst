
.. index::
   pair: Apache ; authtype

.. _AuthType:

=========
AuthType
=========

.. seealso::

   - https://httpd.apache.org/docs/2.4/mod/mod_authn_core.html#authtype

Type of authentication to be used.

Cette directive permet de définir le type d'authentification utilisateur pour
un répertoire.

Les types d'authentification disponibles sont None, Basic (implémenté par mod_auth_basic),
Digest (implémenté par mod_auth_digest), et Form (implémenté par mod_auth_form).

Pour mettre en oeuvre l'authentification, vous devez aussi utiliser les directives
AuthName et Require.

De plus, le serveur doit pouvoir disposer d'un module fournisseur d'authentification
comme mod_authn_file et d'un module d'autorisation comme mod_authz_user.

Le type d'authentification None désactive l'authentification.

Lorsqu'une authentification est définie, elle est en général héritée par chacune
des sections de configuration qui suivent, à moins qu'un autre type
d'authentification ne soit spécifié.

Si l'on ne souhaite pas mettre en oeuvre d'authentification pour une sous-section
d'une section authentifiée, on doit utiliser le type d'authentification None ;

dans l'exemple suivant, les clients peuvent accéder au répertoire /www/docs/public
sans devoir s'authentifier

::

	<Directory "/www/docs">
		AuthType Basic
		AuthName Documents
		AuthBasicProvider file
		AuthUserFile "/usr/local/apache/passwd/passwords"
		Require valid-user
	</Directory>

	<Directory "/www/docs/public">
		AuthType None
		Require all granted
	</Directory>

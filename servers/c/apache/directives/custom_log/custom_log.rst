.. index::
   pair: Log ; CustomLog
   pair: combined ; Log


.. _CustomLog:

=========================================================================
Directive ``CustomLog``  (module mod_log_config)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/mod/mod_log_config.html
   - https://httpd.apache.org/docs/current/en/mod/mod_log_config.html




Sommaire
==========

Ce module apporte une grande souplesse dans la journalisation des requêtes
des clients.

Les journaux sont écrits sous un format personnalisable, et peuvent être
enregistrés directement dans un fichier, ou redirigés vers un programme externe.

La journalisation conditionnelle est supportée, si bien que des requêtes
individuelles peuvent être incluses ou exclues des journaux en fonction
de leur caractéristiques.

Ce module fournit trois directives:

- TransferLog crée un fichier journal,
- LogFormat définit un format personnalisé,
- et CustomLog définit un fichier journal et un format en une seule étape.

Pour journaliser les requêtes dans plusieurs fichiers, vous pouvez utiliser
plusieurs fois les directives TransferLog et CustomLog dans chaque serveur.


Exemples
===========

Quelques chaînes de format couramment utilisées:

Format de journal courant (CLF)
    "%h %l %u %t \"%r\" %>s %b"

Format de journal courant avec un serveur virtuel
    "%v %h %l %u %t \"%r\" %>s %b"

Format de journal NCSA étandu/combiné
    "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-agent}i\""

Format de journal de la page qui contient le lien vers la page concernée (Referer)
    "%{Referer}i -> %U"

Format de journal de l'agent (Navigateur)
    "%{User-agent}i"

Vous pouvez utiliser plusieurs fois la directive %{format}t pour construire un
format de temps utilisant les symboles de format étendus tels que msec_frac :

Format de temps prenant en compte les milisecondes
    "%{%d/%b/%Y %T}t.%{msec_frac}t %{%z}t"



Explication
===========

.. seealso:: https://www.digitalocean.com/community/tutorials/how-to-configure-logging-and-log-rotation-in-apache-on-an-ubuntu-vps


::


    CustomLog log_location log_format


The log format in this example is "combined".
This is not an internal Apache specification.

Instead, this is a label for a custom format that is defined in the default
configuration file.

If we open the default config file again, we can see the line that defines
the "combined" log format::

    sudo nano /etc/apache2/apache2.conf

::

    . . .
    LogFormat "%h %l %u %t \"%r\" %>s %O \"{Referer}i\" \"%{User-Agent}i\"" combined
    . . .


The "LogFormat" command defines a custom format for logs that can be called
using the "CustomLog" directive as we saw in the virtual host definition.

This log format specifies a format known as a "combined" format. Learn more
about available format string variables by going here.

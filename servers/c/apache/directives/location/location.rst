
.. index::
   pair: Apache ; Location

.. _location:

===============
location
===============

.. seealso::

   - https://httpd.apache.org/docs/2.4/fr/mod/core.html#location



Description
=============


La directive <Location> limite la portée des directives contenues aux URLs
définies par l'argument URL.

Elle est similaire à la directive <Directory>, et marque le début d'une section
qui se termine par une directive </Location>.

Les sections <Location> sont traitées selon l'ordre dans lequel elles
apparaissent dans le fichier de configuration, mais après les sections
<Directory> et la lecture des fichiers .htaccess, et après les sections <Files>.

Les sections <Location> agissent complètement en dehors du système de fichiers.

Ceci a de nombreuses conséquences. Parmi les plus importantes, on ne doit pas
utiliser les sections <Location> pour contrôler l'accès aux répertoires du
système de fichiers.

Comme plusieurs URLs peuvent correspondre au même répertoire du système de
fichiers, un tel contrôle d'accès pourrait être contourné.

Les directives que contient cette section seront appliquées aux requêtes si
la partie chemin de l'URL satisfait à l'un au moins de ces critères :

- Le chemin spécifié correspond exactement à la partie chemin de l'URL.
- Le chemin spécifié, qui se termine par un slash, est un préfixe de la partie
  chemin de l'URL (traité comme une racine du contexte).
- Le chemin spécifié, si on lui ajoute un slash de fin, est un préfixe de la
  partie chemin de l'URL (aussi traité comme une racine du contexte).

Dans l'exemple ci-dessous, où aucun slash de fin n'est utilisé, les directives
contenues dans la section s'appliqueront à /private1, /private1/ et /private1/file.txt,
mais pas à /private1other.

::

	<Location "/private1">
		#  ...
	</Location>

De même, dans l'exemple ci-dessous, où l'on utilise un slash de fin, les
directives contenues dans la section s'appliqueront à /private2/ et à /private2/file.txt,
mais pas à /private2other.

::

	<Location "/private2/">
		# ...
	</Location>

Quand utiliser la section <Location>

Vous pouvez utiliser une section <Location> pour appliquer des directives à des
contenus situés en dehors du système de fichiers.

Pour les contenus situés à l'intérieur du système de fichiers, utilisez plutôt
les sections <Directory> et <Files>.

<Location "/"> constitue une exception et permet d'appliquer aisément une
configuration à l'ensemble du serveur.

Pour toutes les requêtes originales (non mandatées), l'argument URL est un
chemin d'URL de la forme /chemin/.

Aucun protocole, nom d'hôte, port, ou chaîne de requête ne doivent apparaître.

Pour les requêtes mandatées, l'URL spécifiée doit être de la forme
protocole://nom_serveur/chemin, et vous devez inclure le préfixe.

L'URL peut contenir des caractères génériques. Dans une chaîne avec caractères
génériques, ? correspond à un caractère quelconque, et * à toute chaîne de
caractères.

Les caractères génériques ne peuvent pas remplacer un / dans le chemin URL.

On peut aussi utiliser les Expressions rationnelles, moyennant l'addition d'un
caractère ~. Par exemple :

::

	<Location ~ "/(extra|special)/data">
		#...
	</Location>

concernerait les URLs contenant les sous-chaîne /extra/data ou /special/data.

La directive <LocationMatch> présente un comportement identique à la version
avec expressions rationnelles de la directive <Location>, et son utilisation
est préférable à l'utilisation de cette dernière pour la simple raison
qu'il est difficile de distinguer ~ de - dans la plupart des fontes.

La directive <Location> s'utilise principalement avec la directive SetHandler.

Par exemple, pour activer les requêtes d'état, mais ne les autoriser que
depuis des navigateurs appartenant au domaine example.com, vous pouvez utiliser :

::

	<Location "/status">
	  SetHandler server-status
	  Require host example.com
	</Location>

Note à propos du slash (/)

La signification du caractère slash dépend de l'endroit où il se trouve dans
l'URL.

Les utilisateurs peuvent être habitués à son comportement dans le système de
fichiers où plusieurs slashes successifs sont souvent réduits à un slash
unique (en d'autres termes, /home///foo est identique à /home/foo).

Dans l'espace de nommage des URLs, ce n'est cependant pas toujours le cas.

Pour la directive <LocationMatch> et la version avec expressions rationnelles
de la directive <Location>, vous devez spécifier explicitement les slashes
multiples si telle est votre intention.

Par exemple, <LocationMatch "^/abc"> va correspondre à l'URL /abc mais pas à
l'URL //abc.

La directive <Location> sans expression rationnelle se comporte de la même
manière lorsqu'elle est utilisée pour des requêtes mandatées.

Par contre, lorsque la directive <Location> sans expression rationnelle
est utilisée pour des requêtes non mandatées, elle fera correspondre
implicitement les slashes multiples à des slashes uniques.

Par exemple, si vous spécifiez <Location "/abc/def">, une requête de la
forme /abc//def correspondra.

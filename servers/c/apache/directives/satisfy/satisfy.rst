
.. index::
   pair: Apache ; Satisfy

.. _Satisfy:

===============
Satisfy
===============

.. seealso::

   - https://httpd.apache.org/docs/2.2/mod/core.html#Satisfy

Cette directive permet de définir la politique d'accès lorsque les directives
Allow et Require sont utilisées conjointement.

L'argument prend pour valeur All ou Any.

Cette directive ne s'avère utile que dans le cas où l'accès à une zone
particulière est contrôlé à la fois par une authentification utilisateur/mot
de passe et par l'adresse IP du client.

Avec la valeur par défaut de l'argument (All), le client doit d'abord satisfaire
à la condition d'accès en fonction de son adresse IP, puis fournir un couple
utilisateur/mot de passe valide.

Si l'argument est Any, le client se verra accorder l'accès s'il satisfait à au
moins une des conditions d'accès : adresse IP et/ou un couple utilisateur/mot
de passe valides.

On peut utiliser cette valeur pour restreindre l'accès à une zone à l'aide
d'un mot de passe, mais laisser cette zone en accès libre pour les clients
possédant certaines adresses IP.

Par exemple, si vous souhaitez accorder un accès sans restriction à une
portion de votre site web aux clients de votre réseau, mais n'accorder cet
accès aux clients à l'extérieur de votre réseau qu'en échange d'un mot de passe,
vous pouvez utiliser une configuration de ce style::

	Require valid-user
	Order allow,deny
	Allow from 192.168.1
	Satisfy Any

Depuis la version 2.0.51, les directives Satisfy peuvent être limitées à certaines
méthodes particulières à l'aide des sections <Limit> et <LimitExcept>.

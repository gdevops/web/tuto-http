
.. index::
   pair: Digest ; Authentication
   pair: htdigest ; command



.. _apache2_digest_authentication_ex:

=========================================================================
HTTP Apache2 Digest Authentication example
=========================================================================




Virtual host
=============


Example: Virtual Host Entry


.. code-block:: apache
   :linenos:

    <Location "/var/www/crock.norang.ca/htdocs/private/">

        AuthType Digest
        AuthBasicProvider file

        AuthName "ENI private"
        AuthUserFile /var/www/crock.norang.ca/.htpasswd-private
        Require user eni


    </Location>


- AuthType : type d'authentification (mod_auth_basic)
- AuthBasicProvider : fournisseur d'authentification (mod_authn_file)
- AuthUserFile : fichier contenant les comptes utilisateur


htdigest command
=================

Le fichier défini par AuthUserFile est créé avec l'utilitaire Apache nommé
``htdigest``.

::

    sudo /opt/prod/apache2.4/bin/htdigest -c /var/www/crock.norang.ca/.htpasswd-private "ENI private" eni



Creating New Users
------------------

Example: Creating a new entry

::

    sudo /opt/prod/apache2.4/bin/htdigest -c /var/www/crock.norang.ca/.htpasswd-private "ENI private" nicolasm


::

    New password:
    Re-type new password:

This prompts for the password for newuser and stores the encrypted password
in the password file.

Example: Created password entry (part of the .htpasswd file)

::

    newuser:Po9FhxMKQJcRY


Deleting Users
----------------

You delete users from the .htpasswd access file as follows

Example: Deleting user account eni

::

    $ sudo htpasswd -D .htpasswd eni

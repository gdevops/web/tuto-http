
.. index::
   pair: Apache module; mod_auth_digest

.. _mod_auth_digest:

===========================================
Apache **mod_auth_digest** module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/mod_auth_digest.html



Définition 1
===============

Ce module à l'inverse du module mod_auth_basic, ne transmet pas les données
en clair car il utilise une authentification HTTP basée sur les condensés
MD5.

Pour l'authentification **Digest**, le module utilise un fichier défini par la
directive AuthUserFile contenant les identifiants et mots de passe
des utilisateurs créés.

Définition 2
===============

Ce module implémente l'authentification HTTP basée sur les condensés MD5 (RFC2617),
et fournit une alternative à mod_auth_basic en ne transmettant plus le mot de
passe en clair.

Cependant, cela ne suffit pas pour améliorer la sécurité de manière
significative par rapport à l'authentification basique.

.. warning:: L'authentification à base de condensé a été conçue pour améliorer
   la sécurité par rapport à l'authentification basique, mais il s'avère que
   ce but n'a pas été atteint. Un attaquant de type "man-in-the-middle" peut
   facilement forcer le navigateur à revenir à une authentification basique.
   Même une oreille indiscrète passive peut retrouver le mot de passe par force
   brute avec les moyens modernes, car l'algorithme de hashage utilisé par
   l'authentification à base de condensé est trop rapide.
   Autre problème, le stockage des mots de passe sur le serveur n'est pas sûr.
   Le contenu d'un fichier htdigest volé peut être utilisé directement pour
   l'authentification à base de condensé. Il est donc fortement recommandé
   d'utiliser mod_ssl pour chiffrer la connexion.

   mod_auth_digest ne fonctionne correctement que sur les plates-formes où APR
   supporte la mémoire partagée.




.. toctree::
   :maxdepth: 3

   example_digest/example_digest

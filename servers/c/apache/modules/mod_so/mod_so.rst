.. index::
   pair: Apache2 ; mod_so
   ! mod_so

.. _mod_so:

=========================================================================
Apache2 **mod_so**
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/mod/mod_so.html




Introduction
=============

Sur les systèmes d'exploitation sélectionnés, ce module peut être utilisé pour
charger des modules dans le serveur HTTP Apache en cours d'exécution grâce au
mécanisme des Dynamic Shared Object ou Objets Partagés Dynamiquement (DSO), et
évite ainsi de devoir effectuer une recompilation.

Sous Unix, le code chargé provient en général de fichiers objet partagés
possèdant en général l'extension .so, alors que sous Windows, l'extension peut
être soit .so, soit .dll.


.. index::
   pair: Apache; modules

.. _apache_modules:

===========================================
Apache modules
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/mod/
   - https://httpd.apache.org/docs/current/en/mod/

.. toctree::
   :maxdepth: 3


   core/core
   mod_so/mod_so
   mod_authn_core/mod_authn_core
   mod_auth_basic/mod_auth_basic
   mod_auth_digest/mod_auth_digest
   mod_authn_file/mod_authn_file
   mod_authz_user/mod_authz_user
   mod_wsgi/mod_wsgi


.. index::
   pair: Apache module; mod_authn_file

.. _mod_authn_file:

===========================================
Apache **mod_authn_file** module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/mod/mod_authn_file.html
   - https://httpd.apache.org/docs/current/en/mod/mod_authn_file.html
   - :ref:`basic_apache_authentication`




Sommaire
==========

.. seealso::

   - :ref:`basic_apache_authentication`

Ce module permet aux frontaux d'authentification comme mod_auth_digest et
:ref:`mod_auth_basic <mod_auth_basic>` d'authentifier les utilisateurs en
les recherchant dans des fichiers de mots de passe au format texte.

.. index::
   pair: Apache2 ; mod_wsgi
   ! mod_wsgi


.. _mod_wsgi_linux:

=========================================================================
Apache2 mod_wsgi on GNU/Linux
=========================================================================

.. seealso::

   - https://code.google.com/p/modwsgi/wiki/QuickConfigurationGuide#Delegation_To_Daemon_Process
   - http://www.rackspace.com/knowledge_center/article/ubuntu-using-modwsgi-to-serve-your-application



.. toctree::
   :maxdepth: 3


   centos/centos
   debian/debian

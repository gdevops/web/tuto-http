
.. index::
   pair: WSGI ; Debian

.. _wsgi_example_2:

=========================================================
Example 2 with WSGIDaemonProcess
=========================================================


.. seealso::

   - https://gist.github.com/suhailvs/fd810e8b59236d80747d






Introduction
=============

How to Run Django with mod_wsgi and Apache with a virtualenv.


pair django with apache server
===============================

Create a specific site sampleapp::

    vi /etc/apache2/sites-available/sampleapp.conf


add below line to that file


.. code-block:: apache
   :linenos:

    <VirtualHost *:80>
    ServerName staging.djangoer.com
    DocumentRoot /home/mysite/www

    WSGIDaemonProcess sampleapp python-path=/home/mysite/www:/home/mysite/myenv/lib/python2.7/site-packages
    WSGIProcessGroup sampleapp
    WSGIScriptAlias / /home/mysite/www/django_sample/wsgi.py
    Alias /static /home/mysite/www/static

    ErrorLog /home/mysite/error.log
    CustomLog /home/mysite/access.log combined

    <Directory /home/mysite/www>
    Require all granted
    </Directory>

    </VirtualHost>


enable it and restart apache::

    a2ensite sampleapp
    service apache2 reload

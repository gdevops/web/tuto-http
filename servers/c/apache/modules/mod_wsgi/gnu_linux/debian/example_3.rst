

.. _wsgi_example_3:

=========================================================
Example 3 with WSGIDaemonProcess
=========================================================


.. seealso::

   - http://www.idreammicro.com/post/django-apache2-wsgi





Introduction
=============

Bien que Django embarque un serveur, celui-ci est à réserver au développement.

En effet il n'est pas prévu pour faire office de serveur de production.
Cet article propose de servir un site Django à l'aide d'Apache 2 et du module WSGI.


Création du VirtualHost (Debian)
=================================

On crée un VirtualHost dédié dans le fichier /etc/apache2/sites-available/idreammicro :
idreammicro


.. warning:: Pas de traitement des fichiers static !

.. warning:: Pas d'environnement virtuel !


.. code-block:: apache
   :linenos:

    <VirtualHost *:80>

        ServerName idreammicro

        DocumentRoot /var/www/idreammicro

        <Directory /var/www/idreammico>
            Order allow,deny
            Allow from all
        </Directory>

        WSGIDaemonProcess daemon user=www-data group=www-data processes=2 threads=15 display-name=%{GROUP}
        WSGIProcessGroup daemon

        WSGIScriptAlias / /var/www/idreammicro/apache/django.wsgi

    </VirtualHost>


On indique qu'Apache (utilisateur www-data, groupe www-data) est le propriétaire
de l'application Django::

    /var/www/idreammicro$ sudo chown -R www-data:www-data /var/www/idreammicro

On active le site idreammicro::

    /var/www/idreammicro$ sudo a2ensite idreammicro


On recharge la configuration d'Apache::

    /var/www/idreammicro$ sudo /etc/init.d/apache2 reload

Le VirtualHost Apache précédemment créé porte le nom idreammicro
(ServerName idreammicro).

Par conséquent il est nécessaire de créer une nouvelle association nom de
machine / adresse IP.

Dans le fichier /etc/hosts, on ajoute la ligne::

    127.0.1.1    idreammicro

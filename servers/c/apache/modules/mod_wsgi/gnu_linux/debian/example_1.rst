
.. index::
   pair: WSGI ; Example 1

.. _wsgi_example_1:

=========================================================
Configuring mod_wsgi in a default virtual host (Debian)
=========================================================


.. seealso::

   - https://www.digitalocean.com/community/tutorials/how-to-run-django-with-mod_wsgi-and-apache-with-a-virtualenv-python-environment-on-a-debian-vps


The idea behind configuring mod_wsgi for any other virtual host in Apache is
the same as the one presented here.
We will use the default virtual host for simplicity, since it is the one already
provided by a clean Apache installation.

Open the default virtual host configuration file in nano editor::


    nano /etc/apache2/sites-enabled/000-default


::

    and add three following lines just below <VirtualHost *:80>

    WSGIDaemonProcess sampleapp python-path=/var/www/sampleapp:/var/www/sampleapp/env/lib/python2.7/site-packages
    WSGIProcessGroup sampleapp
    WSGIScriptAlias / /var/www/sampleapp/sampleapp/wsgi.py



The first line spawns a WSGI daemon process called sampleapp that will be
responsible for serving our Django application.
The daemon name can be basically anything, but is good practice to use
descriptive names such as application names here.

If we were using global Python installation and global Django instance, the
python-path directive would not be necessary.
However, using virtual environment makes it obligatory to specify the alternate
Python path so that mod_wsgi will know where to look for Python packages.

The path must contain two directories: the directory of Django project itself
/var/www/sampleapp and directory of Python packages inside our virtual
environment for that project /var/www/sampleapp/env/lib/python2.7/site-packages.
Directories in path definition are delimited using a colon sign.

The second line tells that particular virtual host to use the WSGI daemon
created beforehand, and as such, the daemon name must match between those
two. We used sampleapp in both lines.

The third line is the most important, as it tells Apache and mod_wsgi where to
find WSGI configuration.
The wsgi.py supplied by Django contains the barebone default configuration
for WSGI for serving Django application that works just fine and changing the
configuration in this file is out of this article scope.

After these changes it is necessary to restart Apache::

    service apache2 restart

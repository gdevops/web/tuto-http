
.. index::
   pair: WSGI ; Debian

.. _wsgi_debian:

=========================================================
Configuring mod_wsgi on Debian
=========================================================


.. toctree::
   :maxdepth: 3

   example_1
   example_2
   example_3
   example_4



.. _wsgi_example_4:

=========================================================
Example 4 with WSGIDaemonProcess Django 1.5
=========================================================


.. seealso::

   - http://www.idreammicro.com/post/django-apache-2-mod-wsgi-maj





Introduction
=============

Comme le bon vin, Django se bonifie avec le temps.
Mieux encore, il se simplifie ! C'est notamment le cas pour son intégration
avec Apache 2 via le module WSGI.


Un précédent article proposait une démarche permettant de servir une application
Django 1.3 avec Apache 2 et le module WSGI.
Depuis, Django est passé en version 1.4 puis 1.5. L'occasion de remettre ça !

L'environnement utilisé pour cet article repose sur la distribution
Debian Wheezy 7.1 amd64. On y a ajouté les paquets apache2, libapache2-mod-wsgi
et Django 1.5.4.


Arborescence
============

::

    idreammicro
    |_ helloworld
    |    |_ __init__.py
    |    |_ models.py
    |    |_ tests.py
    |    |_ views.py
    |_ idreammicro
    |    |_ __init.py__
    |    |_ settings.py
    |    |_ urls.py
    |    |_ wsgi.py
    |_ manage.py



Virtual Host Apache
=====================

On choisit de créer un serveur virtuel dédié au projet Django.

Django 1.4 a introduit une amélioration du support WSGI, en embarquant une
application WSGI dans le fichier idreammicro/wsgi.py.

Les opérations suivantes nécessitent d'avoir les droits d'administration.

On crée le fichier /etc/apache2/sites-available/idreammicro.


.. warning:: Pas de traitement des fichiers static !

.. warning:: Pas d'environnement virtuel !


.. warning:: apparemment utilisation d'Apache 2.2 et non 2.4



.. code-block:: apache
   :linenos:

    <VirtualHost *:80>

        ServerName django
        ServerAlias django

        DocumentRoot /home/jlesech/www-django/idreammicro

        <Directory /home/jlesech/www-django/idreammicro>
            Order allow,deny
            Allow from all
        </Directory>

        WSGIDaemonProcess django processes=2 threads=15 display-name=%{GROUP} python-path=/home/jlesech/www-django/idreammicro
        WSGIProcessGroup django

        WSGIScriptAlias / /home/jlesech/www-django/idreammicro/idreammicro/wsgi.py

        <Directory /home/jlesech/www-django/idreammicro/idreammicro>
            Order allow,deny
            Allow from all
        </Directory>

        ErrorLog ${APACHE_LOG_DIR}/idreammicro_error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog ${APACHE_LOG_DIR}/idreammicro_access.log combined

    </VirtualHost>


On active le nouveau serveur virtuel::

    a2ensite django

Dans le fichier /etc/hosts, on ajoute un host (ligne 3)::

    127.0.0.1   localhost
    127.0.1.1   i7
    127.0.0.1   django

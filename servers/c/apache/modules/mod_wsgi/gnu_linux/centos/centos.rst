
.. index::
   pair: WSGI ; CentOS

.. _wsgi_centos:

=========================================================
Configuring mod_wsgi on CentOS
=========================================================




How to know
===========

::

    $ cat /etc/redhat-release
    CentOS release 6.6 (Final)

    $ rpm -qa|grep pip
    python34u-pip-7.0.3-1.ius.centos6.noarch

    $ rpm -qa|grep python34u-li
    python34u-libs-3.4.3-2.ius.centos6.x86_64

    $ rpm -qa|grep python34u-setuptools
    python34u-setuptools-17.1.1-1.ius.centos6.noarch


    $ httpd -V
    Server version: Apache/2.2.15 (Unix)
    Server built:   Oct 16 2014 14:48:21

    $ python3.4
    Python 3.4.3 (default, Jun  2 2015, 13:59:03)

    $ rpm -q mod_wsgi
    mod_wsgi-3.2-7.el6.x86_64


    $-bash-4.2$ sestatus

    SELinux status:                 enabled
    SELinuxfs mount:                /sys/fs/selinux
    SELinux root directory:         /etc/selinux
    Loaded policy name:             targeted
    Current mode:                   enforcing
    Mode from config file:          enforcing
    Policy MLS status:              enabled
    Policy deny_unknown status:     allowed
    Max kernel policy version:      28


Examples
=========

.. toctree::
   :maxdepth: 3

   example_1
   example_2
   example_3

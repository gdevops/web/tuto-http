
.. index::
   pair: CentOS ; mod_wsgi


.. _wsgi_centos_1:

=========================================================
Configuring mod_wsgi on CentOS exemple 1
=========================================================

.. seealso::

   - https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-centos-7



Description du serveur virtuel
================================

.. code-block:: apache
   :linenos:


    Alias /static /home/user/myproject/static
    <Directory /home/user/myproject/static>
        Require all granted
    </Directory>

    <Directory /home/user/myproject/myproject>
        <Files wsgi.py>
            Require all granted
        </Files>
    </Directory>

    WSGIDaemonProcess myproject python-path=/home/user/myproject:/home/user/myproject/myprojectenv/lib/python2.7/site-packages
    WSGIProcessGroup myproject
    WSGIScriptAlias / /home/user/myproject/myproject/wsgi.py



Amélioration possible
=====================

::

    WSGIDaemonProcess myproject python-home=/home/user/myproject/myprojectenv home=/home/user/myproject

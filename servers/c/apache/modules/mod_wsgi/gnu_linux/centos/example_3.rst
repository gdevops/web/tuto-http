


.. _wsgi_centos_3:

=========================================================
Configuring mod_wsgi on CentOS exemple 3
=========================================================

.. seealso::

   - https://www.linode.com/docs/websites/frameworks/django-apache-and-modwsgi-on-centos-5



::

    <VirtualHost example.com:80>
       ServerName example.com
       ServerAlias www.example.com
       ServerAdmin squire@example.com

       DocumentRoot /srv/www/example.com/public_html

       WSGIScriptAlias / /srv/www/example.com/application/django.wsgi
       <Directory /srv/www/example.com/application>
          Order allow,deny
          Allow from all
       </Directory>

       Alias /robots.txt /srv/www/example.com/public_html/robots.txt
       Alias /favicon.ico /srv/www/example.com/public_html/favicon.ico
       Alias /images /srv/www/example.com/public_html/images
       Alias /static /srv/www/example.com/public_html/static

       ErrorLog /srv/www/example.com/logs/error.log
       CustomLog /srv/www/example.com/logs/access.log combined
    </VirtualHost>


Edit the /etc/httpd/conf.d/wsgi.conf file to enable the mod_wsgi by
uncommenting or adding the following line:

/etc/httpd/conf.d/wsgi.conf


    LoadModule wsgi_module modules/mod_wsgi.so

When you have successfully configured your Apache virtual host, and enabled the
required module, issue the following command to restart the web server:

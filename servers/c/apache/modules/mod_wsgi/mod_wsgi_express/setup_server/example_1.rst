
.. _express_setup_server_1:

=========================================================================
mod_wsgi_express setup-server example 1
=========================================================================



If you wanted to use mod_wsgi-express to run up your application on port 80 and
replace the default Apache configuration file the system then it is better
to use mod_wsgi-express to generate the configuration needed but not actually
start the server straight away.

In this case you would run something like::

    sudo mod_wsgi-express setup-server --server-root /etc/mod_wsgi-www.example.com-80 --log-directory /var/log/apache2 --user www-data --group www-data --port 80 hello.wsgi


This would generate various files in /etc/mod_wsgi-www.example.com-80, the key
one being 'apachectl'.

All that is then required is to setup system init scripts to run 'apachectl start'
and 'apachectl stop' as appropriate.

.. index::
   pair: include_file ; mod_wsgi_express


.. _include_file:

=========================================================================
mod_wsgi_express include_file argument
=========================================================================

.. seealso::

   - https://groups.google.com/forum/#!topic/modwsgi/-dUBBFI0d5o


.. toctree::
   :maxdepth: 3


Question
=========

Hey

I'm currently getting my django app ready for production.

I'm using docker, and i currently using the image grahamdumpleton/mod-wsgi-docker:python-2.7
from https://github.com/GrahamDumpleton/mod_wsgi-docker .

I'm running it a bit customized as i am using the command::

    ["python", "sonofatailorapp/manage_prod.py", "runmodwsgi", "config.wsgi", "--port=80", "--log-to-terminal", "--startup-log"] .


As far as i can understand, this method uses the mod_wsgi-express settings to
run the server.

And now is where it gets confusing to me. The docs says that mod_wsgi-express
will automatically generate the settings as specified in the arguments, but i'm
at a point where i would like to use other apache modules like mod_pagespeed
and i can't seem to find a way doing this with mod_wsgi-express.

I really like that i can startup the server through djangos manage.py as it
set's up all the right paths for static files and so on. I also can't seem to
find full documentation for mod_wsgi-express and maybe that's why i don't get it.

At this point i'm not even sure if express is the right way to go in production.

Any input would be greatly appreciated !

Cheers

Answer
========

To get mod_pagespeed incorporated into the image and used will take a few steps
which are going to be a little bit fiddly.

What the steps are exactly is going to depend a bit on whether you are using
mod_wsgi-docker:python-2.7 or mod_wsgi-docker:python-2.7-onbuild.

Which is used dictates how you trigger the download and building of
mod_pagespeed into the image so it can be used.

I will defer explaining that until you can tell me which Docker image you are
using and what your Dockerfile looks like.

Also, hopefully all the compiler bits needed are already a part of the image.
From memory mod_pagespeed is written in C++. I think I have g++ as part of the
image.

Presuming mod_pagespeed is installed into the module directory of the Apache
installation the image provides, then the next step is to incorporate that
into the mod_wsgi-express configuration.

Since that is auto generated, what would need to be done is create a Apache
configuration file snippet and tell mod_wsgi-express to include that at the end::

    python sonofatailorapp/manage_prod.py runmodwsgi config.wsgi --port=80 --log-to-terminal --startup-log \
        —include-file pagespeed.conf

The pagespeed.conf file would then, at a guess, have something like::

    <IfVersion < 2.4>
      LoadModule pagespeed_module ${MOD_WSGI_MODULES_DIRECTORY}/mod_pagespeed.so
    </IfVersion>
    <IfVersion >= 2.4.2>
      LoadModule pagespeed_module ${MOD_WSGI_MODULES_DIRECTORY}/mod_pagespeed_ap24.so
    </IfVersion>

    ModPagespeed on

    AddOutputFilterByType MOD_PAGESPEED_OUTPUT_FILTER text/html

Let me know which Docker image you are using and your Dockerfile and I will see
if I can work out the recipe to get it incorporated into a derived image.

Graham/arguments

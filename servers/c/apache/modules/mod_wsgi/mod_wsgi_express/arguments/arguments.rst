.. index::
   pair: arguments ; mod_wsgi_express


.. _mod_wsgi_express_args:

=========================================================================
mod_wsgi_express arguments documentation
=========================================================================

.. seealso::

   - https://github.com/GrahamDumpleton/mod_wsgi/releases
   - http://blog.dscpl.com.au/2014/09/what-is-current-version-of-modwsgi.html


.. toctree::
   :maxdepth: 3


   include_file/include_file

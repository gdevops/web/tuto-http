

.. _express_start_server_2:

=========================================================================
sudo mod_wsgi_express start-server example 2
=========================================================================

.. seealso::

   - https://groups.google.com/forum/#!topic/modwsgi/2eFokQjEuEI



By default when have::

    mod_wsgi-express start-server


::

    Server URL         : http://localhost:8000/
    Server Root        : /tmp/mod_wsgi-localhost:8000:5099
    Server Conf        : /tmp/mod_wsgi-localhost:8000:5099/httpd.conf
    Error Log File     : /tmp/mod_wsgi-localhost:8000:5099/error_log
    Locale Setting     : en_US.ISO8859-1
    Daemon Processes   : 1
    Daemon Threads     : 5

it is only listen for socket connections coming from localhost.

If you need to access it from another machine, you would need to use::

    mod_wsgi start-server --host=public.name.of.host


or on some systems you can say::

    mod_wsgi start-server --host=0.0.0.0


Anyway, you at least have it crashing when running as yourself now. We can
therefore run gdb again and not have the root squash issue.

First, run::

    mod_wsgi-express start-server --debug-mode --setup-only

Look in the file:

    /tmp/mod_wsgi-localhost:8000:5099/apachectl


At the top you run see a HTTPD variable, with a setting like::

    HTTPD="/usr/local/httpd-2.4.x/bin/httpd -f /tmp/mod_wsgi-localhost:8000:502/httpd.conf -DONE_PROCESS"

Yours may list other -D options.

Run gdb on the 'httpd' executable path which is first value in that::


    gdb /usr/local/httpd-2.4.x/bin/httpd

Then in gdb, use the rest of that value::

    run -f /tmp/mod_wsgi-localhost:8000:502/httpd.conf -DONE_PROCESS"


If crashes, enter::

    where

at gdb prompt.

Makes me think I should add an option to mod_wsgi-express specifically to run
httpd under gdb to make that easier. :-)



.. _express_start_server_1:

=========================================================================
sudo mod_wsgi_express start-server example 1
=========================================================================

.. seealso::

   - https://groups.google.com/forum/#!topic/modwsgi/zi_v6yoB6fs




I want to have mod_wsgi-express even generate the skeletons for the system init
scripts so you can copy/link them into the right place and not even have to
worry about creating them, but I possibly need a bit of guidance on that one
to handle all the different init systems and when I put out a query for people
who could help, got no response. I do intend to look at that again soon though
myself.

As to how complicated of a system can mod_wsgi-express handle setting up, well
to run a secure web site you could use::

    sudo mod_wsgi-express start-server --server-root /etc/mod_wsgi-www.example.com-443 --log-directory /var/log/apache2 --user www-data --group www-data \
        --https-port 8443 --https-only --ssl-certificate server --server-name www.example.com hello.wsgi

In the directory you would need your 'server.crt' and 'server.key' file with
option of --ssl-certificate used. Unreleased version allows you to specify
the .crt and .key files with separate options.

So mod_wsgi-express supports all sorts of stuff for secure web sites and lots
more common stuff that people would setup in Apache.
Even handles things like log file rotations, access log with different formats
etc etc.

Another example is static file handling. If you had a directory which had
favicon.ico, robots.txt or other static file assets in it, you could do::

    mod_wsgi-express start-server --document-root htdocs hello.wsgi

What will happen is that any static files are given priority and if they exist
are served up. If there is no static file mapped by the URL, then the request
would instead be handled by the WSGI application.

If you have specific directories of static files that need to be hosted at a
specific sub URL, such as with Django, you can use::

    mod_wsgi-express start-server --working-directory example --url-alias /static example/htdocs --application-type module example.wsgi

This presumes you were running it from the directory above where your Django
project is and that 'example/htdocs' was where collectstatic placed the files.


One of my favourites is that through a special application type you can even use
it as a quick alternative to host up static files, replacing 'python -m SimpleHTTPServer'
with a **real server**, one that can be very easily secured with SSL if need be.

Thus::

    mod_wsgi-express start-server --application-type static --directory-listing


As a bit of a joke to prove it could be done I even added an option::

    mod_wsgi-express start-server --document-root static --with-php5 hello.wsgi


If the Apache being used has PHP module installed and setup, you can even easily
mix a PHP application defined by php files in the static directory with the
WSGI application.

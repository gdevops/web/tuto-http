

.. _express_start_server_3:

=========================================================================
sudo mod_wsgi_express start-server example 3
=========================================================================





mod_wsgi-express start-server hello_world_wsgi.py
==================================================


::

    (django_env)vagrant@scotchbox:/var/www/test_wsgi$ mod_wsgi-express start-server hello_world_wsgi.py


::

    Server URL         : http://localhost:8000/
    Server Root        : /tmp/mod_wsgi-localhost:8000:1000
    Server Conf        : /tmp/mod_wsgi-localhost:8000:1000/httpd.conf
    Error Log File     : /tmp/mod_wsgi-localhost:8000:1000/error_log (warn)
    Request Capacity   : 5 (1 process * 5 threads)
    Request Timeout    : 60 (seconds)
    Queue Backlog      : 100 (connections)
    Queue Timeout      : 45 (seconds)
    Server Capacity    : 20 (event/worker), 20 (prefork)
    Server Backlog     : 500 (connections)
    Locale Setting     : en_US.UTF-8


.. figure:: hello_world_wsgi.png
   :align: center

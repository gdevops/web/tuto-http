.. index::
   pair: Introduction ; mod_wsgi_express


.. _mod_wsgi_express_intro:

=========================================================================
mod_wsgi_express introduction
=========================================================================




Intro 1
========

.. seealso::

   - https://groups.google.com/forum/#!topic/modwsgi/zi_v6yoB6fs



What mod_wsgi-express is ultimately intended for is a simple way of getting
people up and running who don't have overly strange requirements or a need to
be running multiple Python web sites at the same time, although there are ways
of doing the latter if need be, even with different Python versions.

Can explain this another time if people interested.

So what mod_wsgi-express does for you is generate all the Apache configuration
for you, based on minimal command line options.

The Apache configuration generated and how mod_wsgi is setup is based on my
knowledge of what is the best way for doing things.

That way you don't have to fumble around making guesses as to what to change
and I give you a really good starting point which is going to be much better
than any default Linux distribution Apache configuration when it comes to
running Python web sites


So in the simplest case if you had a simple WSGI script file all you would
have to do is run::

    mod_wsgi-express start-server hello.wsgi


This would by default run on port 8000.


The mod_wsgi-express package therefore covers quite a lot of range.

It can be used for development, to quickly run WSGI applications on the command
line to production grade applications started on system startup.

It is therefore much more versatile than any other WSGI server in that respect
with its easy options for a range of builtin functionality.


Intro 2
=======

.. seealso:: https://groups.google.com/forum/#!topic/modwsgi/YMt8VyrIvqA

The idea with mod_wsgi-express is that through my experience of Apache and what
makes a good configuration for Python web hosting, I can generate the configuration
automatically for you.
This approach will probably work for a good majority of people who are running
a Python web site. It will not work for everyone though, especially where an
Apache instances is shared for other purposes, and in those cases you would
still have to fall back to configuring an existing Apache installation yourself.

This is not to say that everything is in place to make it as easy as possible
to use mod_wsgi-express.

One thing that still needs to be done is the automatic generation of system
start scripts which could then be quickly integrated into a Linux system to
get things running. Due to my lack of knowledge of different Linux systems
this is where I will need some help, but although I put a call out a while
ago for help in this area, I got no responses.

That is therefore something which will come only slowly at this point as I want
to focus on other areas for the time being.

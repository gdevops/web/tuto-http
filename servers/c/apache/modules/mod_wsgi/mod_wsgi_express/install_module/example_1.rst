
.. _express_install_module_1:

=========================================================================
mod_wsgi_express install-module example 1
=========================================================================


The alternative to setting LoadModule to use the module direct out of the Python
installation, is to run, after having done pip install of mod_wsgi::

    sudo mod_wsgi-express install-module

This would copy the .so from the Python installation into the Apache installation
modules directory.
When it does that it would output to the display what you would then need to add
to the Apache configuration.

Thus it likely would have then output::

    LoadModule wsgi_module modules/mod_wsgi-py34.so
    WSGIPythonHome /usr/local

The final path to LoadModule may be relative or absolute, depending on the way
Apache installation was configured.

The location will be the modules directory for the main Apache installation, so
will only work if you have actually used ‘install-module’ command.

Finally, if swapping what version of Python is being used for mod_wsgi used by
system Apache, it is a good idea to ensure you do a ‘stop’ of Apache and not
a ‘restart’ or ‘reload’.

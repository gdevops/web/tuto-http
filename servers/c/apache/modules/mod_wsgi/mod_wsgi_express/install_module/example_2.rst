
.. _express_install_module_2:

=========================================================================
mod_wsgi_express install-module example 2
=========================================================================


::

    (django_env)vagrant@scotchbox:/var/www/test_wsgi$ sudo mod_wsgi-express install
    -module

::


    LoadModule wsgi_module /usr/lib/apache2/modules/mod_wsgi-py34.cpython-34m.so
    WSGIPythonHome /usr

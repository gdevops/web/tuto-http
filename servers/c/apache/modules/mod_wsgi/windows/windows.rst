.. index::
   pair: Windows ; mod_wsgi


.. _mod_wsgi_windows:

=========================================================================
Apache2 mod_wsgi on Windows
=========================================================================

.. seealso::

   - https://github.com/GrahamDumpleton/mod_wsgi/tree/master/win32
   - https://groups.google.com/forum/#!topic/modwsgi/arW5aD9QEhI
   - https://github.com/GrahamDumpleton/mod_wsgi/releases
   - http://www.lfd.uci.edu/~gohlke/pythonlibs/#mod_wsgi






mod_wsgi on Windows
====================

.. seealso::

   - http://sbac-oss-equation-scorer.readthedocs.org/en/latest/quickstart.html



Download
=========

::

    Any reason you didn't just use the official mod_wsgi builds I make available
    rather than those from the gohike site?

    They can be found at:

        https://github.com/GrahamDumpleton/mod_wsgi/releases

    Go down to version 4.4.6 and you will find the last Windows builds.

    They aren't updated on every build as usually there isn't a need as most
    changes aren't relevant to Windows.

    Graham



::


    (env_django_64) \XLOG5T092_django_REST_log_transaction\composants\apache2\mod_wsgi>pip install mod_wsgi-4.4.13+ap24vc10-cp34-none-win_amd64.whl
     Processing \xlog5t092_django_rest_log_transaction\composants\apache2\mod_wsgi\mod_wsgi-4.4.13+ap24vc10-cp34-none-win_amd64.whl
     Installing collected packages: mod-wsgi
     Successfully installed mod-wsgi-4.4.13+ap24vc10



Checking installation
======================

.. seealso::

   - https://code.google.com/p/modwsgi/wiki/CheckingYourInstallation


httpd.exe -S
-------------

::


    pvergain@NOTOCACTUS /c/Apache24/bin
    $ httpd.exe -S
    AH00558: httpd.exe: Could not reliably determine the server's fully qualified do
    main name, using fe80::1cea:dadf:4c5b:653b. Set the 'ServerName' directive globa
    lly to suppress this message
    VirtualHost configuration:
    ServerRoot: "C:/Apache24"
    Main DocumentRoot: "C:/Apache24/htdocs"
    Main ErrorLog: "C:/Apache24/logs/error.log"
    Mutex default: dir="C:/Apache24/logs/" mechanism=default
    PidFile: "C:/Apache24/logs/httpd.pid"
    Define: DUMP_VHOSTS
    Define: DUMP_RUN_CFG


$ httpd.exe -M
---------------

::

    AH00558: httpd.exe: Could not reliably determine the server's fully qualified do
    main name, using fe80::1cea:dadf:4c5b:653b. Set the 'ServerName' directive globa
    lly to suppress this message
    Loaded Modules:
    core_module (static)
    win32_module (static)
    mpm_winnt_module (static)
    http_module (static)
    so_module (static)
    access_compat_module (shared)
    actions_module (shared)
    alias_module (shared)
    allowmethods_module (shared)
    asis_module (shared)
    auth_basic_module (shared)
    authn_core_module (shared)
    authn_file_module (shared)
    authz_core_module (shared)
    authz_groupfile_module (shared)
    authz_host_module (shared)
    authz_user_module (shared)
    autoindex_module (shared)
    cgi_module (shared)
    dir_module (shared)
    env_module (shared)
    include_module (shared)
    isapi_module (shared)
    log_config_module (shared)
    mime_module (shared)
    negotiation_module (shared)
    setenvif_module (shared)
    wsgi_module (shared)


Examples
=========

.. toctree::
   :maxdepth: 3

   example_1

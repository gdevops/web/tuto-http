
.. index::
   pair: mod_wsgi ; Directives


.. _mod_wsgi_directives:

=========================================================
``mod_wsgi`` configuration directives
=========================================================


.. seealso::

   - https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives


.. toctree::
   :maxdepth: 3

   WSGIScriptAlias
   WSGIDaemonProcess/WSGIDaemonProcess
   WSGIProcessGroup
   WSGIPythonHome
   WSGIRestrictEmbedded

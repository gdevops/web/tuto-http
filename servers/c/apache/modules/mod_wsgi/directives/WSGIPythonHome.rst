
.. index::
   pair: mod_wsgi ; WSGIPythonHome


.. _WSGIPythonHome:

=========================================================
mod_wsgi WSGIPythonHome directive
=========================================================


.. seealso::

   - https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives#WSGIPythonHome
   - :ref:`python_home`





Description
============

Used to indicate to Python when it is initialised where its library files are
installed.
This should be defined where the Python executable is not in the PATH of the
user that Apache runs as, or where a system has multiple versions of Python
installed in different locations in the file system, especially different
installations of the same major/minor version, and the installation that
Apache finds in its PATH is not the desired one.

This directive can also be used to indicate a Python virtual environment
created using a tool such as virtualenv, to be used for the whole of mod_wsgi.

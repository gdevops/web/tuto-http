
.. index::
   pair: mod_wsgi ; WSGIDaemonProcess


.. _WSGIDaemonProcess:

=========================================================
mod_wsgi WSGIDaemonProcess directive
=========================================================


.. seealso::

   - https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives#WSGIDaemonProcess
   - :ref:`python_home`





Description
============

The WSGIDaemonProcess directive can be used to specify that distinct daemon
processes should be created to which the running of WSGI applications can be
delegated.

Where Apache has been started as the root user, the daemon processes can be
run as a user different to that which the Apache child processes would normally
be run as.

When distinct daemon processes are enabled and used, the process is dedicated
to mod_wsgi and the only thing that the processes do is run the WSGI
applications assigned to that process group. Any other Apache modules such
as PHP or activities such as serving up static files continue to be run in
the standard Apache child processes.

Note that having denoted that daemon processes should be created by using the
WSGIDaemonProcess directive, the WSGIProcessGroup directive still needs to be
used to delegate specific WSGI applications to execute within those
daemon processes.


.. warning:: The WSGIDaemonProcess is not available on Windows.



Options
========

.. toctree::
   :maxdepth: 3

   options/options


.. index::
   pair: python-home ; WSGIDaemonProcess
   pair: python-home ; Virtual Python Environment


.. _WSGIDaemonProcess_python_home:
.. _python_home:


=========================================================
WSGIDaemonProcess python-home option
=========================================================

.. seealso::

   - http://modwsgi.readthedocs.org/en/latest/release-notes/version-3.4.html
   - http://blog.dscpl.com.au/2014/09/using-python-virtual-environments-with.html
   - http://blog.dscpl.com.au/2014/09/python-module-search-path-and-modwsgi.html
   - :ref:`WSGIDaemonProcess_home`




Description
============


Cette option a été introduite dans la version 3.4 de mod_wsgi.

::

    4. Added ‘python-home’ option to WSGIDaemonProcess allowing a Python
    virtual environment to be used directly in conjunction with daemon process.


.. note::  this option does not do anything if setting WSGILazyInitialization to ‘Off’.



.. figure:: python_home/python_home.png
   :align: center

   The good news is that since mod_wsgi version 3.4 or later there is a better way

   http://blog.dscpl.com.au/2014/09/using-python-virtual-environments-with.html

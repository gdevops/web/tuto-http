
.. index::
   pair: home ; WSGIDaemonProcess
   pair: home ; Virtual Python Environment


.. _WSGIDaemonProcess_home:

=========================================================
WSGIDaemonProcess home option
=========================================================

.. seealso::

   - https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives#WSGIDaemonProcess
   - http://blog.dscpl.com.au/2014/09/python-module-search-path-and-modwsgi.html
   - :ref:`WSGIDaemonProcess_python_home`




Description
============

home=directory

Defines an absolute path of a directory which should be used as the initial
current working directory of the daemon processes within the process group.

Normally the current working directory of the Apache parent process would be
the :ref:`root directory <DocumentRoot>`.

In mod_wsgi 2.0+ the initial current working directory will be set to be the
home directory of the user that the daemon process runs as.

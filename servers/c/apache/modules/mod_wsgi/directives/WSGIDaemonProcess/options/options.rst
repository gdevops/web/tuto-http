
.. index::
   pair: Options ; WSGIDaemonProcess


.. _WSGIDaemonProcess_options:

=========================================================
WSGIDaemonProcess options
=========================================================

.. toctree::
   :maxdepth: 3


   home
   python_home

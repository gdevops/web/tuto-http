
.. index::
   pair: mod_wsgi ; WSGIRestrictEmbedded


.. _WSGIRestrictEmbedded:

=========================================================
mod_wsgi WSGIRestrictEmbedded directive
=========================================================

.. seealso::

   - https://code.google.com/p/modwsgi/wiki/ConfigurationDirectives#WSGIRestrictEmbedded





Description
============


The WSGIRestrictEmbedded directive determines whether mod_wsgi embedded mode
is enabled or not.

If set to 'On' and the restriction on embedded mode is therefore enabled, any
attempt to make a request against a WSGI application which hasn't been properly
configured so as to be delegated to a daemon mode process will fail with a
HTTP internal server error response.

This option does not exist on Windows, or Apache 1.3 or any other configuration
where daemon mode is not available.


Complément
===========


The WSGIRestrictEmbedded directive turns off Python in the Apache worker
processes since you are using a daemon process group. Saves on memory and
CPU startup cost of worker processes

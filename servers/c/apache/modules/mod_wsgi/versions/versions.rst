.. index::
   pair: Versions ; mod_wsgi


.. _mod_wsgi_versions:

=========================================================================
mod_wsgi versions
=========================================================================

.. seealso::

   - https://github.com/GrahamDumpleton/mod_wsgi/releases
   - http://blog.dscpl.com.au/2014/09/what-is-current-version-of-modwsgi.html





What is the current version of mod_wsgi ?
=========================================

If you pick up any Linux distribution, you will most likely come to the
conclusion that the newest version of mod_wsgi available is 3.3 or 3.4.

Check when those versions were released and you will find:

- mod_wsgi version 3.3 - released 25th July 2010
- mod_wsgi version 3.4 - released 22nd August 2012

Problem is that people look at that and seeing that there are only infrequent
releases and nothing recently, they think that mod_wsgi is no longer being
developed or supported.
I have even seen such comments to the effect of 'mod_wsgi is dead, use XYZ instead'.



Versions
=========


.. toctree::
   :maxdepth: 3


   4.4.13
   4.1.0
   3.5
   3.4


.. index::
   pair: Apache module; mod_authz_user

.. _mod_authz_user:

===========================================
Apache **mod_authz_user** module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/en/mod/mod_authz_user.html
   - https://httpd.apache.org/docs/current/fr/mod/mod_authz_user.html
   - :ref:`basic_apache_authentication`



Sommaire
==========


.. seealso::

   - :ref:`basic_apache_authentication`

Ce module permet d'accorder ou de refuser l'accès à certaines zones du site web
aux utilisateurs authentifiés.

**mod_authz_user** accorde l'accès si l'utilisateur authentifié fait partie
de la liste spécifiée par une directive **Require user**.

On peut aussi utiliser la directive **Require valid-user** pour accorder l'accès
à tous les utilisateurs qui ont été authentifiés avec succès.


.. index::
   pair: Apache module; mod_authn_core

.. _mod_authn_core:

===========================================
Apache **mod_authn_core** module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/2.4/howto/auth.html
   - https://httpd.apache.org/docs/current/en/mod/mod_authn_core.html
   - https://httpd.apache.org/docs/current/fr/mod/mod_authn_core.html
   - :ref:`basic_apache_authentication`



Sommaire
==========


.. seealso::

   - :ref:`basic_apache_authentication`

Ce module fournit le **coeur des fonctionnalités d'authentification** permettant
d'accorder ou de refuser l'accès à certaines zones du site web.

Les directives fournies par le module **mod_authn_core** sont communes à
tous les fournisseurs d'authentification.

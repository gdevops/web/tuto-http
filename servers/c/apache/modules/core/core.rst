
.. index::
   pair: Apache; core

.. _apache_core_module:

===========================================
Apache core module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/mod/core.html
   - https://httpd.apache.org/docs/current/en/mod/core.html


.. index::
   pair: Apache module; mod_auth_basic

.. _mod_auth_basic:

===========================================
Apache **mod_auth_basic** module
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/mod/mod_auth_basic.html
   - https://httpd.apache.org/docs/current/en/mod/mod_auth_basic.html
   - https://www.linode.com/docs/websites/authbased-access-control-with-apache
   - http://openwetware.org/wiki/Apache_basic_authentication
   - :ref:`basic_apache_authentication`



Sommaire
==========

.. seealso::

   - :ref:`basic_apache_authentication`

Ce module permet d'utiliser **l'authentification basique HTTP** pour restreindre
l'accès en recherchant les utilisateurs dans les fournisseurs d'authentification
spécifiés.

Il est en général combiné avec au moins un module d'authentification comme
mod_authn_file et un module d'autorisation comme mod_authz_user.

L'authentification HTTP à base de condensé (digest), quant à elle, est fournie
par le module mod_auth_digest.

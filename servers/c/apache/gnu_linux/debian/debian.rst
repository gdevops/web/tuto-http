.. index::
   pair: Apache2 ; Debian
   pair: mod_wsgi-express ; Debian


.. _apache2_debian:

=========================================================================
Using Apache2 HTTP With Debian Based Systems (Debian / Ubuntu)
=========================================================================

.. seealso::

   - https://www.linode.com/docs/websites/apache/how-to-install-and-configure-the-apache-web-server-on-debian-7-wheezy/



Introduction
==============

.. seealso::

   - https://debian-handbook.info/browse/stable/sect.http-web-server.html

Apache is a modular server, and many features are implemented by external
modules that the main program loads during its initialization.

The default configuration only enables the most common modules, but enabling
new modules is a simple matter of running::

    a2enmod module

to disable a module, the command::

    a2dismod module


These programs actually only create (or delete) symbolic links in
/etc/apache2/mods-enabled/, pointing at the actual files (stored in
/etc/apache2/mods-available/).

With its default configuration, the web server listens on port 80 (as configured
in /etc/apache2/ports.conf), and serves pages from the /var/www/ directory
(as configured in /etc/apache2/sites-enabled/000-default).


Le répertoire /usr/lib/apache2/modules dontient tous les modules Apache.

.. note:: On constate que le module 'mod_wsgi.o' n'apparait pas dans
   la liste des modules Apache.


::

    service apache2 restart

::

    * Restarting web server apache2
    ...done.


cd /etc/apache2 ; tree -L 2
=============================

::

	6 directories, 182 files
	root@f9edede86b83:/etc/apache2# tree -L 1
	.
	├── apache2.conf
	├── conf-available
	├── conf-enabled
	├── envvars
	├── magic
	├── mods-available
	├── mods-enabled
	├── ports.conf
	├── sites-available
	└── sites-enabled


Dockerfile multi-stage building
==================================

.. literalinclude:: Dockerfile
   :language: dockerfile
   :linenos:

.. index::
   pair: Apache2 ; GNU/Linux


.. _apache2_gnu_linux:

=========================================================================
HTTP Apache2 on GNU/Linux
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/platform



Plateformes
============

.. toctree::
   :maxdepth: 3

   centos/centos
   debian/debian

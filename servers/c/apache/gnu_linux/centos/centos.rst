.. index::
   pair: Apache2 ; CentOS
   pair: systemctl ; httpd
   pair: mod_wsgi-express ; CentOS


.. _apache2_centos:

=========================================================================
Using Apache2 HTTP With RPM Based Systems (Redhat / CentOS / Fedora)
=========================================================================

.. seealso::

   - https://httpd.apache.org/docs/current/platform/rpm.html
   - https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-centos-7
   - https://wiki.centos.org/TipsAndTricks/ApacheVhostDir



Introduction
==============

While many distributions make Apache httpd available as operating system
supported packages, it can sometimes be desirable to install and use the
canonical version of Apache httpd on these systems, replacing the natively
provided versions of the packages.

While the Apache httpd project does not currently create binary RPMs for the
various distributions out there, it is easy to build your own binary RPMs
from the canonical Apache httpd tarball.

This document explains how to build, install, configure and run Apache httpd
2.4 under Unix systems supporting the RPM packaging format.



Configuring the Default Instance of Apache httpd
==================================================

The default configuration for the server is installed by default beneath the
/etc/httpd directory, with logs written by default to /var/log/httpd.

The environment for the webserver is set by default within the optional
/etc/sysconfig/httpd file.


    yum install lynx
    yum install tree
    yum install python-pip
    pip install -U setuptools


::

    yum install httpd
    yum install httpd-devel.x86_64
    yum install python-devel.x86_64


::

    systemctl start httpd.service


::

    yum install mariadb


Installing mod_wsgi
====================

::

    yum remove mod_wsgi


::

    [root@localhost yum.repos.d]# pip install mod_wsgi
    Collecting mod-wsgi
      Using cached mod_wsgi-4.4.13.tar.gz
    Installing collected packages: mod-wsgi
      Running setup.py install for mod-wsgi
    Successfully installed mod-wsgi-4.4.13
    [root@localhost yum.repos.d]#



which mod_wsgi-express
=======================

::

    [root@localhost modules]# which mod_wsgi-express
    /bin/mod_wsgi-express


ll /etc/httpd/modules
======================

::

    [root@localhost modules]# ll
    total 2468
    -rwxr-xr-x. 1 root root  11200 Mar 12 15:08 mod_access_compat.so
    -rwxr-xr-x. 1 root root  11152 Mar 12 15:08 mod_actions.so
    -rwxr-xr-x. 1 root root  15352 Mar 12 15:08 mod_alias.so
    -rwxr-xr-x. 1 root root  11128 Mar 12 15:08 mod_allowmethods.so
    -rwxr-xr-x. 1 root root  11080 Mar 12 15:08 mod_asis.so
    -rwxr-xr-x. 1 root root  15320 Mar 12 15:08 mod_auth_basic.so
    -rwxr-xr-x. 1 root root  36048 Mar 12 15:08 mod_auth_digest.so
    -rwxr-xr-x. 1 root root  11136 Mar 12 15:08 mod_authn_anon.so
    -rwxr-xr-x. 1 root root  15360 Mar 12 15:08 mod_authn_core.so
    -rwxr-xr-x. 1 root root  15256 Mar 12 15:08 mod_authn_dbd.so
    -rwxr-xr-x. 1 root root  11176 Mar 12 15:08 mod_authn_dbm.so
    -rwxr-xr-x. 1 root root  11152 Mar 12 15:08 mod_authn_file.so
    -rwxr-xr-x. 1 root root  19536 Mar 12 15:08 mod_authn_socache.so
    -rwxr-xr-x. 1 root root  23728 Mar 12 15:08 mod_authz_core.so
    -rwxr-xr-x. 1 root root  15288 Mar 12 15:08 mod_authz_dbd.so
    -rwxr-xr-x. 1 root root  11176 Mar 12 15:08 mod_authz_dbm.so
    -rwxr-xr-x. 1 root root  11168 Mar 12 15:08 mod_authz_groupfile.so
    -rwxr-xr-x. 1 root root  11168 Mar 12 15:08 mod_authz_host.so
    -rwxr-xr-x. 1 root root  11128 Mar 12 15:08 mod_authz_owner.so
    -rwxr-xr-x. 1 root root   7024 Mar 12 15:08 mod_authz_user.so
    -rwxr-xr-x. 1 root root  40056 Mar 12 15:08 mod_autoindex.so
    -rwxr-xr-x. 1 root root  11144 Mar 12 15:08 mod_buffer.so
    -rwxr-xr-x. 1 root root  36080 Mar 12 15:08 mod_cache_disk.so
    -rwxr-xr-x. 1 root root  77352 Mar 12 15:08 mod_cache.so
    -rwxr-xr-x. 1 root root  36048 Mar 12 15:08 mod_cache_socache.so
    -rwxr-xr-x. 1 root root  36080 Mar 12 15:08 mod_cgid.so
    -rwxr-xr-x. 1 root root  27696 Mar 12 15:08 mod_cgi.so
    -rwxr-xr-x. 1 root root  23584 Mar 12 15:08 mod_charset_lite.so
    -rwxr-xr-x. 1 root root  11080 Mar 12 15:08 mod_data.so
    -rwxr-xr-x. 1 root root  57088 Mar 12 15:08 mod_dav_fs.so
    -rwxr-xr-x. 1 root root  19616 Mar 12 15:08 mod_dav_lock.so
    -rwxr-xr-x. 1 root root 102376 Mar 12 15:08 mod_dav.so
    -rwxr-xr-x. 1 root root  23616 Mar 12 15:08 mod_dbd.so
    -rwxr-xr-x. 1 root root  31816 Mar 12 15:08 mod_deflate.so
    -rwxr-xr-x. 1 root root  11160 Mar 12 15:08 mod_dialup.so
    -rwxr-xr-x. 1 root root  15256 Mar 12 15:08 mod_dir.so
    -rwxr-xr-x. 1 root root  11176 Mar 12 15:08 mod_dumpio.so
    -rwxr-xr-x. 1 root root  11144 Mar 12 15:08 mod_echo.so
    -rwxr-xr-x. 1 root root  11160 Mar 12 15:08 mod_env.so
    -rwxr-xr-x. 1 root root  15304 Mar 12 15:08 mod_expires.so
    -rwxr-xr-x. 1 root root  23536 Mar 12 15:08 mod_ext_filter.so
    -rwxr-xr-x. 1 root root  15400 Mar 12 15:08 mod_file_cache.so
    -rwxr-xr-x. 1 root root  19408 Mar 12 15:08 mod_filter.so
    -rwxr-xr-x. 1 root root  23736 Mar 12 15:08 mod_headers.so
    -rwxr-xr-x. 1 root root  11176 Mar 12 15:08 mod_heartbeat.so
    -rwxr-xr-x. 1 root root  23568 Mar 12 15:08 mod_heartmonitor.so
    -rwxr-xr-x. 1 root root  52512 Mar 12 15:08 mod_include.so
    -rwxr-xr-x. 1 root root  28104 Mar 12 15:08 mod_info.so
    -rwxr-xr-x. 1 root root  11112 Mar 12 15:08 mod_lbmethod_bybusyness.so
    -rwxr-xr-x. 1 root root  11112 Mar 12 15:08 mod_lbmethod_byrequests.so
    -rwxr-xr-x. 1 root root  11120 Mar 12 15:08 mod_lbmethod_bytraffic.so
    -rwxr-xr-x. 1 root root  15304 Mar 12 15:08 mod_lbmethod_heartbeat.so
    -rwxr-xr-x. 1 root root  32288 Mar 12 15:08 mod_log_config.so
    -rwxr-xr-x. 1 root root  15376 Mar 12 15:08 mod_log_debug.so
    -rwxr-xr-x. 1 root root  11200 Mar 12 15:08 mod_log_forensic.so
    -rwxr-xr-x. 1 root root  11208 Mar 12 15:08 mod_logio.so
    -rwxr-xr-x. 1 root root 117088 Mar 12 15:08 mod_lua.so
    -rwxr-xr-x. 1 root root  19424 Mar 12 15:08 mod_macro.so
    -rwxr-xr-x. 1 root root  27720 Mar 12 15:08 mod_mime_magic.so
    -rwxr-xr-x. 1 root root  23608 Mar 12 15:08 mod_mime.so
    -rwxr-xr-x. 1 root root  61032 Mar 12 15:08 mod_mpm_event.so
    -rwxr-xr-x. 1 root root  31864 Mar 12 15:08 mod_mpm_prefork.so
    -rwxr-xr-x. 1 root root  48520 Mar 12 15:08 mod_mpm_worker.so
    -rwxr-xr-x. 1 root root  35992 Mar 12 15:08 mod_negotiation.so
    -rwxr-xr-x. 1 root root  52416 Mar 12 15:08 mod_proxy_ajp.so
    -rwxr-xr-x. 1 root root  48160 Mar 12 15:08 mod_proxy_balancer.so
    -rwxr-xr-x. 1 root root  19384 Mar 12 15:08 mod_proxy_connect.so
    -rwxr-xr-x. 1 root root  15272 Mar 12 15:08 mod_proxy_express.so
    -rwxr-xr-x. 1 root root  19360 Mar 12 15:08 mod_proxy_fcgi.so
    -rwxr-xr-x. 1 root root  11136 Mar 12 15:08 mod_proxy_fdpass.so
    -rwxr-xr-x. 1 root root  44168 Mar 12 15:08 mod_proxy_ftp.so
    -rwxr-xr-x. 1 root root  39936 Mar 12 15:08 mod_proxy_http.so
    -rwxr-xr-x. 1 root root  19448 Mar 12 15:08 mod_proxy_scgi.so
    -rwxr-xr-x. 1 root root 118752 Mar 12 15:08 mod_proxy.so
    -rwxr-xr-x. 1 root root  19344 Mar 12 15:08 mod_proxy_wstunnel.so
    -rwxr-xr-x. 1 root root  11144 Mar 12 15:08 mod_ratelimit.so
    -rwxr-xr-x. 1 root root  11152 Mar 12 15:08 mod_reflector.so
    -rwxr-xr-x. 1 root root  15288 Mar 12 15:08 mod_remoteip.so
    -rwxr-xr-x. 1 root root  15304 Mar 12 15:08 mod_reqtimeout.so
    -rwxr-xr-x. 1 root root  15336 Mar 12 15:08 mod_request.so
    -rwxr-xr-x. 1 root root  69016 Mar 12 15:08 mod_rewrite.so
    -rwxr-xr-x. 1 root root  40280 Mar 12 15:08 mod_sed.so
    -rwxr-xr-x. 1 root root  15312 Mar 12 15:08 mod_setenvif.so
    -rwxr-xr-x. 1 root root  11224 Mar 12 15:08 mod_slotmem_plain.so
    -rwxr-xr-x. 1 root root  15392 Mar 12 15:08 mod_slotmem_shm.so
    -rwxr-xr-x. 1 root root  15312 Mar 12 15:08 mod_socache_dbm.so
    -rwxr-xr-x. 1 root root  11176 Mar 12 15:08 mod_socache_memcache.so
    -rwxr-xr-x. 1 root root  23552 Mar 12 15:08 mod_socache_shmcb.so
    -rwxr-xr-x. 1 root root  15256 Mar 12 15:08 mod_speling.so
    -rwxr-xr-x. 1 root root  23448 Mar 12 15:08 mod_status.so
    -rwxr-xr-x. 1 root root  15256 Mar 12 15:08 mod_substitute.so
    -rwxr-xr-x. 1 root root  11152 Mar 12 15:08 mod_suexec.so
    -rwxr-xr-x. 1 root root  11096 Mar 12 15:08 mod_systemd.so
    -rwxr-xr-x. 1 root root  11128 Mar 12 15:08 mod_unique_id.so
    -rwxr-xr-x. 1 root root  15296 Mar 12 15:08 mod_unixd.so
    -rwxr-xr-x. 1 root root  11160 Mar 12 15:08 mod_userdir.so
    -rwxr-xr-x. 1 root root  15304 Mar 12 15:08 mod_usertrack.so
    -rwxr-xr-x. 1 root root  11096 Mar 12 15:08 mod_version.so
    -rwxr-xr-x. 1 root root  15272 Mar 12 15:08 mod_vhost_alias.so
    -rwxr-xr-x. 1 root root  19472 Mar 12 15:08 mod_watchdog.so


.. note:: On constate que le module 'mod_wsgi.o' n'apparait pas dans
   la liste des modules Apache.


mod_wsgi-express
=================

::

    [root@localhost modules]#  mod_wsgi-express

::

    Usage: mod_wsgi-express command [params]

    Commands:
        install-module
        module-location
        setup-server
        start-server


mod_wsgi-express install-module
--------------------------------

::

    [root@localhost modules]#  mod_wsgi-express install-module --modules-directory /etc/httpd/modules

::

    LoadModule wsgi_module /etc/httpd/modules/mod_wsgi-py27.so
    WSGIPythonHome /usr


.. figure:: chmod_a_plus_x_mod_wsgi_27_so.png
   :align: center

   chmod a+x mod_wsgi-py27.so


Deploy Applications with Apache

Edit the /etc/httpd/conf.d/wsgi.conf file to enable the mod_wsgi by uncommenting
or adding the following line:

/etc/httpd/conf.d/wsgi.conf

    1



    LoadModule wsgi_module modules/mod_wsgi.so

When you have successfully configured your Apache virtual host, and enabled the
required module, issue the following command to restart the web server:


::

    pip install virtualenv


systemctl restart httpd.service
================================


::

    systemctl restart httpd.service




.. figure:: running_lynx_http_localhost.png
   :align: center




systemctl status httpd.service -l
===================================

::

    [root@localhost httpd]# systemctl status httpd.service -l
    httpd.service - The Apache HTTP Server
       Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled)
       Active: active (running) since Fri 2015-07-24 11:24:41 UTC; 4min 57s ago
       Process: 8225 ExecStop=/bin/kill -WINCH ${MAINPID} (code=exited, status=0/SUCCESS)
       Main PID: 8231 (httpd)
       Status: "Total requests: 1; Current requests/sec: 0; Current traffic:   0 B/sec"

       CGroup: /system.slice/httpd.service
               ├─8231 /usr/sbin/httpd -DFOREGROUND
               ├─8232 /usr/sbin/httpd -DFOREGROUND
               ├─8233 /usr/sbin/httpd -DFOREGROUND
               ├─8234 /usr/sbin/httpd -DFOREGROUND
               ├─8235 /usr/sbin/httpd -DFOREGROUND
               ├─8236 /usr/sbin/httpd -DFOREGROUND
               └─8239 /usr/sbin/httpd -DFOREGROUND

    Jul 24 11:24:41 localhost.localdomain httpd[8231]: AH00558:
        httpd: Could not reliably determine the server's fully qualified domain name,
        using localhost.localdomain.
        Set the 'ServerName' directive globally to suppress this message
    Jul 24 11:24:41 localhost.localdomain systemd[1]:
        Started The Apache HTTP Server



cd /etc/httpd ; tree -L 2
==========================


::

    [root@localhost httpd]# tree -L 2
    .
    ├── conf
    │   ├── httpd.conf
    │   └── magic
    ├── conf.d
    │   ├── autoindex.conf
    │   ├── README
    │   ├── userdir.conf
    │   └── welcome.conf
    ├── conf.modules.d
    │   ├── 00-base.conf
    │   ├── 00-dav.conf
    │   ├── 00-lua.conf
    │   ├── 00-mpm.conf
    │   ├── 00-proxy.conf
    │   ├── 00-systemd.conf
    │   └── 01-cgi.conf
    ├── logs -> ../../var/log/httpd
    ├── modules -> ../../usr/lib64/httpd/modules
    └── run -> /run/httpd

    6 directories, 13 files



Problème nfs
=============


::

    ==> default: Machine booted and ready!
    GuestAdditions 5.0.0 running --- OK.
    ==> default: Checking for guest additions in VM...
    ==> default: Setting hostname...
    ==> default: Configuring and enabling network interfaces...
    ps: illegal option -- o
    Usage ps [-aefl] [-u uid]
    -f = show process uids, ppids
    -l = show process uids, ppids, pgids, winpids
    -u uid = list processes owned by uid
    -a, -e = show processes of all users
    -s = show process summary
    -W = show windows as well as cygwin processes
    The following SSH command responded with a non-zero exit status.
    Vagrant assumes that this means the command failed!

    /etc/init.d/rpcbind restart; /etc/init.d/nfs restart

    Stdout from the command:



    Stderr from the command:

    bash: line 2: /etc/init.d/rpcbind: No such file or directory
    bash: line 2: /etc/init.d/nfs: No such file or directory


Pas de solution pour centos
=============================

- https://github.com/mitchellh/vagrant/issues/1657

::

    [root@geercentos7 ~]# /etc/init.d/vboxadd setup
    Removing existing VirtualBox non-DKMS kernel modules       [  OK  ]
    Building the VirtualBox Guest Additions kernel modules
    Building the main Guest Additions module                   [  OK  ]
    Building the shared folder support module                  [  OK  ]
    Building the OpenGL support module                         [  OK  ]
    Doing non-kernel setup of the Guest Additions              [  OK  ]
    You should restart your guest to make sure the new modules are actually used

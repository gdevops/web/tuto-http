
.. _apache_def:

===========================================
Apache definition
===========================================



English wikipedia definition
=============================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_HTTP_Server

The Apache HTTP Server, colloquially called Apache (/əˈpætʃi/ ə-PATCH-ee), is
free and open-source cross-platform **web server software**, released under the
terms of Apache License 2.0.

Apache is developed and maintained by an open community of developers under the
auspices of the Apache Software Foundation.

The vast majority of Apache **HTTP Server** instances run on a Linux distribution,
but current versions also run on Windows and a wide variety of Unix-like systems.

Past versions also ran on OpenVMS, NetWare, OS/2 and other operating systems.

Originally based on the NCSA HTTPd server, development of Apache began in early
1995 after work on the NCSA code stalled.

Apache played a key role in the initial growth of the World Wide Web, quickly
overtaking NCSA HTTPd as the dominant HTTP server, and has remained most popular
since April 1996.

In 2009, it became the first web server software to serve more than 100 million
websites.

As of August 2018, it was estimated to serve 39% of all active websites and 35%
of the top million websites.


French wikipedia definition
=============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Apache_HTTP_Server


Le logiciel libre Apache HTTP Server (Apache) est un **serveur HTTP** créé et
maintenu au sein de la fondation Apache.

C'est le serveur HTTP le plus populaire du World Wide Web.

Il est distribué selon les termes de la licence Apache.

Historique
-----------

Apache est apparu en avril 1995.

Au début, il s'agissait d'une collection de correctifs et d'additions au serveur
NCSA HTTPd 1.3, qui était dans le domaine public et le serveur HTTP alors le
plus répandu.
De cette origine, de nombreuses personnes affirment que le nom Apache vient de
a patchy server, soit « un serveur rafistolé ».
Par la suite, Apache a été complètement réécrit, de sorte que, dans la version 2,
il ne reste pas de trace de NCSA HTTPd.

La version 2 d'Apache possède plusieurs avancées majeures par rapport à la
version 1, entre autres le support de plusieurs plates-formes (Windows, Linux
et UNIX, entre autres), le support de processus légers UNIX, une nouvelle API
et le support IPv6.

La fondation Apache (Apache Software Foundation ou ASF) a été créée en 1999
à partir du groupe Apache (Apache Group) à l'origine du serveur en 1995.

Depuis, de nombreux autres logiciels utiles au World Wide Web sont développés
à côté du serveur HTTP.

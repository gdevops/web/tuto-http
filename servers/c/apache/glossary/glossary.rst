
.. index::
   pair: Apache; glossary


.. _apache_glossary:

===========================================
Apache glossary
===========================================

.. seealso::

   - https://httpd.apache.org/docs/2.4/fr/glossary.html
   - https://httpd.apache.org/docs/2.4/en/glossary.html#method

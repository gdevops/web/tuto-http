

.. _apache_authentication_def:

===========================================
Apache authentication definition
===========================================



Description
============

L':term:`authentification` est un processus qui vous permet de vérifier qu'une personne
est bien celle qu'elle prétend être.

L':term:`autorisation` est un processus qui permet à une personne d'aller là où elle
veut aller, ou d'obtenir les informations qu'elle désire.

Deux modules d'authentification sont actuellement disponibles:

- :ref:`mod_auth_basic <mod_auth_basic>`
- :ref:`mod_auth_digest <mod_auth_digest>`


Les fournisseurs d'authentification
====================================

- mod_authn_anon
- mod_authn_dbd
- mod_authn_dbm

- :ref:`mod_authn_file <mod_authn_file>`
- mod_authnz_ldap

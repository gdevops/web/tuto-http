
.. _basic_virtualhost_1:

===========================================
virtualhost example 1
===========================================

.. seealso::

   - https://tecadmin.net/how-to-setup-basic-apache-authentication-using-virtualhost/

::


	<VirtualHost *:80>
	...
		<Location />
			Deny from all
			#Allow from (Set IP to allow access without password)
			AuthUserFile /etc/apache2/.htpasswd
			AuthName "Restricted Area"
			AuthType Basic
			Satisfy Any
			require valid-user
		</Location>
	...
	</VirtualHost>

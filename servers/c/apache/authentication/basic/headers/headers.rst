

.. _apache_response_headers:

=========================================================================
HTTP requests and response headers
=========================================================================




.. _apache_header_authorization:

Apache HTTP request header Authorization
==========================================

.. seealso::

   - :ref:`header_authorization`


.. _apache_header_www_authenticate:

Apache HTTP response header WWW_Authenticate
==============================================

.. seealso::

   - :ref:`WWW_Authenticate`

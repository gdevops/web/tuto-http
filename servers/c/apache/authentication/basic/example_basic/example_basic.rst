
.. index::
   pair: htpasswd ; command



.. _apache2_basic_authentication_1:

=========================================================================
HTTP Apache2 Basic Authentication example 1
=========================================================================

.. seealso::

   - http://doc.norang.ca/apache-basic-auth.html




Virtual host
=============

To add basic authentication to an apache2 virtual host you need to add the
following lines to the virtual host configuration::

    AuthType Basic                                               (1)
    AuthName "Private Documentation Repository"                  (2)
    AuthUserFile /var/www/crock.norang.ca/.htpasswd-private      (3)
    Require valid-user                                           (4)



- (1) Set the Basic authentication method
- (2) Provide a name for the location (optional)
- (3) Specify the pathname to the file that contains usernames and passwords.
      The usual filename to use is .htpasswd
- (4) Specify that only users that exist in the file are allowed access

The AuthUserFile should not be located in a directory served by apache2 since
you do not want people to be able to download the contents of this file.
This file contains the valid usernames and passwords.

Example: Virtual Host Entry

.. code-block:: apache
   :linenos:

    <Location "/var/www/crock.norang.ca/htdocs/private/">

        AuthType Basic
        AuthBasicProvider file
        AuthName "Restrited Files"
        AuthUserFile /var/www/crock.norang.ca/.htpasswd-private
        Require user eni


    </Location>


- AuthType : type d'authentification (mod_auth_basic)
- AuthBasicProvider : fournisseur d'authentification (mod_authn_file)
- AuthUserFile : fichier contenant les comptes utilisateur


.. _htpasswd_command:

htpasswd command
=================

The htpasswd file (var/www/crock.norang.ca.htpasswd-private in the example
above) is created and maintained by the `htpasswd` program.

Use use this program to add or change password entries in the file.

Creating New Users
------------------

Example: Creating a new entry

::

    $ sudo htpasswd /var/www/crock.norang.ca/.htpasswd-private eni

::

    New password:
    Re-type new password:

This prompts for the password for newuser and stores the encrypted password
in the password file.

Example: Created password entry (part of the .htpasswd file)

::

    newuser:Po9FhxMKQJcRY


Deleting Users
----------------

You delete users from the .htpasswd access file as follows

Example: Deleting user account eni

::

    $ sudo htpasswd -D .htpasswd eni

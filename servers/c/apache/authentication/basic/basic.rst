
.. index::
   pair: Apache ; Basic authentication

.. _basic_apache_authentication:

===========================================
Basic Apache authentication (RFC 7617)
===========================================


.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication
   - https://tools.ietf.org/html/rfc7617


.. toctree::
   :maxdepth: 3

   htpasswd/htpasswd
   virtualhost/virtualhost
   headers/headers
   example_basic/example_basic

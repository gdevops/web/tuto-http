
.. index::
   pair: htpasswd ; Basic authentication

.. _htpasswd:

===========================================
htpasswd
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/programs/htpasswd.html
   - https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-apache-on-ubuntu-14-04
   - https://tecadmin.net/how-to-setup-basic-apache-authentication-using-virtualhost/

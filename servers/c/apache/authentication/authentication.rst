
.. index::
   pair: Apache ; authentication

.. _apache_authentication:

===========================================
Apache authentication
===========================================

.. seealso::

   - https://httpd.apache.org/docs/2.4/howto/auth.html
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

.. toctree::
   :maxdepth: 6

   definition/definition
   basic/basic

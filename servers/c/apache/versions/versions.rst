
.. index::
   pair: Versions; Apache

.. _apache_versions:

===========================================
Apache versions
===========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_HTTP_Server


.. toctree::
   :maxdepth: 3

   2.4.39/2.4.39

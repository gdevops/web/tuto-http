
.. index::
   pair: Apache logs; overview

.. _apache_logs_ovreview:

===========================================
Apache logs overview
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/logs.html#overview
   - https://httpd.apache.org/docs/current/en/logs.html#overview





Introduction
=============

Pour véritablement gérer un serveur web, il est nécessaire de disposer d'un
retour d'informations à propos de l'activité et des performances du serveur,
ainsi que de tout problème qui pourrait survenir.

Le serveur HTTP Apache propose des fonctionnalités de journalisation souples
et très complètes. Ce document décrit comment configurer ces fonctionnalités
de journalisation et interpréter le contenu des journaux.


Description
=============

Le serveur HTTP Apache fournit toute une variété de mécanismes différents pour
la journalisation de tout ce qui peut se passer au sein de votre serveur, depuis
la requête initiale, en passant par le processus de mise en correspondance
des URLs, et jusqu'à la fermeture de la connexion, y compris toute erreur
pouvant survenir au cours du traitement.

De plus, certains modules tiers fournissent des fonctionnalités de journalisation
ou insèrent des entrées dans les fichiers journaux existants, et les applications
comme les programmes CGI, les scripts PHP ou autres gestionnaires peuvent envoyer
des messages vers le journal des erreurs du serveur.

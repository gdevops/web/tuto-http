
.. index::
   pair: Apache; errorlog

.. _errorlog:

===========================================
Apache errorlog
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/fr/logs.html#errorlog
   - https://httpd.apache.org/docs/current/en/logs.html#errorlog



Introduction
=============

Le journal des erreurs du serveur, dont le nom et la localisation sont définis
par la directive ErrorLog, est le journal le plus important.

C'est dans celui-ci que le démon Apache httpd va envoyer les informations de
diagnostic et enregistrer toutes les erreurs qui surviennent lors du traitement
des requêtes.

Lorsqu'un problème survient au démarrage du serveur ou pendant son fonctionnement,
la première chose à faire est de regarder dans ce journal, car il vous renseignera
souvent sur le problème rencontré et la manière d'y remédier.

Le journal des erreurs est habituellement enregistré dans un fichier
(en général **error_log** sur les systèmes de type Unix et error.log sur Windows
et OS/2).

Sur les systèmes de type Unix, le serveur peut aussi enregistrer ses erreurs
dans syslog ou les rediriger vers un programme par l'intermédiaire d'un tube
de communication (pipe).

Le format par défaut du journal des erreurs est descriptif et de forme
relativement libre.
Certaines informations apparaissent cependant dans la plupart des entrées
du journal.

Voici un message typique à titre d'exemple::

    [Wed Oct 11 14:32:52 2000] [error] [client 127.0.0.1] client denied by server configuration: /export/home/live/ap/htdocs/test

Le premier champ de l'entrée du journal est la date et l'heure du message.
Le second champ indique la sévérité de l'erreur rapportée.

La directive **LogLevel** permet de restreindre le type des erreurs qui doivent
être enregistrées dans le journal des erreurs en définissant leur niveau de
sévérité.

Le troisième champ contient l'adresse IP du client qui a généré l'erreur.
Vient ensuite le message proprement dit, qui indique dans ce cas que le
serveur a été configuré pour interdire l'accès au client.

Le serveur indique le chemin système du document requis (et non son chemin web).

Une grande variété de messages différents peuvent apparaître dans le journal
des erreurs.
La plupart d'entre eux sont similaires à l'exemple ci-dessus.
Le journal des erreurs peut aussi contenir des informations de débogage en
provenance de scripts CGI. Toute information qu'un script CGI écrit sur la
sortie d'erreurs standard stderr sera recopiée telle quelle dans le journal
des erreurs.

La directive **ErrorLogFormat** vous permet de personnaliser le format
du journal des erreurs, et de définir les informations à journaliser.

Si **mod_unique_id** est présent, vous pouvez utiliser le drapeau %L à la fois
dans le journal des erreurs et dans le journal des accès, ce qui aura pour effet
de générer un identifiant d'entrée qui vous permettra de corréler les entrées
du journal des erreurs avec celles du journal des accès.

Pendant la phase de test, il est souvent utile de visualiser en continu le
journal des erreurs afin de détecter tout problème éventuel.

Sur les systèmes de type Unix, ceci s'effectue à l'aide de la commande::

    tail -f error_log

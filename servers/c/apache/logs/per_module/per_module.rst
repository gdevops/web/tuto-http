

.. _apache_logs_per_module:

===========================================
Apache Per-module logging
===========================================

.. seealso::

   - https://httpd.apache.org/docs/current/en/logs.html#permodule
   - https://httpd.apache.org/docs/current/fr/logs.html#permodule




Journalisation par module
===========================

La directive **LogLevel** permet de spécifier un niveau de sévérité de journalisation
pour chaque module.

Vous pouvez ainsi résoudre un problème propre à un module particulier en
augmentant son volume de journalisation sans augmenter ce volume pour les
autres modules.

Ceci est particulièrement utile lorsque vous voulez obtenir des détails sur le
fonctionnement de modules comme mod_proxy ou mod_rewrite.

Pour ce faire, vous devez spécifier le **nom du module** dans votre directive
LogLevel::

    LogLevel info rewrite:trace5

Dans cet exemple, le niveau de journalisation général est défini à info, et à
trace5 pour mod_rewrite.

Cette directive remplace les directives de journalisation par module des
versions précédentes du serveur, comme RewriteLog.

Le serveur HTTP Apache fournit toute une variété de mécanismes différents pour
la journalisation de tout ce qui peut se passer au sein de votre serveur, depuis
la requête initiale, en passant par le processus de mise en correspondance
des URLs, et jusqu'à la fermeture de la connexion, y compris toute erreur
pouvant survenir au cours du traitement.

De plus, certains modules tiers fournissent des fonctionnalités de journalisation
ou insèrent des entrées dans les fichiers journaux existants, et les applications
comme les programmes CGI, les scripts PHP ou autres gestionnaires peuvent envoyer
des messages vers le journal des erreurs du serveur.

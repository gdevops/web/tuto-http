
.. index::
   pair: HTTP Server; Apache
   ! Apache
   ! /əˈpætʃi/

.. _apache:
.. _apache_http_server:

===========================================
Apache (/əˈpætʃi/ ə-PATCH-ee) HTTP server
===========================================

.. seealso::

   - https://en.wikipedia.org/wiki/Apache_HTTP_Server
   - https://httpd.apache.org/docs/current/
   - https://httpd.apache.org/docs/current/fr/sitemap.html
   - https://github.com/apache/httpd

.. figure:: Apache_HTTP_server_logo.png
   :align: center
   :width: 300

   https://httpd.apache.org/docs/current/

.. toctree::
   :maxdepth: 6

   definition/definition
   glossary/glossary
   versions/versions
   gnu_linux/gnu_linux
   authentication/authentication
   logs/logs
   directives/directives
   modules/modules

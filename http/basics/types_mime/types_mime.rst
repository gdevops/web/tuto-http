.. index::
   pair: HTTP; MIME
   pair: HTTP; Types MIME
   ! MIME
   ! Types MIME
   ! Multipurpose Internet Mail Extensions
   ! RFC 6838

.. _types_mime:

=========================================================
Types **MIME (Multipurpose Internet Mail Extensions)**
=========================================================

- https://developer.mozilla.org/fr/docs/Web/HTTP/Basics_of_HTTP/MIME_types
- https://www.iana.org/assignments/media-types/media-types.xhtml
- https://tools.ietf.org/html/rfc6838



Description
=============

Le type Multipurpose Internet Mail Extensions (type MIME) est un standard
permettant d'indiquer la nature et le format d'un document.

Il est défini au sein de la RFC 6838.

L'Internet Assigned Numbers Authority (IANA) est l'organisme officiel
responsable du suivi de l'ensemble des types MIME officiels existants.

Une liste exhaustive et maintenue est consultable sur la page Media Types de l'IANA.

Les navigateurs utilisent le plus souvent le type MIME et non l'extension
d'un fichier pour déterminer la façon dont ils vont traiter ou afficher
un document.

Il est donc important que les serveurs puissent correctement attacher
le type MIME dans l'en-tête de la réponse qu'ils renvoient.

Syntaxe
==========

Structure générale
----------------------

::

    type/sous-type

La structure d'un type MIME est simple, elle est composée d'un type et
d'un sous-type.

Les deux chaînes de caractères sont séparées par un '/'.

Les caractères d'espacement ne sont pas autorisés.
Le type représente la catégorie et peut être particulier ou composé
lorsqu'il regroupe plusieurs formats. Le sous-type est spécifique à chaque type.

Un type MIME est insensible à la casse mais il s'écrit usuellement en
minuscule.

Types particuliers
---------------------

::

    text/plain
    text/html
    image/jpeg
    image/png
    audio/mpeg
    audio/ogg
    audio/*
    video/mp4
    application/octet-stream
    ...

Les types particuliers indiquent la catégorie d'un document.

Les valeurs possibles sont:

- text 	Représente n'importe quel document contenant du texte et qui est
        théoriquement lisible par un utilisateur.
        text/plain, text/html, text/css, text/javascript
- image
  Représente n'importe quelle image.
  Les vidéos ne font pas partie de ce type bien que les images animées
  tels les GIFs animés) font partie de ce type.
  image/gif, image/png, image/jpeg, image/bmp, image/webp
- audio
  Représente n'importe quel fichier audio.
  audio/midi, audio/mpeg, audio/webm, audio/ogg, audio/wav
- video
  Représente n'importe quel fichier vidéo.
  video/webm, video/ogg
- application
  Représente n'importe quelle donnée binaire.
  application/octet-stream, application/pkcs12, application/vnd.mspowerpoint, application/xhtml+xml, application/xml, application/pdf

**text/plain** doit être utilisé pour tous les documents texte sans
sous-type spécifique.

De la même façon, les documents binaires sans sous-type ou dont le
sous-type est inconnu doivent utiliser application/octet-stream.

Types MIME utiles pour les développeurs web
=============================================

application/octet-stream
--------------------------

Il s'agit de la valeur par défaut pour un fichier binaire.

Etant donné qu'il signifie fichier binaire inconnu il est probable que
les navigateurs ne l'exécutent pas automatiquement et que l'utilisateur
ne puisse pas l'exécuter directement dans le navigateur.

Le comportement sera alors le même que si l'en-tête Content-Disposition
était présente avec la valeur attachment et proposera une invite "Enregistrer sous".


text/plain
-------------

Il s'agit de la valeur par défaut pour les fichiers texte.

Bien qu'il signifie fichier texte de format inconnu, les navigateurs
prendront pour hypothèse qu'ils peuvent l'afficher.

Il est important de noter que text/plain ne signifie pas tous les formats
de fichiers textuels.

Si le client s'attend à recevoir un format particulier de données textuelles,
il est vraisemblable que le type text/plain ne soit pas considéré comme
valide à la réception.
Par exemple, si le client télécharge un fichier text/plain à partir
d'un <link> déclarant des fichiers CSS, ce dernier ne sera pas considéré
comme un CSS, le type MIME à utiliser étant text/css.

De l'importance de définir correctement un type MIME
=======================================================

La plupart des serveurs envoient des ressources de format inconnu et
donc utilisent le type par défaut application/octet-stream.

Pour des considérations de sécurité, les navigateurs n'effectuent pas
d'action par défaut pour les ressources de ce type, ce qui oblige
l'utilisateur à stocker le fichier sur son dique pour l'utiliser.

Voici les erreurs communes de configuration côté serveur pour les
formats suivants :

- Les fichiers RAR.
  Idéalement il faudrait définir le type MIME associé aux fichiers contenus.
  Ce n'est généralement pas possible étant donné que le type de ces
  fichiers est vraisemblablement inconnu du serveur, d'autre part,
  il est possible que plusieurs formats soient présents dans le fichier
  RAR. On pourra alors configurer le serveur pour envoyer le type
  MIME application/x-rar-compressed bien qu'il soit probable qu'aucune
  action par défaut pour ce type MIME n'ait été définie côté utilisateur.

- Fichiers audios et vidéos. Seules les ressources associées à un type
  MIME approprié seront reconnues et lues dans les éléments <video> ou <audio>.
  Vérifiez que vous utilisez un format correct pour les fichiers audios et vidéos.

- Les fichiers au format propriétaire. Il est nécessaire d'être vigilent
  lorsque l'on sert des fichiers propriétaires. Evitez autant que possible
  l'utilisation de application/octet-stream puisque ce type générique
  ne permet pas une gestion appropriée de la ressource.

Autres méthodes pour transporter le format d'un document
===========================================================

Les types MIME ne sont pas la seule façon existante pour gérer le format d'un document :

- Les extensions de fichiers sont parfois utilisées, comme sur les systèmes
  d'exploitation Microsoft Windows. Tous les systèmes d'exploitation ne
  considèrent pas l'extension comme signifiante (en particulier Linux et Mac OS).
  De la même manière que pour les types MIME externes, il n'est pas garanti
  que le contenu soit effectivement du type correspondant à l'extension du document.
- Nombres magiques : La syntaxe de différents fichiers permet de déterminer
  le fichier en analysant son contenu, ainsi les fichiers GIF commencent
  par les valeurs hexadécimales 47 49 46 38 soit [GIF89], les fichiers
  PNG quant à eux commencent par 89 50 4E 47 soit [.PNG].
  Néanmoins, tous les types de fichiers ne permettent pas d'utiliser des
  nombres magiques, il ne s'agit donc pas d'une technique infaillible.

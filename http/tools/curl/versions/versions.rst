
.. index::
   pair: Versions ; cURL

.. _curl_versions:

=====================================
cURL versions
=====================================

.. seealso::

   - https://curl.haxx.se/changes.html

.. toctree::
   :maxdepth: 3

   7.64.1/7.64.1

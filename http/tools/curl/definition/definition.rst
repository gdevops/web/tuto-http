

.. _curl_definition:

=====================================
cURL definition
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/CURL
   - https://en.wikipedia.org/wiki/CURL



English Wikipedia definition
==============================

cURL (pronounced 'curl'[4]) is a computer software project providing a library
(libcurl) and command-line tool (curl) for transferring data using various
protocols.

It was first released in 1997. The name stands for "Client URL".[5]
The original author and lead developer is the Swedish developer Daniel Stenberg


French Wikipedia definition
==============================

cURL (abréviation de client URL request library : « bibliothèque de requêtes
aux URL pour les clients » ou see URL : « voir URL ») est une interface en
ligne de commande, destinée à récupérer le contenu d'une ressource accessible
par un réseau informatique.

La ressource est désignée à l'aide d'une URL et doit être d'un type supporté
par le logiciel (voir ci-dessous).
Le logiciel permet de créer ou modifier une ressource (contrairement à wget),
il peut ainsi être utilisé en tant que client REST.

Le programme cURL implémente l'interface utilisateur et repose sur la
bibliothèque logicielle libcurl, développée en langage C.

Celle-ci est ainsi accessible aux programmeurs qui veulent disposer des
fonctionnalités d'accès au réseau dans leurs programmes.

Des interfaces ont été créées dans de nombreux langages (C++, Java, .NET, Perl,
PHP, Ruby...).

cURL definition
================

.. seealso::

   - https://github.com/curl/curl

A command line tool and library for transferring data with URL syntax,
supporting HTTP, HTTPS, FTP, FTPS, GOPHER, TFTP, SCP, SFTP, SMB, TELNET,
DICT, LDAP, LDAPS, FILE, IMAP, SMTP, POP3, RTSP and RTMP. libcurl offers
a myriad of powerful features.

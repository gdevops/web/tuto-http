
.. index::
   pair: HTTP ; Curl

.. _curl:

=====================================
cURL (client URL request library)
=====================================

.. seealso::

   - https://fr.wikipedia.org/wiki/CURL
   - https://curl.haxx.se/


.. figure:: Curl-logo.svg.png
   :align: center

.. toctree::
   :maxdepth: 3

   definition/definition
   versions/versions

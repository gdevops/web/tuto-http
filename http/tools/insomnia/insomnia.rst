
.. index::
   pair: API ; insomnia

.. _insomnia:

=====================================
insomnia
=====================================

.. seealso::

   - https://github.com/getinsomnia/insomnia
   - https://insomnia.rest/
   - https://x.com/GetInsomnia
   - https://x.com/GregorySchier


.. index::
   pair: httpie ; Versions

.. _httpie_versions:

=====================================
httpie versions
=====================================

.. seealso::

   - https://github.com/jkbrzt/httpie




.. toctree::
   :maxdepth: 3

   1.0.2/1.0.2

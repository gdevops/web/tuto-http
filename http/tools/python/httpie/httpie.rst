
.. index::
   pair: httpie ; Python

.. _httpie:

=====================================
httpie
=====================================

.. seealso::

   - https://github.com/jkbrzt/httpie
   - https://x.com/clihttp
   - https://httpie.org/




As easy as httpie /aitch-tee-tee-pie/ 🥧 Modern command line HTTP client,
user-friendly curl alternative with intuitive UI, JSON support, syntax
highlighting, wget-like downloads, extensions, etc.


.. toctree::
   :maxdepth: 3

   articles/articles
   authentication/authentication
   plugins/plugins
   help/help
   versions/versions

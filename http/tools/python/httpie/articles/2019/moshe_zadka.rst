
.. index::
   pair: httpie ; Moshe Zaka

.. _httpie_zadka:

==================================================
Getting started with HTTPie for API testing
==================================================

.. seealso::

   - https://opensource.com/article/19/8/getting-started-httpie

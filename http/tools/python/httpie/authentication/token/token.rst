
.. index::
   pair: httpie ; Token
   pair: Authorization ; Token

.. _httpie_token:

=====================================
httpie Authorization: Token
=====================================

.. seealso::

   - https://github.com/jkbrzt/httpie
   - https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html
   - https://stackoverflow.com/questions/40398566/token-authentication-django-rest-framework-httpie
   - https://www.django-rest-framework.org/api-guide/authentication/#tokenauthentication
   - :ref:`header_authorization`




Use 'Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
================================================================

Example with httpie::

    http -v "http://localhost:8005/programs/XXX/?TTTTTTTTTT=RRRR&ZZZZZZ=PPPPPPPPP" 'Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

::

    GET programs/XXX/?TTTTTTTTTT=RRRR&ZZZZZZ=PPPPPPPPP HTTP/1.1
    Accept: */*
    Accept-Encoding: gzip, deflate
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Connection: keep-alive
    Host: localhost:8005
    User-Agent: HTTPie/1.0.2



    HTTP/1.1 200 OK
    Allow: OPTIONS, GET
    Connection: Keep-Alive
    Content-Length: 1138
    Content-Type: application/json
    Date: Fri, 05 Apr 2019 13:01:58 GMT
    Keep-Alive: timeout=5, max=100
    Server: Apache/2.4.25 (Debian)
    X-Frame-Options: SAMEORIGIN

    {
        XXX,
        YYY,
    }

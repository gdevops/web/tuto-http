
.. index::
   pair: httpie ; authentication

.. _httpie_authentication:

=====================================
httpie authentication
=====================================

.. seealso::

   - https://github.com/jkbrzt/httpie
   - https://x.com/clihttp
   - https://httpie.org/



Description
=============

The currently supported authentication schemes are Basic and Digest
(see auth plugins for more).

There are two flags that control authentication:

    --auth, -a  Pass a username:password pair as the argument.


Example::

        http -b -a <username>:<password> "http://<hostname>/programs/<function>/?xx=aaa=&&yyy=bbbb"


Or, if you only specify a username (-a username), you'll be prompted
for the password before the request is sent.

To send an empty password, pass username:.

The username:password@hostname URL syntax is supported as well
(but credentials passed via -a have higher priority).

--auth-type, -A     Specify the auth mechanism.

Possible values are basic and digest.

The default value is basic so it can often be omitted.


auth-plugins
================

.. seealso:: :ref:`httpie_authentication_plugins`



httpie token
==============

.. toctree::
   :maxdepth: 3

   token/token

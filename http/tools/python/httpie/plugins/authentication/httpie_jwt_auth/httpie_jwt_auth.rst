
.. index::
   pair: httpie ; JWTAuth (JSON Web Tokens

.. _httpie_jwtauth_plugin:

============================================================================
httpie-jwt-auth plugin (JWTAuth (JSON Web Tokens) auth plugin for HTTPie)
============================================================================

.. seealso::

   - https://github.com/teracyhq/httpie-jwt-auth
   - https://simpleisbetterthancomplex.com/tutorial/2018/12/19/how-to-use-jwt-authentication-with-django-rest-framework.html




Installation
=============

::

    $ pip install -U httpie-jwt-auth


Usage
=========

::

    $ http --auth-type=jwt --auth="<token>" example.org -h


::

    GET / HTTP/1.1
    Accept: */*
    Accept-Encoding: gzip, deflate
    Authorization: Token_Prefix abc
    Connection: keep-alive
    Host: example.org
    User-Agent: HTTPie/0.9.7


Sometimes you don't need to expose the JWT token on the command line,
you can use the environment variable::

    $ export JWT_AUTH_TOKEN=secret

And it should work::

    $ http teracy.com --auth-type=jwt -v


::


    GET / HTTP/1.1
    Accept: */*
    Accept-Encoding: gzip, deflate
    Authorization: Bearer secret
    Connection: keep-alive
    Host: teracy.com
    User-Agent: HTTPie/0.9.7

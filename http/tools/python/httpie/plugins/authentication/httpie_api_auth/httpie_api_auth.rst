
.. index::
   pair: httpie ; httpie_api_auth

.. _httpie_api_auth_plugin:

========================================================================
httpie-api-auth (HTTPie support for the ApiAuth authentication scheme)
========================================================================

.. seealso::

   - https://github.com/pd/httpie-api-auth




HTTPie support for the ApiAuth authentication scheme


.. index::
   pair: httpie ; authentication plugins

.. _httpie_authentication_plugins:

=====================================
httpie authentication plugins
=====================================

.. seealso::

   - https://github.com/jakubroztocil/httpie#auth-plugins




Additional authentication mechanism can be installed as plugins.

They can be found on the Python Package Index.

Here's a few picks:

- httpie-api-auth: ApiAuth
- httpie-aws-auth: AWS / Amazon S3
- httpie-edgegrid: EdgeGrid
- httpie-hmac-auth: HMAC
- httpie-jwt-auth: JWTAuth (JSON Web Tokens)
- httpie-negotiate: SPNEGO (GSS Negotiate)
- httpie-ntlm: NTLM (NT LAN Manager)
- httpie-oauth: OAuth
- requests-hawk: Hawk


.. toctree::
   :maxdepth: 3

   httpie_api_auth/httpie_api_auth
   httpie_oauth/httpie_oauth
   httpie_jwt_auth/httpie_jwt_auth

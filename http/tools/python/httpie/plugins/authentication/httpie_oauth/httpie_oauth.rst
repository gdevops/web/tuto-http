
.. index::
   pair: httpie ; httpie_oauth plugin

.. _httpie_oauth_plugin:

================================================
httpie_oauth plugin (OAuth plugin for HTTPie)
================================================

.. seealso::

   - https://github.com/httpie/httpie-oauth




It currently provides support for OAuth 1.0a 2-legged.

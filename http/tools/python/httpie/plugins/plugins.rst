
.. index::
   pair: httpie ; plugins

.. _httpie_plugins:

=====================================
httpie plugins
=====================================

.. seealso::

   - https://github.com/jkbrzt/httpie




.. toctree::
   :maxdepth: 3

   authentication/authentication

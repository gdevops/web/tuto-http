.. index::
   pair: httpx ; Description

.. _httpx_description:

==================================================================
**httpx** description
==================================================================

.. seealso::

   - https://github.com/encode/httpx/blob/master/README.md



Features
============

HTTPX is a high performance asynchronous HTTP client, that builds on the well-established usability of requests, and gives you:

- A broadly requests-compatible API.
- Standard synchronous interface, but with async support if you need it.
- HTTP/1.1 and HTTP/2 support.
- Ability to make requests directly to WSGI applications or ASGI applications.
- Strict timeouts everywhere.
- Fully type annotated.
- 99% test coverage.

Plus all the standard features of requests...

- International Domains and URLs
- Keep-Alive & Connection Pooling
- Sessions with Cookie Persistence
- Browser-style SSL Verification
- Basic/Digest Authentication
- Elegant Key/Value Cookies
- Automatic Decompression
- Automatic Content Decoding
- Unicode Response Bodies
- Multipart File Uploads
- HTTP(S) Proxy Support
- Connection Timeouts
- Streaming Downloads
- .netrc Support
- Chunked Requests

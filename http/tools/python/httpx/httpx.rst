
.. index::
   pair: httpx ; Python

.. _httpx:

==================================================================
**httpx** A next generation HTTP client for Python
==================================================================

.. seealso::

   - https://github.com/encode/httpx
   - https://github.com/encode/httpx/graphs/contributors
   - https://www.python-httpx.org/


.. figure:: logo_httpx.jpg
   :align: center

.. toctree::
   :maxdepth: 3

   description/description
   versions/versions


.. index::
   pair: urllib3 ; Python

.. _urllib3:

==================================================================
**urllib3** a powerful, sanity-friendly HTTP client for Python
==================================================================

.. seealso::

   - https://github.com/urllib3/urllib3
   - https://urllib3.readthedocs.io/en/latest/



.. figure:: logo_urllib3.svg
   :align: center


.. toctree::
   :maxdepth: 3

   versions/versions

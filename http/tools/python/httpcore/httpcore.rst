.. index::
   pair: httpcore ; async HTTP

.. _httpcore:

=========================================================================
**httpcore** (A minimal HTTP client)
=========================================================================

.. seealso::

   - https://github.com/encode/httpcore
   - https://www.encode.io/httpcore/
   - https://github.com/encode/httpcore/graphs/contributors
   - https://github.com/tomchristie
   - https://github.com/yeraydiazdiaz
   - https://github.com/florimondmanca


.. note: httpcore is used by :ref:`httpx <httpx_0_13_0>`

.. toctree::
   :maxdepth: 3


   versions/versions

.. index::
   pair: requests ; Python

.. _requests:

============================================================================================
**requests** (A end of life simple, yet elegant HTTP library) => the sucessor will be httpx
============================================================================================

.. seealso::

   - https://github.com/psf/requests
   - https://github.com/psf/requests/graphs/contributors
   - https://requests.readthedocs.io/en/latest/
   - https://realpython.com/python-requests/


.. warning:: httpx is the successor of requests

.. toctree::
   :maxdepth: 3

   definition/definition
   examples/examples
   versions/versions


.. _requests_def:

=====================================
requests definition
=====================================

.. seealso::

   - https://github.com/psf/requests/blob/master/README.md


Requests is an elegant and simple HTTP library for Python, built with hearts.

.. code-block:: python

    >>> import requests
    >>> r = requests.get('https://api.github.com/user', auth=('user', 'pass'))
    >>> r.status_code
    200
    >>> r.headers['content-type']
    'application/json; charset=utf8'
    >>> r.encoding
    'utf-8'
    >>> r.text
    '{"type":"User"...'
    >>> r.json()
    {'disk_usage': 368627, 'private_gists': 484, ...}

.. index::
   pair: requests ; examples

.. _requests_examples:

=====================================
requests examples
=====================================

.. seealso::

   - https://github.com/psf/requests/blob/master/README.md



json encoded data
==================

.. code-block:: python
   :linenos:


    def get_auth_bearer_token() -> str:
        auth_bearer_token = None
        try:
            url_api_authenticate = settings.URL_API_AUTHENTICATE_V1
            payload = {}
            payload["key"] = settings.API_KEY_AUTHENTICATE_FOR_UPDATE
            requete = requests.post(url_api_authenticate, json=payload)
            data = requete.json()
            # logger.info(f"{url_api_authenticate=}")
            auth_bearer_token = data["bearerToken"]
        except Exception as error:
            logger.error(f"get_auth_bearer_token {error=}")

        return auth_bearer_token


bearer tokens
================

.. code-block:: python
   :linenos:

    def has_license_against_api_license_v2(hardwareid: str, reference: str) -> bool:
        """Retourne True si la licence est activée
           pour le hardwareid et la référence de produit donné.

           Args:
               hardwareid (str): le hardwareid identifie le PC

               reference (str): la référence du produit

           Returns:
               bool: True si la licence est activée


        """
        logger.info(f"has_license_against_api_license_v2()")
        nb_licenses = 0
        url_licenses_keys_v2 = settings.URL_API_LICENSE_V2
        requete = None
        has_license = False
        try:
            # http://docs.python-requests.org/en/master/user/quickstart/#passing-parameters-in-urls
            payload = {}
            payload["hardwareid"] = hardwareid
            payload["reference"] = reference
            bearer_token = get_auth_bearer_token()
            headers = {
                "Accept": "application/json",
                "Authorization": "Bearer " + bearer_token,
            }
            requete = requests.get(url_licenses_keys_v2, params=payload, headers=headers,)
            data = requete.json()[0]
            status = data["status"]
            if status.lower() == "active":
                has_license = True

            logger.info(f"\n{requete.url=}\n{payload=}\n" f"{status=}\n" f"{has_license=}")
        except Exception as error:
            logger.error(f"{error=}\n")
            return False

        return has_license

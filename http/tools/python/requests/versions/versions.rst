
.. index::
   pair: requests ; Versions

.. _requests_versions:

=====================================
requests versions
=====================================

.. seealso::

   - https://github.com/psf/requests/releases

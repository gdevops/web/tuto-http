
.. index::
   pair: HTTP ; status codes
   pair: IANA ; HTTP response status codes
   pair: Mozilla ; HTTP response status codes
   pair: RFC 7231 ; HTTP response status codes
   pair: RFC ; 7231
   pair: RFC ; 2616
   ! HTTP response status codes

.. _http_status_codes:

===================================================
HTTP response status codes (rfc2616 and RFC 7231)
===================================================

.. seealso::

   - https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
   - https://tools.ietf.org/html/rfc7231
   - https://tools.ietf.org/html/rfc2616#section-10



Mozilla HTTP status codes
===========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Status


IANA HTTP status codes
=========================

.. seealso::

   - https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml


Liste
=======

.. toctree::
   :maxdepth: 3

   403/403
   405/405
   418/418
   500/500

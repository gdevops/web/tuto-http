
.. index::
   pair: HTTP ; base
   ! Hypertext Transfer Protocol
   ! HTTP

.. _http_base:

=====================================
HTTP
=====================================

- https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview


.. toctree::
   :maxdepth: 6

   news/news
   versions/versions
   definition/definition
   basics/basics
   authentication/authentication
   headers/headers
   protocoles/protocoles
   security/security
   status/status
   tools/tools

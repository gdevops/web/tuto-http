
.. _http_definition:

=====================================
HTTP definition
=====================================




English wikipedia definition
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview


L'Hypertext Transfer Protocol (HTTP, littéralement « protocole de
transfert hypertexte ») est un protocole de communication client-serveur
développé pour le World Wide Web. HTTPS (avec S pour secured, soit
« sécurisé ») est la variante du HTTP sécurisée par l'usage des
protocoles SSL ou TLS.

HTTP est un protocole de la couche application. Il peut fonctionner sur
n'importe quelle connexion fiable, dans les faits on utilise le protocole
TCP comme couche de transport.

Un serveur HTTP utilise alors par défaut le port 80 (443 pour HTTPS).

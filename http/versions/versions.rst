
.. index::
   pair: HTTP ; Versions

.. _http_versions:

=====================================
HTTP versions
=====================================

.. toctree::
   :maxdepth: 6

   2.0/2.0
   1.1/1.1


.. index::
   pair: HTTP ; 2.0

.. _http_2_0:

=====================================
HTTP 2.0 RFC 7540, May 2015
=====================================

.. seealso::

   - https://www.rfc-editor.org/info/rfc7540
   - https://wizardzines.com/comics/http2/
   - https://mailarchive.ietf.org/arch/msg/ietf-announce/5gYnAnBRmwS7jHWllgryNSyPtD4/

.. toctree::
   :maxdepth: 6


.. _http2_julia_evans:

HTTP2 by Julia evans (https://x.com/b0rk)
==================================================

.. seealso::

   - https://x.com/b0rk
   - https://wizardzines.com/comics/http2/

.. figure:: http2.png
   :align: center

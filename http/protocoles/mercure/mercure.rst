.. index::
   pair: HTTP/2 ; mercure
   ! mercure

.. _http_mercure:

=====================================
**mercure**
=====================================

.. seealso::

   - https://mercure.rocks/spec
   - https://tools.ietf.org/html/draft-dunglas-mercure-07
   - https://github.com/dunglas/mercure
   - https://github.com/dunglas/mercure/graphs/contributors
   - https://demo-chat.mercure.rocks/
   - https://github.com/dunglas/mercure/tree/master/examples/chat
   - https://dunglas.fr/2020/06/say-hello-to-mercure-0-10/
   - https://les-tilleuls.coop/fr/blog/article/mercure-un-protocole-pour-pousser-des-mises-a-jour-vers-des-navigateurs-et-app-mobiles-en-temps-reel


.. toctree::
   :maxdepth: 6

   definition/definition
   demos/demos
   versions/versions

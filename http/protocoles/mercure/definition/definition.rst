

.. _http_mercure_def:

=====================================
Definition
=====================================

.. seealso::

   - https://les-tilleuls.coop/fr/blog/article/mercure-un-protocole-pour-pousser-des-mises-a-jour-vers-des-navigateurs-et-app-mobiles-en-temps-reel




Mercure en quelques mots
============================

- Support natif de navigateurs, pas de bibliothèque ni de SDK requis
  (basé sur les “Server-Sent Events”).
- Compatible avec tous les serveurs existants, même ceux qui ne supportent
  pas les connexions persistantes (architecture serverless, PHP, FastCGI…).
- Support natif de la reconnexion et de la récupération des événements perdus.
- Mécanisme d'autorisation basé sur JWT (envoi sécurisé d'une mise à jour
  à certains “abonnés” sélectionnés).
- Exploite le multiplexage HTTP/2.
- Conçu pour les API hypermédia, supporte également GraphQL.
- Auto-découvrable grâce au web linking.
- Supporte le chiffrement.
- Fonctionne même avec les vieux navigateurs en utilisant un polyfill (IE7+).
- Support du push sans connexion dans des environnements contrôlés
  (délégation aux protocoles spécifiques des opérateurs mobiles).

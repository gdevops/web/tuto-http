
.. _http_mercure_demos:

=====================================
**mercure demos**
=====================================

.. seealso::

   - https://demo-chat.mercure.rocks/

.. toctree::
   :maxdepth: 6

   chat_python/chat_python



.. _mercure_annonce_2018_10_23:

=========================================================================================================
Mercure, un protocole pour pousser des mises à jour vers des navigateurs et app mobiles en temps réels
=========================================================================================================

.. seealso::

   - https://les-tilleuls.coop/fr/blog/article/mercure-un-protocole-pour-pousser-des-mises-a-jour-vers-des-navigateurs-et-app-mobiles-en-temps-reel
   - https://mercure.rocks/docs/ecosystem/awesome



Introduction
=============

Mercure est un protocole permettant de transmettre en temps réel des
mises à jour de données vers les navigateurs web (ou autres clients HTTP)
de manière fiable, rapide et économe en énergie.

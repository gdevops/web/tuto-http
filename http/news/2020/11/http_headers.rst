

.. _http_structured_headers:

=====================================
HTTP structured headers
=====================================

.. seealso::

   - https://www.fastly.com/blog/improve-http-structured-headers

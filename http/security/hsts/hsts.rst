.. index::
   pair: HTTP Strict-Transport-Security; HSTS
   pair: RFC ; 6797
   pair: Sécurité ; HSTS

.. _hsts:

======================================================
**HSTS** (HTTP Strict-Transport-Security, RFC 6797)
======================================================

.. seealso::

   - http://www.bortzmeyer.org/6797.html
   - https://blog.appcanary.com/2017/http-security-headers.html#http-strict-transport-security-hsts
   - https://wiki.mozilla.org/Security/Server_Side_TLS#HSTS:_HTTP_Strict_Transport_Security
   - https://fr.wikipedia.org/wiki/HTTP_Strict_Transport_Security
   - https://docs.djangoproject.com/en/dev/ref/middleware/#http-strict-transport-security


.. figure:: hsts.png
   :align: center



**Utile si on utilise https** (voir https://letsencrypt.org/)


Explications de Stéphane Bortzmeyer
====================================

.. seealso::

   - http://www.bortzmeyer.org/6797.html


La technique **HSTS** (HTTP Strict Transport Security), normalisée dans ce RFC
(mais déjà largement déployée) vise à résoudre une attaque contre TLS.
Si un site Web est accessible en HTTPS, et qu'un méchant arrive à convaincre
un utilisateur de se connecter en HTTP ordinaire à la place, le méchant peut
alors monter une attaque de l'homme du milieu.
Le fait que le site soit protégé par TLS n'aidera pas dans ce cas. Pour éviter
cette attaque, HSTS permet à un site de déclarer qu'il n'est accessible qu'en
HTTPS. Si l'utilisateur a visité le site ne serait-ce qu'une fois, son
navigateur se souviendra de cette déclaration et ne fera plus ensuite de
connexions non sécurisées.

Bien sûr, il n'y aurait pas de problème si l'utilisateur utilisait systématiquement
des URL https://
Mais les erreurs ou les oublis peuvent arriver, l'utilisateur peut cliquer sur
un lien http://... dans un spam, il peut être victime d'une page Web certes
sécurisée mais qui contient des images chargées via un URL http://....

Bref, il y a encore trop de risques.

D'où l'idée d'épingler la politique de sécurité du site Web dans le navigateur,
en permettant à un site de déclarer au navigateur « je suis sécurisé, ne reviens
jamais me voir en HTTP ordinaire » et de compter sur le fait que le navigateur
respectera automatiquement cette politique, et s'en souviendra.

Cette déclaration se fera par l'en-tête HTTP Strict-Transport-Security:,
par exemple::

    Strict-Transport-Security: max-age=7905600.


Définition Mozilla (MDN)
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Strict-Transport-Security

The HTTP Strict-Transport-Security response header (often abbreviated as HSTS)
is a **security feature** that lets a web site tell browsers that it should only be
communicated with using HTTPS, instead of using HTTP


HTTP Strict Transport Security (souvent abrégé par HSTS) est un dispositif de
sécurité par lequel un site web peut déclarer aux navigateurs qu'ils doivent
communiquer avec lui en utilisant exclusivement le protocole HTTPS, au lieu du HTTP.



Définition wikipedia
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/HTTP_Strict_Transport_Security


HTTP Strict Transport Security (HSTS) est un mécanisme de politique de sécurité
proposé pour HTTP, permettant à un serveur web de déclarer à un agent utilisateur
(comme un navigateur web), compatible, qu'il doit interagir avec lui en
utilisant une connexion sécurisée (comme HTTPS).

La politique est donc communiquée à l'agent utilisateur par le serveur via la
réponse HTTP, dans le champ d'en-tête nommé « Strict-Transport-Security ».

La politique spécifie une période de temps durant laquelle l'agent utilisateur
doit accéder au serveur uniquement de façon sécurisée


Définition Django
===================

.. seealso::

   -    - https://docs.djangoproject.com/en/dev/ref/middleware/#http-strict-transport-security


Sécurité de transport HTTP stricte (HSTS)

Pour les sites qui ne devraient pouvoir être accédés que par HTTPS, vous pouvez
demander aux navigateurs modernes de refuser les connexions à votre nom de
domaine si la connexion n’est pas sécurisée (pour une certaine période de temps)
en définissant l’en-tête « Strict-Transport-Security ».

Cela réduit l’exposition à certaines attaques de type « homme du milieu »
(MITM) par détournement SSL.

SecurityMiddleware définit cet en-tête pour vous sur toutes les réponses HTTPS
si vous définissez le réglage SECURE_HSTS_SECONDS à une valeur entière
différente de zéro.

Lors de l’activation de HSTS, il est recommandé d’utiliser d’abord une faible
valeur pour tester, par exemple SECURE_HSTS_SECONDS = 3600 pour une heure.

Chaque fois qu’un navigateur Web lit l’en-tête HSTS pour votre site, il
refusera de communiquer de manière non sécurisée (par HTTP) avec votre domaine
pour la durée de temps indiquée. Après avoir obtenu confirmation que toutes
les ressources de votre site sont servies de manière sécurisée (c’est-à-dire
que HSTS ne casse rien du tout), il est alors possible et souhaité d’augmenter
cette valeur afin que les visiteurs occasionnels soient protégés (il est
fréquent de voir 31536000 secondes, c’est-à-dire 1 an).

De plus, si vous définissez le réglage SECURE_HSTS_INCLUDE_SUBDOMAINS à True,
SecurityMiddleware ajoute la directive includeSubDomains
à l’en-tête Strict-Transport-Security. C’est recommandé (pour autant que tous
les sous-domaines soient servis exclusivement par HTTPS), sinon votre site
pourrait rester vulnérable au travers d’une connexion non sécurisée à un
sous-domaine



Configuration Django
======================

::

    SECURE_HSTS_SECONDS = 31536000
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True




.. note:: Si votre déploiement se trouve derrière un répartiteur de charge ou
   un serveur mandataire inverse, et que l’en-tête Strict-Transport-Security
   n’est pas ajouté à vos réponses, il est possible que Django ne détecte pas
   qu’il fonctionne sur une connexion sécurisée ; il faut alors définir le
   réglage SECURE_PROXY_SSL_HEADER.




Configuration apache
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/HTTP_Strict_Transport_Security#Impl.C3.A9mentation_Apache


::

    # load module (example using [RHEL])
    LoadModule headers_module modules/mod_headers.so


.. code-block:: apache
   :linenos:

    # redirect all HTTP to HTTPS (optional)
    <VirtualHost *:80>
           ServerAlias *
           RewriteEngine On
           RewriteRule ^(.*)$ https://%{HTTP_HOST}$1 [redirect=301]
    </VirtualHost>



.. code-block:: apache
   :linenos:

    <VirtualHost 10.0.0.1:443>
          # HTTPS-Host-Configuration
          # Use HTTP Strict Transport Security to force client to use secure connections only
          Header always set Strict-Transport-Security "max-age=500; includeSubDomains; preload"

          # Further Configuration goes here
    </VirtualHost>

.. index::
   pair: Set-Cookie ; HTTP
   pair: Sécurité ; Set-Cookie


.. _set_cookie:

============
Set-Cookie
============


.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie
   - https://blog.appcanary.com/2017/http-security-headers.html#cookie-options


.. figure:: cookies.png
   :align: center



Définition 1
==============

This isn’t a security header per se, but there are three different options for
cookies that you should be aware of.

- Cookies marked as Secure will only be served over HTTPS. This prevents someone
  from reading the cookies in a MiTM attack where they can force the browser
  to visit a given page.

- HttpOnly is a misnomer, and has nothing to do with HTTPS (unlike Secure above).
  Cookies marked as HttpOnly can not be accessed from within javascript.
  So if there is an XSS flaw, the attacker can’t immediately steal the cookies.

- SameSite helps defend against Cross-Origin Request Forgery (CSRF) attacks.
  This is an attack where a different website the user may be visiting
  inadvertently tricks them into making a request against your site, i.e. by
  including an image to make a GET request, or using javascript to submit a
  form for a POST request.
  Generally, people defend against this using CSRF tokens. A cookie marked as
  SameSite won’t be sent to a different site.



Solution Django (si utilisation de https)
===========================================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SESSION_COOKIE_SECURE

Django 	Session cookies are HttpOnly by default. To set secure::

    SESSION_COOKIE_SECURE = True.

Not sure about SameSite.

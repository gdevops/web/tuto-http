.. index::
   pair: Content Security Policy ; CSP
   pair: Sécurite ; CSP
   ! CSP
   ! Content Security Policy

.. _csp:

=========================================
**Content Security Policy** (CSP, new)
=========================================

.. seealso::

   - https://www.w3.org/TR/CSP/
   - http://caniuse.com/#feat=contentsecuritypolicy
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy


.. figure:: csp.png
   :align: center



Définition 1
=============

Content Security Policy can be thought of as much more advanced version of the
X-XSS-Protection header above. While X-XSS-Protection will block scripts that
come from the request, it’s not going to stop an XSS attack that involves
storing a malicious script on your server or loading an external resource
with a malicious script in it.

CSP gives you a language to define where the browser can load resources from.

You can white list origins for scripts, images, fonts, stylesheets, etc in a
very granular manner. You can also compare any loaded content against a
hash or signature.


Définition Mozilla
===================

The HTTP Content-Security-Policy response header allows web site administrators
to control resources the user agent is allowed to load for a given page.

With a few exceptions, policies mostly involve specifying server origins and
script endpoints.

This helps guard against cross-site scripting attacks (XSS)


Paramètrage Django
===================

.. seealso::

   - https://github.com/mozilla/django-csp


Installer django-csp mais seul un expert sécurité peut régler les
paramètres.

CSP niveau 1
===============

CSP niveau 1 permet de spécifier des directives pour les éléments suivants:

- script-src : les sources autorisées pour les scripts JS
- styles-src : les sources autorisées pour les styles CSS
- img-src : les sources autorisées pour les images
- connect-src : s’applique pour XMLHttpRequest (AJAX), WebSocket ou EventSource
- font-src : les sources pour les web fonts
- object-src : les sources des plug-ins (par exemple : <object>, <embed>, <applet>)
- media-src : les sources pour les balises <audio> et <video>

.. index::
   pair: HTTP ; X-XSS-Protection
   pair: Sécurité ; X-XSS-Protection


.. _xss_protection:

=========================================
X-XSS-Protection (Cross-site_scripting)
=========================================

.. seealso::

   - https://blog.appcanary.com/2017/http-security-headers.html#x-xss-protection
   - https://docs.djangoproject.com/fr/1.10/ref/middleware/#module-django.middleware.security
   - https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SECURE_BROWSER_XSS_FILTER


.. figure:: x_xss_protection.png
   :align: center


Définition Mozilla
===================

.. seealso:: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-XSS-Protection


The HTTP X-XSS-Protection response header is a feature of Internet Explorer,
Chrome and Safari that stops pages from loading when they detect reflected
cross-site scripting (XSS) attacks.

Although these protections are largely unnecessary in modern browsers when
sites implement a strong Content-Security-Policy that disables the use of
inline JavaScript ('unsafe-inline'), they can still provide protections for
users of older web browsers that don't yet support CSP.


Définition Wikipedia
=====================

Le cross-site scripting (abrégé XSS), est un type de faille de sécurité des
sites web permettant d'injecter du contenu dans une page, permettant ainsi
de provoquer des actions sur les navigateurs web visitant la page.

Les possibilités des XSS sont très larges puisque l'attaquant peut utiliser
tous les langages pris en charge par le navigateur (JavaScript, Java, Flash...)
et de nouvelles possibilités sont régulièrement découvertes notamment avec
l'arrivée de nouvelles technologies comme HTML5.

Il est par exemple possible de rediriger vers un autre site pour de
l'hameçonnage ou encore de voler la session en récupérant les cookies.

Le cross-site scripting est abrégé XSS pour ne pas être confondu avec le CSS
(feuilles de style)1, X se lisant « cross » (croix) en anglais.


Paramètrage Django
==================

::

    SECURE_BROWSER_XSS_FILTER = True


If True, the SecurityMiddleware sets the **X-XSS-Protection: 1; mode=block** header
on all responses that do not already have it.

Si True, l’intergiciel SecurityMiddleware définit l’en-tête
**X-XSS-Protection: 1; mode=block** pour toutes les réponses qui ne l’ont pas déjà.

.. index::
   pair: HTTP ; Security headers

.. _http_security_headers:

=======================================
HTTP security headers
=======================================

.. seealso::

   - https://www.alsacreations.com/article/lire/1723-tour-horizon-https-et-en-tetes-de-securite.html
   - https://securityheaders.com/
   - https://www.ssllabs.com/

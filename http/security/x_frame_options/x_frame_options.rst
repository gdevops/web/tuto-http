.. index::
   pair: HTTP ; X-Frame-Options
   pair: Sécurité ; X-Frame-Options


.. _x_frame_options:

==============================================================
X-Frame-Options (contre le click-jacking, RFC 7034, 2013-10)
==============================================================


.. seealso::

   - https://tools.ietf.org/html/rfc7034
   - https://en.wikipedia.org/wiki/Clickjacking
   - https://fr.wikipedia.org/wiki/D%C3%A9tournement_de_clic
   - https://docs.djangoproject.com/en/dev/ref/clickjacking/
   - https://blog.appcanary.com/2017/http-security-headers.html#x-frame-options
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options


.. figure:: x_frame_options.png
   :align: center



Définition Mozilla
===================


The X-Frame-Options HTTP response header can be used to indicate whether or
not a browser should be allowed to render a page in a <frame>, <iframe> or
<object> .

Sites can use this to avoid clickjacking attacks, by ensuring that their
content is not embedded into other sites.


Solution Django
=================

.. seealso::

   - https://docs.djangoproject.com/fr/1.10/ref/clickjacking/


.. code-block:: python

   MIDDLEWARE = [
   'django.middleware.clickjacking.XFrameOptionsMiddleware',

   ]



This defaults to SAMEORIGIN.

To set DENY::

    X_FRAME_OPTIONS = 'DENY'

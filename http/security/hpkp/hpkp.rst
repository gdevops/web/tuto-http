.. index::
   pair: HTTP Public Key Pinning ; HPKP
   pair: RFC; 7469 (2015)
   pair: Sécurite ; HPKP


.. _hpkp:

=============================================================
HPKP (Public Key Pinning Extension for HTTP, RFC7469, 2015)
=============================================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Public_Key_Pinning
   - https://wiki.mozilla.org/Security/Server_Side_TLS#HPKP:_Public_Key_Pinning_Extension_for_HTTP
   - http://www.bortzmeyer.org/7469.html


.. figure:: hpkp.png
   :align: center



Explication de Stéphane Bortzmeyer
====================================

.. seealso:: http://www.bortzmeyer.org/7469.html

Depuis que les piratages de Comodo et DigiNotar ont fait prendre conscience du
peu de sécurité qu'apportent les certificats X.509, plusieurs solutions ont
été proposées pour combler les failles des autorités de certification.

La proposition de ce RFC consiste à permettre à un client d'épingler (to pin)
les clés cryptographiques d'un serveur HTTP utilisant TLS, c'est-à-dire à s'en
souvenir pendant une durée spécifiée par le serveur.
Ainsi, même si un faux certificat est émis par une AC, il ne sera pas accepté.




Définition Mozilla
===================

The Public Key Pinning Extension for HTTP (HPKP) is a security feature that
tells a web client to associate a specific cryptographic public key with a
certain web server to decrease the risk of MITM attacks with forged certificates.

To ensure the authenticity of a server's public key used in TLS sessions,
this public key is wrapped into a X.509 certificate which is usually signed
by a certificate authority (CA). Web clients such as browsers trust a lot of
these CAs, which can all create certificates for arbitrary domain names.

If an attacker is able to compromise a single CA, they can perform MITM attacks
on various TLS connections. HPKP can circumvent this threat for the HTTPS
protocol by telling the client which public key belongs to a certain web server.

HPKP is a Trust on First Use (TOFU) technique. The first time a web server
tells a client via a special HTTP header which public keys belong to it,
the client stores this information for a given period of time.

When the client visits the server again, it expects at least one certificate
in the certificate chain to contain a public key whose fingerprint is already
known via HPKP. If the server delivers an unknown public key, the client
should present a warning to the user.


Django
========

Django 	Write custom middleware

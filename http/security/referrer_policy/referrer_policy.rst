.. index::
   pair: Referrer-Policy; HTTP
   pair: Sécurité ; Referrer-Policy


.. _referer_policy:

================
Referrer-Policy
================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Referrer-Policy
   - https://blog.appcanary.com/2017/http-security-headers.html#referrer-policy
   - https://en.wikipedia.org/wiki/HTTP_referer


.. figure:: referer.png
   :align: center

Définition 1
==============

Ah, the Referer header. Great for analytics, bad for your users’ privacy.

At some point the web got woke and decided that maybe it wasn’t a good idea to
send it all the time. And while we’re at it, let’s spell “Referrer” correctly.

The Referrer-Policy header allows you to specify when the browser will set a
Referer header.

Should I use it ?
==================

It’s up to you, but it’s probably a good idea. If you don’t care about your
users’ privacy, think of it as a way to keep your sweet sweet analytics to
yourself and out of your competitors’ grubby hands.


Set **Referrer-Policy: "no-referrer"**


Définition Mozilla
====================

The Referrer-Policy HTTP header governs which referrer information, sent in
the Referer header, should be included with requests made.

Note that Referer is actually a misspelling of the word "referrer".
The Referrer-Policy header does not share this misspelling.


Solution Django
===================

Django 	Write custom middleware.

.. index::
   pair: Subresource ; Integrity ;


.. _subresource_integrity:

======================
Subresource integrity
======================

.. seealso::

   - https://www.w3.org/TR/SRI/
   - https://w3c.github.io/webappsec-subresource-integrity/
   - https://developer.mozilla.org/fr/docs/Web/Security/Subresource_Integrity


.. figure:: subresource_integrity.png
   :align: center


Définition MDN Mozilla
=======================


Subresource Integrity (SRI, ou « Intégrité des sous-ressources ») est une
fonction de sécurité qui permet aux navigateurs de vérifier que les fichiers
qu'ils vont chercher (par exemple, à partir d'un CDN) sont livrés sans
manipulation inattendue.

Cela fonctionne en permettant de fournir un hachage cryptographique (« hash »)
auquel le fichier récupéré doit correspondre.

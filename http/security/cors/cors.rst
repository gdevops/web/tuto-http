.. index::
   pair: Sécurite ; CORS
   pair: HTTP ; CORS
   pair: CORS ; Cross-origin resource sharing

.. _cors:

=======================================
CORS (Cross-origin resource sharing)
=======================================

.. seealso::

   - https://www.w3.org/TR/cors/
   - https://github.com/ottoyiu/django-cors-headers
   - https://en.wikipedia.org/wiki/Cross-origin_resource_sharing
   - https://www.html5rocks.com/en/tutorials/cors/

.. figure:: cors.png
   :align: center



Django
=======

.. seealso::

   - https://github.com/ottoyiu/django-cors-headers


Django app for handling the server headers required for Cross-Origin Resource
Sharing (CORS)

Although JSON-P is useful, it is strictly limited to GET requests. CORS builds
on top of XmlHttpRequest to allow developers to make cross-domain requests,
similar to same-domain requests.

Read more about it here: http://www.html5rocks.com/en/tutorials/cors/

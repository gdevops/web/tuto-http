.. index::
   pair: HTTP ; Sécurité
   pair: Sécurité ; ANSSI
   pair: Sécurité ; SSLLabs
   pair: Sécurité ; EFF
   pair; Sécurité ; https://www.ssllabs.com/
   pair; Sécurité ; https://observatory.mozilla.org/
   pair; SSL ; https://www.ssllabs.com/
   pair; Mozilla ; https://observatory.mozilla.org/


.. _chapitre_securite:

=============
HTTP
=============

.. seealso::

   - https://fr.wikipedia.org/wiki/Portail:S%C3%A9curit%C3%A9_informatique
   - https://docs.djangoproject.com/en/dev/topics/security/
   - https://observatory.mozilla.org/analyze.html?host=appcanary.com
   - https://securityheaders.com/
   - https://internethealthreport.org/v01/fr/privacy-and-security/
   - https://blog.appcanary.com/2017/http-security-headers.html




EFF
======

.. seealso::

   - https://starttls-everywhere.org


ssllabs
--------

.. seealso::

   - https://www.ssllabs.com/


Les menaces de sécurité HTTP
==============================

.. toctree::
   :maxdepth: 3

   csp/csp
   xss_protection/xss_protection
   headers/headers
   hsts/hsts
   hpkp/hpkp
   x_frame_options/x_frame_options
   x_content_type_options/x_content_type_options
   referrer_policy/referrer_policy
   set_cookie/set_cookie
   subresource_integrity/subresource_integrity
   cors/cors
   redirection/redirection

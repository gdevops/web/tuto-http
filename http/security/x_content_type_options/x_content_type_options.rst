.. index::
   pair: X-Content-Type-Options ; HTTP
   pair: Sécurité ; X-Content-Type-Options


.. _x_content_type_options:

========================
X-Content-Type-Options
========================

.. seealso::

   - https://blog.appcanary.com/2017/http-security-headers.html#x-content-type-options
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Content-Type-Options


.. figure:: x_content_type_options.png
   :align: center


Définition 1
=============


The problem this header solves is called “MIME sniffing”, which is actually a
browser “feature”.


Définition 2
=============

The X-Content-Type-Options response HTTP header is a marker used by the server
to indicate that the MIME types advertised in the Content-Type headers should
not be changed and be followed. This allows to opt-out of MIME type sniffing,
or, in other words, it is a way to say that the webmasters knew what they
were doing.

This header was introduced by Microsoft in IE 8 as a way for webmasters to
block content sniffing that was happening and could transform non-executable
MIME types into executable MIME types. Since then, other browsers have
introduced it, even if their MIME sniffing algorithms were less aggressive.

**Site security testers usually expect this header to be set**.


Solution Django
================

.. seealso::

   - https://docs.djangoproject.com/en/dev/ref/settings/#secure-content-type-nosniff


::

    SECURE_CONTENT_TYPE_NOSNIFF = True

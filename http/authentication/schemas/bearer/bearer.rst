
.. index::
   pair: Bearer ; authentication schema
   pair: Bearer ; Porteur

.. _bearer_http_authentication:

=======================================
**bearer** HTTP authentication schema
=======================================

- :ref:`rfc_6819`


Bearer
=========

Voir `RFC 6750 <https://www.rfc-editor.org/rfc/rfc6750>`_, jetons bearer
(**porteur** en français) pour accéder à des ressources protégées par OAuth 2.0.

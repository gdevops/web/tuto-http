
.. index::
   pair: HTTP ; authentication schemas

.. _basic_http_authentication:

=====================================
basic HTTP authentication schema
=====================================

- https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication#sch%C3%A9ma_dauthentification_basique


Le schéma d'authentification « basique » est défini dans la RFC 7617, et
transmet les identifiants via des ensembles ID_utilisateur/mot_de_passe,
encodés avec base64.

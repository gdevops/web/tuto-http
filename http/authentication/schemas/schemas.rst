
.. index::
   pair: HTTP ; authentication schemas

.. _http_authentication_schemas:

=====================================
HTTP authentication schemas
=====================================


- https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication
================================================================

- https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication

Schéma d'authentification
--------------------------

La structure d'authentification HTTP est utilisée par plusieurs schémas
d'authentification.

Ils diffèrent de par leur niveau de sécurité ainsi que par leur disponibilité
dans les systèmes client ou serveur.

Le plus commun est le schéma d'authentification « Basique » (« Basic » en anglais),
qui est présenté plus en détail ci-dessous.

IANA maintient une liste des schémas d'authentification, mais ils y en a
d'autres fournit par des services d'hébergement comme Amazon AWS.

Les schémas communs sont :

Basic

    Voir RFC 7617, identifiants encodés en base64. Voir ci-dessous pour plus de détails.

Bearer

    Voir RFC 6750, jetons bearer (« porteur » en français) pour accéder
    à des ressources protégées par OAuth 2.0.




.. toctree::
   :maxdepth: 3

   basic/basic
   bearer/bearer


.. index::
   pair: HTTP ; authentication

.. _http_authentication:

=====================================
HTTP authentication
=====================================

- https://github.com/mdn/translated-content/blob/main/files/fr/web/http/authentication/index.md
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication
- https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication

.. toctree::
   :maxdepth: 3

   definition/definition
   schemas/schemas
   rfcs/rfcs

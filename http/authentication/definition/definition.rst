

.. _http_authentication_def:

=====================================
HTTP authentication definition
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication
   - https://developer.mozilla.org/fr/docs/Web/HTTP/Authentication



Definition
=============

HTTP fournit la structure permettant le contrôle d'accès ainsi que
l'authentification.

Le schéma d'authentification HTTP le plus courant est l'authentification
"Basique" ("Basic authentication" en anglais).

Cette page a pour but de présenter ce schéma d'authentification, et montre
comment l'utiliser pour restreindre l'accès à votre serveur.

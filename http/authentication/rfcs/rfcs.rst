
.. index::
   pair: HTTP ; RFCs

.. _authentication_rfcs:

=====================================
Authentication RFCs
=====================================

.. toctree::
   :maxdepth: 3

   7235/7235
   6819/6819


.. index::
   pair: HTTP header; CORS

.. _header_cors:

=====================================
Cross-Origin Resource Sharing (CORS)
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS
   - https://fetch.spec.whatwg.org/#http-cors-protocol

.. toctree::
   :maxdepth: 3

   definition/definition

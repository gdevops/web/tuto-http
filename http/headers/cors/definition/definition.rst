

.. _header_cors_def:

=====================================
HTTP CORS definition
=====================================



Mozilla CORS definition
=========================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS


Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional
HTTP headers to tell a browser to let a web application running at one origin
(domain) have permission to access selected resources from a server at a
different origin.

A web application executes a cross-origin HTTP request when it requests a
resource that has a different origin (domain, protocol, and port) than its
own origin.

An example of a cross-origin request: The frontend JavaScript code for a web
application served from http://domain-a.com uses XMLHttpRequest to make a
request for http://api.domain-b.com/data.json.

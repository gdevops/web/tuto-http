

.. _headers_authentication_def:

=====================================
HTTP headers Definition
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#Authentication



Definition
=============

HTTP provides a general framework for access control and **authentication**.

The most common HTTP authentication is based on the "Basic" schema.

This page shows an introduction to the HTTP framework for authentication
and shows how to restrict access to your server using the HTTP "Basic"
schema.


.. index::
   pair: DRF; Authentication

.. _drf_authentication:

=====================================
DRF Authentication
=====================================

.. seealso::

   - https://www.django-rest-framework.org/api-guide/authentication/

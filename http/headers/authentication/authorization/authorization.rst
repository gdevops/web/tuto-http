
.. index::
   pair: HTTP headers; Authorization

.. _header_authorization:

=======================================
HTTP request header **Authorization**
=======================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization
   - :ref:`apache_header_authorization`




Description
=============

The HTTP **Authorization** request header contains the credentials to
authenticate a user agent with a server, usually after the server has
responded with a 401 Unauthorized status and the WWW-Authenticate header.


Syntax
========

::

    Authorization: <type> <credentials>


Examples
----------

Example 1
++++++++++++++


::

    GET programs/XXX/?TTTTTTTTTT=RRRR&ZZZZZZ=PPPPPPPPP HTTP/1.1
    Accept: */*
    Accept-Encoding: gzip, deflate
    Authorization: Token XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    Connection: keep-alive
    Host: localhost:8005
    User-Agent: HTTPie/1.0.2



Example 2
++++++++++++++

::

	http -vvv -a username:password http://localhost:8003/admin/

::

	GET /admin/ HTTP/1.1
	Accept: */*
	Accept-Encoding: gzip, deflate
	Authorization: Basic <data cryptees>
	Connection: keep-alive
	Host: localhost:8003
	User-Agent: HTTPie/1.0.2

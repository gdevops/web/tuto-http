
.. index::
   pair: HTTP headers; Authentication

.. _headers_authentication:

=====================================
HTTP headers Authentication
=====================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers#Authentication
   - https://tools.ietf.org/html/rfc7235




.. toctree::
   :maxdepth: 3

   definition/definition
   drf/drf

Headers
========

.. toctree::
   :maxdepth: 3

   authorization/authorization
   WWW_authenticate/WWW_authenticate

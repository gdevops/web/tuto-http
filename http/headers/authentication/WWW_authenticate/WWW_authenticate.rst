
.. index::
   pair: HTTP headers; WWW-Authenticate

.. _WWW_Authenticate:

===========================================
HTTP response header **WWW-Authenticate**
===========================================

.. seealso::

   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/WWW-Authenticate
   - https://tools.ietf.org/html/rfc7235#section-4.1
   - :ref:`apache_header_www_authenticate`



Description
=============

The HTTP **WWW-Authenticate** response header defines the authentication
method that should be used to gain access to a resource.

The WWW-Authenticate header is sent along with a 401 Unauthorized response.


Syntax
=======

::

    WWW-Authenticate: <type> realm=<realm>


Examples
----------

::

	HTTP/1.1 401 Unauthorized
	Connection: Keep-Alive
	Content-Length: 358
	Content-Type: text/html; charset=UTF-8
	Date: Wed, 22 May 2019 09:48:48 GMT
	Keep-Alive: timeout=5, max=100
	Server: Apache/2.4.25 (Debian)
	WWW-Authenticate: Basic realm="Protected"


::

	HTTP/1.1 401 Unauthorized
	Connection: Keep-Alive
	Content-Length: 458
	Content-Type: text/html; charset=iso-8859-1
	Date: Wed, 22 May 2019 11:28:24 GMT
	Keep-Alive: timeout=5, max=100
	Server: Apache/2.4.25 (Debian)
	WWW-Authenticate: Basic realm="Authentification basique pour log_transaction"


.. index::
   pair: HTTP ; headers

.. _http_headers:

=====================================
HTTP headers
=====================================

.. seealso::

   - https://en.wikipedia.org/wiki/List_of_HTTP_header_fields
   - https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers
   - https://www.iana.org/assignments/message-headers/message-headers.xhtml


.. toctree::
   :maxdepth: 6

   authentication/authentication
   cors/cors

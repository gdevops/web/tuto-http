.. Tuto web base documentation master file, created by
   sphinx-quickstart on Fri Apr  5 13:20:08 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. figure:: images/http_home_specs_2023_08.png
   :align: center

   https://httpwg.org


.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/tuto-http/rss.xml>`_


.. _http_tuto:
.. _tuto_http:

=====================
**HTTP tutorial**
=====================

- https://github.com/httpwg
- https://httpwg.org/
- https://httpwg.org/specs/
- https://httpwg.org/http-core/
- https://github.com/httpwg/http-core
- https://github.com/httpwg/http-core.commits.atom
- https://developer.mozilla.org/en-US/docs/Web/HTTP
- https://devdocs.io/http/
- https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
- https://datatracker.ietf.org/wg/httpbis/about/
- https://httptoolkit.tech/


.. toctree::
   :maxdepth: 6

   history/history
   people/people
   news/news
   browsers/browsers
   http/http
   servers/servers
   load_testing/load_testing
   tutorials/tutorials
   glossaires/glossaires

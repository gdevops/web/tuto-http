
.. _http_browsers:

=====================
Browsers
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/World_Wide_Web


.. seealso::

   - :ref:`genindex`
   - :ref:`search`


.. toctree::
   :maxdepth: 6


   firefox/firefox


.. index::
   pair: DOM ;  dom_forms_selectsearch

.. _dom_forms_selectsearch:

=========================================
dom_forms_selectsearch  versus datalist
=========================================

.. seealso::

   - https://x.com/pascalchevrel/status/1184871012489486336?s=20
   - https://bugzilla.mozilla.org/show_bug.cgi?id=1332301
   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist




dom_forms_selectsearch
=========================

.. figure:: dom_forms_selectsearch.png
   :align: center



Datalist
==========

.. seealso::

   - https://x.com/meduzen/status/1184912321619152900?s=20
   - https://developer.mozilla.org/en-US/docs/Web/HTML/Element/datalist

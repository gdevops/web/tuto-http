
.. index::
   pair: Glossaire ; Apache2
   ! Glossaire technique


.. _glossaire_technique:

==========================================
Glossaire technique
==========================================

.. seealso::

   - https://httpd.apache.org/docs/2.4/fr/glossary.html


.. glossary::

   authentification
   Authentification
       .. seealso::

          - :ref:`apache_authentication`

       L'**authentification** est un processus qui vous permet de vérifier
       qu'une personne est bien celle qu'elle prétend être.

       L'**authentification** est un mécanisme qui consiste à vérifier une identité,
       par exemple avec un nom d'utilisateur et un mot de passe.


   autorisation
   Autorisation
       L'**autorisation** est un processus qui permet à une personne d'aller là
       où elle veut aller, ou d'obtenir les informations qu'elle désire.

       Le **contrôle d'accès** (ou **autorisation**) consiste à accorder ou refuser
       l'accès à une ressource en fonction de certaines conditions.

    Bearer Token
    Jeton au porteur
       OAuth repose largement sur des jetons (tokens), un petit bout de données
       qui sert à prouver qu’on est légitime. Avec certains de ces jetons
       (par exemple les jetons au porteur, bearer tokens ), la seule possession
       du jeton suffit.

   Boxes
       A box is basically a bundle containing an installed operating system
       (and some basic stuff), for a specific provider (e.g. VirtualBox).

       Vagrant will replicate this basic image for your virtual machine.

       When you setup your project, you define which base box you want to use.
       The box will be downloaded and imported to the system when you
       use it for the first time


   CDN
   Content Delivery Network
       Un CDN est un réseau de serveurs judicieusement réparti sur Internet,
       sur les différents continents, dans les différents pays et chez les
       fournisseurs d'accès; chaque serveur héberge des données utilisées
       par des sites internet pour les rendre plus rapidement accessibles
       au client que s'il les obtenait du serveur.

       .. seealso::

          - https://en.wikipedia.org/wiki/Content_delivery_network

       Attention à CloudFlare : https://linuxfr.org/users/thibg/journaux/cloudflare-au-milieu (6 octobre 2015)


   DevOps
       DevOps is a loaded word; some people argue that using the word to identify
       both the movement of development and operations working more closely to
       automate infrastructure related processes, and the personnel who skew
       slightly more towards the system administration side of the equation,
       dilutes the word’s meaning.

       I think the word has simply come to be a rallying cry for the employees
       who are dragging their startups, small businesses, and enterprises into
       a new era of infrastructure growth and stability.

       I’m not too concerned that the term has become more of a catch-all for
       modern infrastructure management.

       My advice: spend less time arguing over the definition of the word, and
       more time making it mean something to you.

       .. seealso:: https://leanpub.com/ansible-for-devops

   HATEOAS
      .. seealso:: http://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven

      Les termes :term:`REST` et RESTful sont devenus des termes marketing
      pour rendre les services plus attractifs.

      Bien souvent, les services Web se réclamant de REST ne le sont pas.

      Tout au plus, ils appliquent le protocole HTTP de manière un peu plus
      conventionnelle.

      La communauté Web attachée aux principes de REST et la
      nature hypermedia des applications a décidé d'utiliser dorénavant le
      terme **HATEOAS** (en), qui est une abréviation pour hypermedia as the engine
      of application state.

      Elle permet de rendre plus explicite une des contraintes essentielles
      de REST_

      .. _REST:  http://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven

   Host and Guest
       The Host machine / OS is the one who starts vagrant.

       The Guest machine, as you can guess, is the virtual machine started
       by the Host.

   Providers
       A provider will handle the virtualization process.

       VirtualBox is the default Vagrant provider, but you could also use
       VMWare, KVM and others.

   Provisioners
       A provisioner will automate the setup of your server, installing
       packages and performing tasks in general. Using a provisioner is not
       mandatory, but not using it would make Vagrant worthless, since you
       would have to login and setup your environment manually, just as you
       were used to do before (and you could just use VirtualBox alone).

       We have many provisioners available, from the basic Shell to
       complex automation systems like Ansible.

   REST
   rest
       REST (representational state transfer) est un style d’architecture pour
       les systèmes hypermédia distribués, créé par Roy Fielding en 2000 dans
       le chapitre 5 de sa thèse de doctorat

       Ce style architectural s'applique tout autant à la réalisation
       d’applications pour un utilisateur humain qu'à la réalisation
       d’architectures orientées services destinées à la communication
       entre machines.

       .. seealso:: https://fr.wikipedia.org/wiki/Representational_State_Transfer


   Vagrantfile
      The Vagrantfile will hold your machine definitions, and it’s usually
      placed on your application root folder.

      This file is written in Ruby, but it’s basically a set of variable
      definitions, very straightforward

   Shared / Synced Folder
      It’s useful to have a common space shared between the Host and the Guest machines.
      With a shared folder, you can still edit your files with your favorite
      IDE installed on the Host machine, using the Guest machine only as a
      test server

   WSGI
      WSGI is a specification of a generic API for mapping between an
      underlying web server and a Python web application. WSGI itself is
      described by Python PEP 0333.

      The purpose of the WSGI specification is to provide a common mechanism
      for hosting a Python web application on a range of different web servers
      supporting the Python programming language
